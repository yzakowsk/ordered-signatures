
% Some thoughts about R.
The initial motivation for this work emerged from a \coq formalisation of R~\cite{RDLS18}.
This formalisation is a large monadic interpreter of roughly \(18,000\)~lines of code,
with about two thousand \bindn applications.
Proving some safety invariant within this formalisation
(for instance memory safety)
is doable, but requires a substantial proof effort due to the size of the interpreter:
invariant lemmas have to be stated and proven for every function.
Frustratingly, a lot of these lemmas felt obvious,
as most functions only use a small subset of effects.

To illustrate this intuition, we categorised the interpreter functions
involved in the formalization by the effects they used:
Figure~\ref{fig:Rlattice} shows the different categories that we identified.
An interesting part of this diagram are the categories that are not shown:
for instance, we did not find any interpreter functions
that could raise an error (effect~\lfont{E})
without reading the interpreter's global variables (effect~\lfont{R}),
thus the \lfont{RE}~category is shown in the diagram, but not~\lfont{E}.
%
Almost all interpreter functions read these global variables,
but only a small number write them:
these are the functions initialising the interpreter
at the very beginning of the execution, never to be called back again.
In figure~\ref{fig:Rlattice}, these initialising functions fall into the \lfont{RWH}~category.
%
As a consequence, for most of the interpreter execution,
these variables are constant by construction:
no interpreter function syntactially call the write operations.
The invariant that these global variables are constant after the initialisation phase
thus ought to be simple to prove, but it turned out to be cumbersome in practice.
%
We expect the \(\top\)~category to be almost empty,
with the sole exception of the main function that sets up the initialisation
and calls the read-eval-print loop.

\begin{figure}
	\begin{center}
		\begin{tikzpicture}[node distance = 15mm]
			\node (R) {\lfont{R}} ;
			\node [above left of = R] (RE) {\lfont{RE}} ;
			\node [above right of = R] (RH) {\lfont{RH}} ;
			\node [above right of = RE] (RHE) {\lfont{RHE}} ;
			\node [above right of = RH] (RWH) {\lfont{RWH}} ;
			\node [above of = RHE] (RHFE) {\lfont{RHFE}} ;
			\node [above of = RHFE] (RHFJE) {\lfont{RHFLE}} ;
			\node [above right of = RHFJE, right] (top) {\(\top\)} ;

		\draw (R) -- (RE) -- (RHE) -- (RH) -- (R) ;
		\draw (RHE) -- (RHFE) -- (RHFJE) -- (top) -- (RWH) -- (RH) ;

	\begin{scope}[node distance = 3ex]
		\node [right = 2cm of top] (legend) {\rlap{Effects used by each category:}} ;
		\node [below of = legend, xshift = 2ex] (Rl) {\lfont{R}:} ;
		\node [right = 1ex of Rl] {reading global variables} ;
		\node [below of = Rl] (Wl) {\lfont{W}:} ;
		\node [right = 1ex of Wl] {writing global variables} ;
		\node [below of = Wl] (Hl) {\lfont{H}:} ;
		\node [right = 1ex of Hl] {heap operations} ;
		\node [below of = Hl] (El) {\lfont{E}:} ;
		\node [right = 1ex of El] {throwing errors} ;
		\node [below of = El] (Fl) {\lfont{F}:} ;
		\node [right = 1ex of Fl] {function calls} ;
		\node [below of = Fl] (Jl) {\lfont{L}:} ;
		\node [right = 1ex of Jl] {low-level operations} ;
	\end{scope}
	\end{tikzpicture}
	\end{center}
	\caption{Hasse diagram for the different effects within a R~monadic interpreter~\protect\cite{RDLS18}.}
	\label{fig:Rlattice}
\end{figure}

We believe that the ongoing work we present in this paper may help ease the
points observed in this large scale project:
the categories of Figure~\ref{fig:Rlattice} fit well
into the directed set of Section~\ref{sec:programming:lattice}.
We could thus imagine rewriting the interpreter within this formalism.
As the approach presented in this work is based on a \bindn operation
whose usage is very close to the usual monadic \bindn,
we do not expect this conversion to require a significant amount of work.
During type inference,
\coq will compute where each interpreter function falls within the directed set.
%
This enables a first phase of verification for the programmer:
for instance, if a function is inferred to be of type~\(\top\)
(which in the context of R is expected to be very rare),
something may be wrong and is worth inspecting.
%
Furthermore, each of these categories provides some invariants:
% these can be stated and proven with
the sketches of methodology presented in Section~\ref{sec:reasoning}
should be improved upon to cleanly state, prove, and leverage these invariants.
In the case of this R interpreter,
the size of the interpreter is large compared to the number of monads involved:
we expect to get similar results to the ones presented in a cumbersome way in~\cite{RDLS18},
but with less work.

