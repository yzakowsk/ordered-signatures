In the previous section, we have seen that indexing monads by a directed set
whose order maps to monad morphisms between the reified monad provides an
adequate interface to support heterogeneous programming among those monads.
Looking once again back at Figure~\ref{fig:diamonds}, one gets the feeling that we
did not quite strike the bullseye. First, we had not one but two structured sets
of monads in mind --- the syntactic world with the various free monads, and the
semantic counterpart with their interpretations. Furthermore, signatures
themselves seemed to play a specific role.
In this section, we provide yet another interface, from which we will derive
two instances of the directed one, respectively for the syntax and the semantics.
The full interface is depicted on Figure~\ref{fig:interface-free}: we motivate
and walk through its components in this section. As an additional visual support,
Figure~\ref{fig:diagram:free} depicts the data contained in the interface when
instantiated to our running example.

\begin{figure}
\begin{coqlisting}
(* The directed domain by which we index *)
Variable DD : Type.
Djoin_le_l  : forall d1 d2, d1 LE d1 CUP d2;
Djoin_le_r  : forall d1 d2, d2 LE d1 CUP d2;
Djoin_assoc : forall d1 d2 d3, d1 CUP (d2 CUP d3) = d1 CUP d2 CUP d3;

(* First layer of reification: in terms of signatures *)
Sreify : DD -> signature;
Notation "'[[' II ']]'" := (Sreify II);
(* To the order must correspond injections *)
Sinj : forall d1 d2, d1 LE d2 -> [[d1]] -< [[d2]];
(* Coherence properties of these injections *)
Sinj_proper : forall d1 d2 (I12 I12' : d1 LE d2), Sinj I12 = Sinj I12';
Sinj_refl : forall d (I : d LE d), Sinj I = inject_id;
Sinj_trans : forall d1 d2 d3 (I12 : d1 LE d2) (I23 : d2 LE d3),
	 (Sinj I12) (*@$\circ$@*) (Sinj I23) = Sinj (PreOrder_Transitive _ _ _ I12 I23);

(* Via the free construction, DD reifies into the corresponding free monads *)
Freify d := free [[d]];
Notation "'{{' II '}}'" := (Freify II);
Ffmap d1 d2 (I : d1 LE d2) : {{d1}} ~> {{d2}} := fun m => translate (Sinj I) m;

(* Second layer of reification: in terms of concrete monads *)
Mreify : DD -> monad;
Notation "'<<' II '>>'" := (Mreify II).
Mmonad : forall d, Monad <<d>>;
(* To the order must correspond morphisms *)
Mmorph : forall d1 d2, d1 LE d2 -> MonadMorphism <<d1>> <<d2>>;
(* Coherence properties of these morphisms *)
Mfmap {d1 d2} (I : d1 LE d2) X : <<d1>> ~> <<d2>> :=
	fmap(*@\inferredfmap{\Mreify{d1}}{\Mreify{d2}}@*) (MonadMorphism := Morph (Mmorph I)) X;
Mmorph_proper : forall d1 d2 (I J : d1 LE d2), (Mfmap I) = (Mfmap J);
Mmorph_refl : forall d (I : d LE d) X (m : <<d>> X), Mfmap I m = m;
Mmorph_trans : forall d1 d2 d3 (I12 : d1 LE d2) (I23 : d2 LE d3),
   (Mfmap I12) (*@$\circ$@*) (Mfmap I23) = Mfmap (PreOrder_Transitive I12 I23);

(* Both layers are finally connected by handlers *)
Dh : forall d, [[d]] ~> <<d>>;
IM d : {{d}} ~> <<d>> := interp (Dh d);
(* Climbing in the syntax and then interpreting or interpreting and then
climbing in the semantics should commute *)
IM_commut : forall d1 d2 (I : d1 LE d2) X (m: {{d1}} X),
   IM (Ffmap I m) = Mfmap I (IM m)
\end{coqlisting}

  \caption{Interface for free monadic computations (\filelink[171]{theories/free.v})}
  \label{fig:interface-free}
\end{figure}

\begin{figure}
	\begin{center}
		\begin{tikzpicture}
			\begin{scope}
				\node (Ldecl) {\((\ilc{DD}, \sqsubseteq, \sqcup)\)} ;
				\node [below of = Ldecl] (RW) {\lfont{RW}} ;
				\node [below right of = RW] (W) {\lfont{W}} ;
				\node [below left of = RW] (R) {\lfont{R}} ;
				\node [below left of = W] (V) {\lfont{V}} ;
				\draw[->] (V) -- (W) ;
				\draw[->] (W) -- (RW) ;
				\draw[->] (R) -- (RW) ;
				\draw[->] (V) -- (R) ;
					\node [draw, rectangle, rounded corners, fit=(Ldecl)(RW)(R)(W)(V)] (Lscope) {};
			\end{scope}

			\begin{scope}
				\node [right = 35mm of Ldecl] (Sdecl) {Signatures} ;
				\node [below of = Sdecl] (sRW) {\ilc{Rd +' Wr}} ;
				\node [below right of = sRW] (sW) {\ilc{Wr}} ;
				\node [below left of = sRW] (sR) {\ilc{Rd}} ;
				\node [below left of = sW] (sV) {\ilc{void1}} ;
				\draw[->] (sV) -- (sW) ;
				\draw[->] (sW) -- (sRW) ;
				\draw[->] (sV) -- (sR) ;
				\draw[->] (sR) -- (sRW) ;
				\node [draw, rectangle, rounded corners, fit=(Sdecl)(sRW)(sR)(sW)(sV)] (Sscope) {};
			\end{scope}

			\begin{scope}[node distance = 15mm]
				\node [right = 5cm of Sdecl] (Fdecl) {Free Monads} ;
				\node [below of = Fdecl, yshift = 3ex] (fRW) {\ilc{free (Rd +' Wr)}} ;
				\node [below right of = fRW] (fW) {\ilc{free Wr}} ;
				\node [below left of = fRW] (fR) {\ilc{free Rd}} ;
				\node [below left of = fW] (fV) {\ilc{free void1}} ;
				\draw[->] (fV) -- (fW) ;
				\draw[->] (fW) -- (fRW) ;
				\draw[->] (fV) -- (fR) ;
				\draw[->] (fR) -- (fRW) ;
				\node [draw, rectangle, rounded corners, fit=(Fdecl)(fRW)(fR)(fW)(fV)] (Fscope) {};
			\end{scope}

			\begin{scope}
				\node [below = 4cm of Sdecl] (Mdecl) {Monads} ;
				\node [below of = Mdecl] (mRW) {\ilc{state}} ;
				\node [below right of = mRW] (mW) {\ilc{write}} ;
				\node [below left of = mRW] (mR) {\ilc{read}} ;
				\node [below left of = mW] (mV) {\ilc{pure}} ;
				\draw[->] (mV) -- (mW) ;
				\draw[->] (mW) -- (mRW) ;
				\draw[->] (mV) -- (mR) ;
				\draw[->] (mR) -- (mRW) ;
				\node [draw, rectangle, rounded corners, fit=(Mdecl)(mRW)(mR)(mW)(mV)] (Mscope) {};
			\end{scope}

			\draw[->, thick] (Lscope) -- node [above, sloped] {\ilc{Sreify} / \ilc{[[_]]}} (Sscope.west |- Lscope) ;
			\draw[->, thick] (Sscope) -- node [above, sloped] {\ilc{free}} (Fscope.west |- Sscope) ;
			\draw[->, thick] (Lscope) to [bend right = 20] node [below, xshift = -7ex] {\ilc{Mreify} / \ilc{<<_>>}} (Mscope) ;
			\draw[->, thick] (Sscope) -- node [right] {\ilc{Dh : [[d]] ~> <<d>>}} (Mscope) ;
			\draw[->, thick] (Fscope) to [bend left = 20] node [right, xshift = 2ex] {\ilc{interp Dh}} (Mscope) ;
			\draw[->, thick] (Lscope) to [bend left] node [above] {\ilc{Freify} / \(\Freify{\ilc{\_}}\)} (Fscope) ;
		\end{tikzpicture}
	\end{center}
	\caption{The full picture instantiated on our running example (\filelink[633]{theories/ExampleCell.v})}
	\label{fig:diagram:free}
\end{figure}

A first intuition could be to consider that signatures themselves should form
the indexing structure. After all, the disjoint sum (\ilc{+'}) could be a candidate
for \ilc{join}, while signature inclusion, written \ilc{-<} and defined by the
existence of an injection between the signatures, would constitute a valid order.
However, disjoint sum is much too crude a \ilc{join} --- at the very least, we
would like to look more closely to a set-theoretical join --- and we therefore
still need to introduce a proper pointed set \ilc{DD}.
But on the syntactic side, we are indeed indexing signatures rather than monads: this is
the purpose of the part of Figure~\ref{fig:interface-free} describing the first
layer of reification.
In contrast to the previous case, two ordered indices must here map to reified
signatures such that the smallest one can be injected into the largest one, as
witnessed by \ilc{Sinj}.
We impose three coherence properties on this first layer of reification:
the injections should be proof irrelevant, proofs of self relation should map to
the identity injection, and proofs by transitivity should map to the composition
of their respective injections.

Turning our eye to Figure~\ref{fig:diagram:free}, we have so far covered the
upper part of the figure. The indexing diamond maps to the one containing the
four signatures introduced in Section~\ref{sec:example} via \ilc{Sreify} (also noted \ilc{[[_]]}).
By the \ilc{free} construction, signatures get mapped to all the free monads at
which we might want to write programs --- hence by composition, reification into
signature induces a reification \ilc{Freify} (noted \inlinecoqlst|{{_}}|) into free monads.
Non-explicitly represented are the nature and relationship between the arrows in
each of these three boxes: to a proof of inequality corresponds an injection of
signatures, to which correspond a monad morphism between the corresponding free
monads --- the latter is derived for free.

We now bring into the picture the monads in which we concretely implement our effectful
operations.
This is the purpose of the second layer of reification from
Figure~\ref{fig:interface-free}:
\ilc{Mreify} (also noted \ilc{<<_>>}) maps the same domain of indices \ilc{DD} to
concrete monads. This part of the interface is very close to the one
developed in Section~\ref{sec:programming:lattice}: indices must be reified into
monads, and proofs of inequality must map to monad morphisms.
The coherence properties are identical to the ones for the first layer of reification.
On Figure~\ref{fig:diagram:free}, we have now introduced a fourth diamond,
containing the state monad and its simplified versions, and the left arrow.

It remains only to relate the ``free'' diamond to the concrete one: we need to explain how we can implement
the relevant signatures --- this is captured by \ilc{Dh}.
The user must provide at each index \ilc{d} a handler
\ilc{(Dh d : [[d]] ~> <<d>>)}: the \ilc{Rd} events must be implemented in the
\ilc{read} monad, the \ilc{Rd +' Wr} ones in the state monad, and so on.
Much like the reification into signatures gives rise to a reification into the free
monads, this family of handlers entails a bridge~\InterpM{} % \ilc{IM} % For some reasons, the mathcal makes no effect here.
between the diamond of free
monads and the one of concrete monads via the \ilc{interp} function, completing
the picture.

There remains one unstated coherence condition. Given two indices \ilc{i} and \ilc{j}
such that \ilc{i LE j}, one may now follow two paths, represented in Figure~\ref{fig:Icommut}.
%
On one hand, we can translate the signature \ilc{[[i]]} into \ilc{[[j]]}
(property \ilc{Sinj} in Figure~\ref{fig:interface-free}),
and thus translate any tree in \Freify{\ilc{i}} into \Freify{\ilc{j}}---%
this is the role of the \ilc{translate} function in the definition of \ilc{Ffmap}.
We can then interpret the resulting computation into \ilc{<<j>>}.
%
On the other hand, we may interpret \ilc{[[i]]} into \ilc{<<i>>}, and then
transport \ilc{<<i>>} into \ilc{<<j>>}.
%
Condition \InterpM\ilc{\_commut} ensures that this diagram commutes.

\begin{figure}
\begin{center}
	\begin{tikzpicture}[node distance = 15mm]
		\node (ifree) {\Freify{\ilc{i}}} ;
		\node[right of = ifree, node distance = 4cm] (jfree) {\Freify{\ilc{j}}} ;
		\node[below of = ifree] (imod) {\ilc{<<i>>}} ;
		\node[below of = jfree] (jmod) {\ilc{<<j>>}} ;
		\draw[->, thick] (ifree) -- node [above] {\ilc{translate} / \ilc{Ffmap}} (jfree) ;
		\draw[->, thick] (jfree) -- node [right] {{\InterpM}} (jmod) ;
		\draw[->, thick] (ifree) -- node [left] {{\InterpM}} (imod) ;
		\draw[->, thick] (imod) -- node [above] {\ilc{Mfmap}} (jmod) ;
	\end{tikzpicture}
\end{center}
	\caption{Representation of the \InterpM\ilc{\_commut} property}
	\label{fig:Icommut}
\end{figure}

From an instance of the interface from Figure~\ref{fig:interface-free}, we
derive three essential facts.
The first is an instance of the directed interface indexed by \ilc{DD} \wrt \ilc{Freify}.
The second is an instance of the directed interface indexed by \ilc{DD} \wrt
\ilc{Mreify}.
We can hence sequence heterogeneous computations in both the syntactic and
semantic world: let us write these operations respectively \ilc{Fbind} and \ilc{Mbind}.
The last important result we derive is a theorem for commuting
interpretation with binds in this melting pot:
\begin{coqlisting}
Theorem bind_IM : forall d1 d2 X Y (m : free [[d1]] X) (k : X -> free [[d2]] Y),(*@\filelinkmargin[900]{theories/ExampleCell.v}@*)
   IM (Fbind m k) = Mbind (IM m) (fun x => IM (k x)).
\end{coqlisting}

We have fulfilled the core of our contract by axiomatizing an interface
allowing us to write the tightly typed version
of \ilc{main} without additional crust. But even more importantly, it allows us
to switch the perspective we have on the semantics of a compound computation,
obtained by monadic interpretation, into the composition of the generally
simpler implementation of its sub-components.
Equipped with the minimal ingredients necessary to express and leverage
effect typing information, we now sketch some early avenues to try and take advantage of it.

% In the previous sections, we structured effect signatures with an order and a join operation.
% This is reminiscent of the subevent \ilc{-<} and \ilc{+'} operations of e.g. interaction trees.
% In this section, we show how we can use the effect signatures themselves as a natural indexing lattice.
% %
% We base ourselves on the context of the Freer Monad~\cite{freer},
% but the message would not change if using Interaction Trees~\cite{XZHH+20}
% or FreeSpec~\cite{LetanR20} instead.

% Figure~\ref{fig:diagram:free} shows the different structures involved in this idea,
% instantiated for the running example.
% %
% In Section~\ref{sec:programming:order}, we introduced the domain~\ilc{DD},
% then turned it into a semi-lattice in Section~\ref{sec:programming:lattice}.
% The \ilc{reify} function associates each index to its corresponding monad,
% and the heterogeneous bind manipulates computations across these monads.

% Following the Free monad methodology, we add an indirect step:
% an uninterpreted execution tree.
% Such a tree announces its (uninterpreted) effects within its type,
% enabling easy composition:
% \ilc{Free E X} is a computation returning a value of type~\ilc{X}
% while possibly using the effects~\ilc{E}.
% %
% To manipulate the Free monad within our framework,
% we thus specify what effects associated to each monad:
% the \ilc{Sreify} function of Figure~\ref{fig:diagram:free}
% associates the type of effect to each index.
% %
% It must be compatible with the order:
% if \ilc{II1 LE II2}, then \ilc{Sreify II1 -< Sreify II2}.

% From this, we can build uninterpreted computations manipulating these effects:
% these are free monads, each associated to a given set of effects \ilc{Sreify II}.
% We have built a heterogeneous bind in this structure,
% based on \ilc{bindL} (see Section~\ref{sec:programming:lattice}):
% \begin{coqlisting}
% Definition bindF II1 II2 X Y :(*@\filelinkmargin[275]{theories/free.v}@*)
% 	Free (Sreify II1) X -> (X -> Free (Sreify II2) Y) -> Free (Sreify (II1 CUP II2)) Y.
% \end{coqlisting}

% These free computations then be manipulated as-is, or be interpreted.
% To interpret these trees, we require a handler for each effect:
% this is the \ilc{Dh} function of Figure~\ref{fig:diagram:free}.
% The interpretation function \ilc{interp} of the free monad (\filelink[83]{theories/free.v}),
% given the handler \ilc{Dh}, embeds each free computation to the corresponding monad.
% This interpretation function is usual in the Free monad framework.
% In the diagram, it corresponds to the bottom-right arrow \ilc{interp Dh}.

% As for \ilc{bindL}, the running example requires no annotation to program.
% Given the following function, \coq infers \ilc{free (Sreify RW) bool} as a return type:
% \begin{coqlisting}
% Definition main (n : data) :=(*@\filelinkmargin[882]{theories/ExampleCell.v}@*)
% 	init n ;;
% 	v1 <- fetch ;;
% 	v2 <- fetch ;;
% 	ret (v1 =? v2).
% \end{coqlisting}

% \todo{Examples of signatures, writing up the relationship between this structure
%   over the free monad and the handlers into actual monads}
