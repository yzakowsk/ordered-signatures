As a means to guide our intuition, we consider as a minimal concrete example
stateful computations over a single memory cell as introduced in
Section~\ref{sec:intro}.
To work with such effects, one would typically (1) assemble pieces of
computations written in the \ilc{free (Rd +' Wr)} monad, (2) interpret these
computations into the \ilc{state} monad, and (3) perform any reasoning, of
functional correctness for instance, in this structure.

We propose ourselves to leverage three simple pieces of static
information we may collect: a computation might only read the cell, it might
only write to the cell, or it might perform neither operation.
On the syntactic side, a sufficient condition to ensure these invariants
is to type a piece of computation
in respectively the \ilc{free Rd}, \ilc{free Wr}, or \ilc{free
  void1}\footnote{Where \ilc{void1} is the empty signature.} monad.
We hence have naturally four signatures in mind, organized into a diamond
w.r.t. a form of signature inclusion.

\begin{figure}
\begin{coqlisting}
Definition read : monad := fun X => data -> X.
Definition read_ret X (x : X) := fun c => x.
Definition read_bind X Y (m : read X) (k : X -> read Y) : read Y :=
  fun c => k (m c) c.

Definition write : monad := fun X => X * option data.
Definition write_ret X (x : X) := (x, None).
Definition write_bind X Y (m : write X) (k : X -> write Y) : write Y :=
  let '(x,mw) := m in
  match k x with
  | (y, None)   -> (y, mw)
  | (y, Some w) -> (y, Some w)
  end.

Definition pure : monad := fun X => X.
Definition pure_ret X (x : X) := x.
Definition pure_bind X Y (m : pure X) (k : X -> pure Y) : pure Y := k m.
\end{coqlisting}

  \caption{Read only, write only, and pure monads (\filelink[34]{theories/ExampleCell.v})}
  \label{fig:monads}
\end{figure}

So far, little differs from previous work: we could translate
the simpler signatures into the largest one when combining computations.
However, this would mean that semantically, we still see all computations
through the lens of the overly general state monad.
Having made the existence of these four kinds of typed computations explicit,
the temptation is great to instead interpret each of them into the simplest
structure possible. Figure~\ref{fig:monads} suggests such structures:
read only computations should not need to return an updated
cell; write only ones should not have to take the initial cell as argument;
and pure computation should live in the identity monad.
We omit the formal definitions here (\filelink[542]{theories/ExampleCell.v})
but analogues to \ilc{h_state} can be defined to interpret each signature at
play into its corresponding implementation monad.

We hence have a diamond of free monads, upon which we can climb by translation,
and an isomorphic diamond of concrete monads.
Although less explicit, the intuition according to which monads get ``more
general'' when climbing in the diamond is valid in the concrete one as well.
Here, the arrow between two monads \ilc{M} and \ilc{N} represents a monad
morphism, i.e., an operation \ilc{fmap : M ~> N} commuting with \retn and \bindn.
For instance, the morphism between the \ilc{read} monad and the \ilc{state}
monad simply exposes that the cell has been left untouched:
\begin{coqlisting}
Definition read_state_map: read ~> state := fun X m c => (c, m c).(*@\filelinkmargin[202]{theories/ExampleCell.v}@*)
\end{coqlisting}

\begin{figure}
	\begin{center}
		\begin{tikzpicture}[node distance = 15mm]
			\begin{scope}
				\node (RW) {\ilc{state}} ;
				\node [below right of = RW] (W) {\ilc{write}} ;
				\node [below left of = RW] (R) {\ilc{read}} ;
				\node [below left of = W] (V) {\ilc{pure}} ;
				\draw[->] (V) -- (W) ;
				\draw[->] (W) -- (RW) ;
				\draw[->] (R) -- (RW) ;
				\draw[->] (V) -- (R) ;
  			\node [draw, rectangle, rounded corners, fit=(RW)(R)(W)(V)] (Lscope) {};
			\end{scope}

  		\begin{scope}
				\node [left = 4cm of RW] (fRW) {\ilc{Free (Rd +' Wr)}} ;
				\node [below right of = fRW] (fW) {\ilc{Free Wr}} ;
				\node [below left of = fRW] (fR) {\ilc{Free Rd}} ;
				\node [below left of = fW] (fV) {\ilc{Free void1}} ;
				\draw[->] (fV) -- (fW) ;
				\draw[->] (fW) -- (fRW) ;
				\draw[->] (fV) -- (fR) ;
				\draw[->] (fR) -- (fRW) ;
				\node [draw, rectangle, rounded corners, fit=(fRW)(fR)(fW)(fV)] (Fscope) {};
  		\end{scope}

			\draw[->, thick] (Fscope) to node [above] {\ilc{interp}} (Lscope) ;
      % \begin{scope}
      %   \node [right = 3cm of Ldecl] (Sdecl) {Signatures} ;
      %   \node [below of = Sdecl] (sRW) {\ilc{Rd +' Wr}} ;
      %   \node [below right of = sRW] (sW) {\ilc{Wr}} ;
      %   \node [below left of = sRW] (sR) {\ilc{Rd}} ;
      %   \node [below left of = sW] (sV) {\ilc{void1}} ;
      %   \draw (sV) -- (sW) -- (sRW) -- (sR) -- (sV) ;
      %   \node [draw, rectangle, rounded corners, fit=(Sdecl)(sRW)(sR)(sW)(sV)] (Sscope) {};
      % \end{scope}
		\end{tikzpicture}
	\end{center}
	\caption{Flavors of stateful computations: syntax and semantics}
	\label{fig:diamonds}
\end{figure}

Figure~\ref{fig:diamonds} sums up the eight monads we are tempted
to manipulate, four on the syntactic side, connected via interpretation
to the four on the semantic side. We aim to identify the right interface
necessary to program with no overhead with this heterogeneous syntax,
as well as to benefit from the additional typing annotations to
compose pieces of reasoning in the simplest possible structures.
As a simplistic but illustrative example, we consider the following program.

\begin{coqlisting}
Variable init : cell -> free Wr unit.
Variable fetch : free Rd cell.
Definition main (n : cell) : free (Rd +' Wr) bool :=
  init n ;;
  v1 <- fetch ;;
  v2 <- fetch ;;
  ret (v1 =? v2).
\end{coqlisting}

The goal is twofold. Can we write this syntax, despite \ilc{init} and
\ilc{fetch} living in different syntax than \ilc{main}? And second, can we
frame the right theory allowing us to leverage the typing information
to prove that \ilc{main n} will always return \ilc{true} while keeping
the definitions of \ilc{init} and \ilc{fetch} completely opaque?
We already intuitively observe that the second goal requires us to know how to combine
heterogeneous computations not only across the syntactic diamond, but also across the semantic
one: the interpretation of a heterogeneous bind should commute into a
heterogeneous bind of interpretations into distinct monads.

% We can define monad morphisms, i.e. family of functions


% As a running example, we consider a system equipped with a single memory
% cell---let us say of type \ilc{data}.
% We naturally represent it with the state monad~\cite{wadler1995monads}:
% \begin{coqlisting}
% Definition monad := Type -> Type.

% Definition state : monad := fun X => data -> data * X.

% Definition state_ret : forall X, X -> state X := fun X x c => (c, x).(*@\filelinkmargin[128]{theories/ExampleCell.v}@*)
% Definition state_bind : forall X Y, state X -> (X -> state Y) -> state Y	:=
% 	fun X Y m k c => let (c', x) := m c in k x c'.

% Notation "x <- m1 ;; m2" := (state_bind m1 (fun x => m2)).

% Definition state_read : state data := fun c => (c, c).(*@\filelinkmargin[244]{theories/ExampleCell.v}@*)
% Definition state_write : data -> state unit := fun c' c => (c', ()).
% \end{coqlisting}
% This structure respects the three monadic laws
% (the relation `\(\approx\)' here denotes the functional extensionality):
% \begin{coqlisting}
% Lemma state_ret_bind : forall X (x : X) k,(*@\filelinkmargin[136]{theories/ExampleCell.v}@*)
% 	state_bind (state_ret x) k ~~ k x.
% Lemma state_bind_ret : forall X (x : state X),
% 	state_bind m state_ret ~~ m.
% Lemma state_bind_bind : forall X (x : state X),
% 	state_bind (state_bind m k1) k2 ~~ state_bind m (fun x => state_bind (k1 x) k2).
% \end{coqlisting}
% Within this monad, the programmer can use the \ilc{state_read} and \ilc{state_write} effects to read and write the content of the cell represented within the monad.

% Let us consider what happens if a program part \ilc{foo} never uses the write effect,
% only the read one.
% We could still express this program within the \ilc{state} monad, of course.
% But we could also use the following simpler monad.
% %A use-case could be that this program part was written as part of a library,
% % and that this library did not anticipated the possible use of a writing operation.
% \begin{coqlisting}
% Definition read : monad := fun X => data -> X.

% Definition read_ret : forall X, X -> read X := fun X x c => x.(*@\filelinkmargin[68]{theories/ExampleCell.v}@*)
% Definition read_bind : forall X Y, read X -> (X -> read Y) -> read Y :=
% 	fun X Y m k c => k (m c) c.

% Definition read_read : read data := fun c => c.(*@\filelinkmargin[248]{theories/ExampleCell.v}@*)
% \end{coqlisting}
% To use \ilc{foo : read X} as part of our \ilc{state}-based program,
% we need to define a monad morphism from \ilc{state_read} to the \ilc{state} monad:
% \begin{coqlisting}
% Definition fmap : forall X, read X -> state X :=(*@\filelinkmargin[202]{theories/ExampleCell.v}@*)
% 	fun X m c => (c, m c).
% \end{coqlisting}
% We show that it is indeed a morphism, and that it maps the action \ilc{read_read} into \ilc{state_read}:
% \begin{coqlisting}
% Lemma fmap_ret : forall X (x : X),(*@\filelinkmargin[206]{theories/ExampleCell.v}@*)
% 	fmap (read_ret x) ~~ state_ret x.
% Lemma fmap_bind : forall X Y (m : read X) (k : X -> read Y),
% 	fmap (read_bind m k) ~~ state_bind (fmap m) (fun x => fmap (k x)).

% Lemma fmap_read : fmap read_read ~~ state_read.(*@\filelinkmargin[252]{theories/ExampleCell.v}@*)
% \end{coqlisting}
% The objective of this work is to ease the inference of properties of such a program part \ilc{fmap foo : state X} that are correct by construction.
% In this case, as \ilc{foo} is defined in the \ilc{read} monad, we know that it can't have changed the memory cell.

% Similarly to the \ilc{read} monad, we define a monad for a program part that neither reads nor writes the memory cell:
% \begin{coqlisting}
% Definition pure : monad := fun X => X.

% Definition pure_ret : forall X, X -> pure X := fun X x => x.(*@\filelinkmargin[44]{theories/ExampleCell.v}@*)
% Definition pure_bind : forall X Y, pure X -> (X -> pure Y) -> pure Y := fun X Y m k => k m.
% \end{coqlisting}

% Finally, we define a monad for a program that never reads the memory cell, but is allowed to write.
% It keeps track of the last (if any) writing operation performed on the memory cell:
% \begin{coqlisting}
% Definition write : monad := fun X => X * option data.

% Definition write_ret : forall X, X -> write X := fun X x => (x, None).(*@\filelinkmargin[92]{theories/ExampleCell.v}@*)
% Definition write_bind : forall X Y, write X -> (X -> write Y) -> write Y :=
% 	fun X Y (x, w) k =>
% 		match k x with
% 		| (x, None) -> (x, w)
% 		| (x, Some c) -> (x, Some c)
% 		end.

% Definition write_write : data -> write unit := fun c' => ((), Some c').(*@\filelinkmargin[250]{theories/ExampleCell.v}@*)
% \end{coqlisting}

% \begin{figure}
% 	\begin{center}
% 		\begin{tikzpicture}[node distance = 15mm]
% 			\begin{scope}
% 				\node (RW) {\ilc{state}} ;
% 				\node [below right of = RW] (W) {\ilc{write}} ;
% 				\node [below left of = RW] (R) {\ilc{read}} ;
% 				\node [below left of = W] (V) {\ilc{pure}} ;
% 				\draw[->] (V) -- (W) ;
% 				\draw[->] (W) -- (RW) ;
% 				\draw[->] (R) -- (RW) ;
% 				\draw[->] (V) -- (R) ;
% 			\end{scope}
% % 		\begin{scope}
% % 			\node [right = 3cm of Ldecl] (Sdecl) {Signatures} ;
% % 			\node [below of = Sdecl] (sRW) {\ilc{Rd +' Wr}} ;
% % 			\node [below right of = sRW] (sW) {\ilc{Wr}} ;
% % 			\node [below left of = sRW] (sR) {\ilc{Rd}} ;
% % 			\node [below left of = sW] (sV) {\ilc{void1}} ;
% % 			\draw (sV) -- (sW) -- (sRW) -- (sR) -- (sV) ;
% % 			\node [draw, rectangle, rounded corners, fit=(Sdecl)(sRW)(sR)(sW)(sV)] (Sscope) {};
% % 		\end{scope}
% 		\end{tikzpicture}
% 	\end{center}
% 	\caption{Morphisms between the four presented monads}
% 	\label{fig:example:morphisms}
% \end{figure}

% As for \ilc{read} and \ilc{state}, we can define morphisms between these monads,
% following the diagram of Figure~\ref{fig:example:morphisms}.
% These four monads will be used as illustration in this article.
% %
% In particular, we will consider the following program \ilc{main}.
% It is based on two external program parts \ilc{init} and \ilc{fetch}.
% The \ilc{main} program is defined in the \ilc{state} monad,
% but \ilc{init} and \ilc{fetch} are defined in a submonad, respectively \ilc{write} and \ilc{read}.
% \begin{coqlisting}
% Variable init : data -> write unit.(*@\filelinkmargin[279]{theories/ExampleCell.v}@*)
% Variable fetch : read data.

% Definition main (n : data) : state bool :=
% 	fmap (init n) ;;
% 	v1 <- fmap fetch ;;
% 	v2 <- fmap fetch ;;
% 	state_ret (v1 =? v2).
% \end{coqlisting}

% The \ilc{read} monad restricts how \ilc{fetch} may behave:
% our goal is here to prove that both occurences of \ilc{fetch} returns the same value,
% and thus that \ilc{main} always returns \ilc{true}.
% We can prove this without having to inspect \ilc{fetch}'s source code, which might be large.
% %
% Crucially, this property holds because no call to the \ilc{state_write} operation
% (including through a call to \ilc{init}) stands between both invocations of \ilc{fetch}.
% We are thus basing our reasonning both on the returned type of program parts
% and the precise instruction sequences of the \ilc{main} program.

% Section~\ref{sec:programming} presents various ways in which we can represent the \ilc{main} program
% in order to avoid having to think about the different monads involved here.
% In particular, we would like to avoid having to explicitely call \ilc{fmap}
% when the monads do not match
% (in the example when calling \ilc{init} or \ilc{fetch}).
% To this end, we introduce different heterogeneous bind operators.
% In particular, the ones of Section~\ref{sec:programming:lattice} and~\ref{sec:programming:free}
% not only remove the need of any such \ilc{fmap} annotation within the code,
% but also let \coq infer the minimal type for the \ilc{main} function.
% %
% Section~\ref{sec:reasoning} shows how we can use these representations to derive properties---%
% in this case, that \ilc{main} always returns \ilc{true}.
