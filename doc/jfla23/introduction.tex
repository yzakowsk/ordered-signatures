Monads were initially introduced as a semantic model for effectful
computations~\cite{Moggi89}. Quickly, they have spread beyond this
formal setup to become a popular programming device allowing for safely
encapsulating effects in functional programs~\cite{JonesW93,Wadler95}, taking most
famously a central role in Haskell's design.

Under the lens of programming, a given monad consists of three core components.\footnote{In general, monads should also come equipped with a notion of equivalence of computation. We brush this detail under the rug in this presentation, pretending that we may work with Coq's equality \ilc{eq} everywhere. We refer the interested reader to our formal development for a proper setoid-based approach.}
First, a family of types specifies
a domain of computations: the \ilc{Option} type constructor
captures potentially failing computations, the \ilc{List} type constructor
can be seen as specifying non-deterministic computations, state-threading functions may
encapsulate stateful computations, and so on.
Monads are furthermore equipped with two operations: the \retn construct describes
how to embed a pure computation into the domain of computations, while the \bindn
operation describes how computations can be sequenced
(hence its notation `\ilc{x <- m ;; k}').
Figure~\ref{fig:state} illustrates the \coq definition of a stateful
monad over a single memory \ilc{cell} storing a natural number.

\begin{figure}
\begin{coqlisting}
Definition monad := Type -> Type.

Definition cell : Type := nat.
Definition state : monad := fun X => cell -> cell * X.
Definition state_ret : forall X, X -> state X := fun X x c => (c, x).
Definition state_bind : forall X Y, state X -> (X -> state Y) -> state Y	:=
	fun X Y m k c => let (c', x) := m c in k x c'.
\end{coqlisting}

\caption{The state monad (\filelink[128]{theories/ExampleCell.v})}
\label{fig:state}
\end{figure}

Perhaps more surprisingly, monads offer a solution to Wadler's
expression problem.
% : can one extend the syntax of their
% programming language without having to recompile their interpreter?
The \emph{free monad} (Figure~\ref{fig:free})
acts as an extensible syntax~\cite{Swierstra08} for effectful computations:
effects are thought of as arising from the use of effectful operations
described via an interface \ilc{E}. A computation is then essentially encoded as a
tree where leaves contain pure computations, and nodes contains operations.
The relationship between a node and its children arises from a continuation indexed by the
type of answer expected from a valid implementation of the operation.
%
This tree structure is naturally equipped with a monadic structure:
leaves directly act as \retn operations,
while \bindn attaches the appropriate continuation
to the leaves of the first part of the computation
(\filelink[17]{theories/free.v}).

\begin{figure}
\begin{subfigure}{0.52\textwidth}
\begin{coqlisting}
Definition signature := Type -> Type.
Notation "M ~> N" := (forall X, M X -> N X).
Inductive free (E : signature) (X : Type) :=
  | pure (x : X)
  | op Y (e : E Y) (k : Y -> free E X).

Fixpoint interp E M (MM: Monad M)
	  (h : E ~> M) X (m : free E X) : M X :=
  match m with
  | pure x => ret x
  | op e k =>
		  bind (h e) (fun x => interp h (k x))
  end.
\end{coqlisting}
\end{subfigure}
\hfill
\begin{subfigure}{0.45\textwidth}
\begin{coqlisting}
Variant Rd : signature :=
  | rd : Rd cell.
Variant Wr : signature :=
  | wr (c : cell) : Wr unit.

Definition h_state
  : Rd +' Wr ~> state :=
   fun _ e c => match e with
              | inl1 rd (*@\hspace{4.8ex}@*) => (c,c)
              | inr1 (wr c') => (c',tt)
              end.
\end{coqlisting}
\end{subfigure}

\caption{The free monad (\filelink[5]{theories/free.v}) and its use for stateful computations (\filelink[504]{theories/ExampleCell.v})}
\label{fig:free}
\end{figure}

The right hand-part of Figure~\ref{fig:free} illustrates the approach when declaring computations manipulating
the previously mentioned cell.
The \ilc{rd} event specifies the \emph{read} operation: the fact that one expects to get back
the content of the cell
following this operation is reflected in its type \ilc{rd: Rd cell}.
In contrast, a \emph{write} event \ilc{wr c} is intended to update the content of the cell,
but does not entail any informative answer: its return type is merely an acknowledgement,
embodied by the \emph{unit} type.
From there, one can write rich programs manipulating this memory cell as elements of the monad \ilc{free (Rd +' Wr)}
where \ilc{+'} is the disjoint sum of both signatures. Read operations give rise to an infinite branching, with a branch
per natural number, while write operations have a single child.
In concrete syntax, a computation doubling the current content of the cell becomes:
\begin{coqlisting}
Definition double : free (Rd +' Wr) unit := n <- rd;; wr (2 * n).
\end{coqlisting}

The final crucial ingredient to our story is hidden behind the \emph{free} monad's name:
except for their return type, the operations are free of constraints, no semantics is
attached to them!
Because of this freedom, one can plug'n'play the monadic implementation of one's choice: by folding over the tree,
substituting nodes for their implementations,
the \ilc{interp} function %(\filelink[83]{theories/free.v})
lifts monadic implementations of operations into implementations of computations.
For instance, \ilc{h_state} provides a typical stateful implementation to read and write operations, such that
\ilc{interp h_state double} becomes the expected executable function of an initial cell.

Monadic interpreters built atop of the free monad are an
increasingly popular way to formalize the semantics of programming languages
in proof assistants based on dependent typed theories, such as \coq for instance.
Indeed, via variants of the \emph{delay} monad~\cite{Capretta05,mcbride-free}, divergence
can be internalized, freeing us from Coq's termination checker when writing
monadic definitional interpreters.
When applicable, the approach offers the benefits of compositionality --- in the
traditional sense of denotational semantics ---, of modularity --- effects are
specified in isolation and modularly composed ---, and of executability --- through
extraction to OCaml for instance, testing is made possible, including when dealing with
non-terminating programs.

Specifically in the \coq ecosystem, the approach has been implemented
in the FreeSpec library~\cite{LetanRCH18,LetanR20,freespec} and the Interaction Tree (\itreen)
project~\cite{XZHH+20,YoonZZ22}.
The viability of the method has been shown to scale to realistic settings such as for modelling
LLVM IR~\cite{zakowski2021},
or to support non-determinism and model concurrency~\cite{MohsenXKBCPZ22,CHHZZ23}.
In this context, \emph{formal reasoning} about these monadic computations takes back a central place,
combining the algebraic reasoning enabled by the monadic setup to unary or relational Hoare style reasoning.

The extensibility of the free approach is central to these large scale projects to enable modularity: orthogonal effects are specified in isolation, and theories can be reused across projects.
It is additionally used as a means to painlessly link computations arising from different sources:
one can always \emph{translate} free computations into free computations over larger interfaces.
However, in our experience, these projects have vastly shied away from leveraging the extensibility as a means to facilitate reasoning.
Indeed, signatures can be thought of as a primitive type and effect system.
As such, one could play the game of typing as precisely as possible every computation.
Coming back to our cell example, we should benefit when possible from reflecting in the type of a computation that the cell is never written to, by typing the computation at type \ilc{free Rd X} rather than the larger \ilc{free (Rd +' Wr) X}.
But we should be able to go further: if considering now a stateful computation over several cells, one should be able to reflect in the signature an over-approximation of the read and write location sites of the computation.
Speculating even further, invariants could be encoded in dependent types, specifying for instance that the cell may be written to, but only in an increasing fashion.

\emph{In this work in progress}, we ask the question: is this ambition practical? Can we meaningfully and painlessly combine a zoo of computations of different natures, both as free computations over diverse signatures, but also as their semantic implementations in monads of diverse complexities?
Can we leverage this additional typing information to ease the reasoning in two directions: transporting invariants for free, and allowing for reasoning in simpler monadic structures?

We introduce the following early contributions that lead us believe
these question should find a positive answer:
\begin{itemize}
  \item we observe that monadic computations of distinct natures can be bound,
    provided there is a pair of monad morphisms into a common target (Section~\ref{sec:programming:morphisms});
  \item we propose to index the subset of monads of interest by first a partial order (Section~\ref{sec:programming:order}), then a directed set (Section~\ref{sec:programming:lattice}), in order to remove the need for outrageous explicit type annotations;
  \item moving to a setup based on the free monad, we propose an axiomatized interface to program with the free and concrete views of the indexing domain of computations (Section~\ref{sec:free});
  \item we provide a minimal instance of this interface to the case of a stateful computation over a cell (Section~\ref{sec:example}), and offer some thoughts as to how this work can lead to nicer reasoning principles (Section~\ref{sec:reasoning}).
\end{itemize}

All our results are formalized in \coq\footnote{\url{https://gitlab.inria.fr/yzakowsk/ordered-signatures/-/tree/jfla23/theories}}. This paper can naturally be read as plain text, but we additionally provide hyperlinks
to our source code as a support for the interested reader: those are indicated by a `\filelink{}' symbol.

% Extensibility is at the root of modularity: a computation interacting with the state, formalized as an \ilc{itree MemE X} computation, can be later viewed as, and hence combined with, a computation additionally manipulating registers, i.e. an \ilc{itree (MemE +' RegE) X} computation.
% While the emphasis on this perspective has been extensively explored, we propose to look at it from another lense: extensibility offers the opportunity for tightly typing our computations, and exploiting this static guarantee to derive invariants about our monadic computations. In this work-in-progress report, we describe an attempt at providing an adequate framework to make this perspective practical.

% \todo{
% 	Clearly state our assumptions.
% 	In particular, we assume that our monadic program is large,
% 	and thus that all the work performed to simplify the writing and manipulation of the program is worth it.
% }

% This work has been formalised in \coq and is available at \url{\repobaselink}.
% The Coq snippets shown in this article have been rewritten to help readability.
% Hyperlinks to our \coq repository are scattered along the article,
% indicated as a `\filelink{}' symbol.
