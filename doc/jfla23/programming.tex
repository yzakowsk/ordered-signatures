We set aside the free monad temporarily, and first focus on developing the
right mathematical framework to painlessly
program with monadic computations living in different structures.
This amounts to binding together computations from different monads:
a value \ilc{m : M X} in a first monad \ilc{M}
and a continuation \ilc{k : X -> N Y} in a second monad \ilc{N}.
In other words, we are trying to define a practical structure
with a heterogeneous bind operator with the following type,
for several monads \ilc{M}, \ilc{N}, and \ilc{T}.
\begin{coqlisting}
bind : forall X Y, M X -> (X -> N Y) -> T Y
\end{coqlisting}
We furthermore ask this heterogeneous bind operator extends the usual monadic
laws: \retn should be a unit for \bindn on both sides, and \bindn should be associative.

\subsection{Monad Morphisms to Transport}
\label{sec:programming:morphisms}

We start with a very simple observation:
two computations can always be sequenced as soon as we know how to transport
both arguments into a common monad.
We hence request the existence of monad morphisms from \ilc{M} to \ilc{T} and
from \ilc{N} to \ilc{T} and compose \bindn with the appropriate \ilc{fmap}s.
\coq automatically infers the type of these \ilc{fmap}s,
but for better readability, we annotate them in grey in the code.


% \ilc{m} and \ilc{k} can be sequenced
% using \ilc{T}'s \bindn provided they both know how to be embedded into \ilc{T}.
% We hence request the existence of monad morphisms from \ilc{M} to \ilc{T} and
% from \ilc{N} to \ilc{T} and compose \bindn with the appropriate \ilc{fmap}s.

% As seen in the running example, the usual monadic bind can already accommodate different monads
% as soon as there exists a monad morphism from the arguments of the bind to the monad of the bind:
% we were able to call \ilc{fetch} (returning a value in the \ilc{read} monad)
% from a bind of the \ilc{state} monad using \ilc{fmap}.
% %
% We can define a heterogeneous bind as long as we know how to transport
% both argument monads (\ilc{M} and \ilc{N})
% toward a common third one (\ilc{T}) via monad morphisms.

\begin{coqlisting}
Definition bindH M N T (MM : Monad M) (MN : Monad N) (MT : Monad T) (*@\filelinkmargin[107]{theories/monad.v}@*)
	(MMT : MonadMorphism M T) (MNT : MonadMorphism N T)
  : forall X Y, M X -> (X -> N Y) -> T Y :=
  fun X Y m k => v <- fmap(*@\inferredfmap{M}{T}@*) m;; fmap(*@\inferredfmap{N}{T}@*) (k v).
\end{coqlisting}
% Notation "x @ T <- m1 ;; m2" := (bindH (T := T) m k).
Where \ilc{Monad M} is a type class constraint asserting the existence of the
monadic operations over \ilc{M}, and \ilc{MonadMorphism N T} a type class
constraint ensuring the existence of a morphism \ilc{fmap}\inferredfmap{N}{T}
from \ilc{N} to \ilc{T}.
In the rest of this subsection, we omit these constraints to lighten the presentation.

By identifying all three monads and using the identity morphism,
we show that the heterogeneous bind is indeed an extension of the usual monadic bind.
\begin{coqlisting}
Lemma bindH_extends_bind : forall M X Y (m : M X) (k : X -> M Y),(*@\filelinkmargin[245]{theories/monad.v}@*)
	bindH(*@\inferredbind{M X}{X}{M Y}{M Y}@*) m k = bind m k.
\end{coqlisting}

Furthermore, the monadic laws remain valid, once appropriately generalized. The
left and right \retn laws simply have to manually \ilc{fmap} the unbound pure computations.
\begin{coqlisting}
Lemma bindH_ret_l : forall M N T X Y (x : X) (k : X -> N Y),(*@\filelinkmargin[115]{theories/monad.v}@*)
  bindH(*@\inferredbind{M X}{X}{N Y}{T Y}@*) (ret x) k = fmap(*@\inferredfmap{N}{T}@*) (k x).

Lemma bindH_ret_r : forall M N T X (c : M X),(*@\filelinkmargin[129]{theories/monad.v}@*)
  bindH(*@\inferredbind{M X}{X}{N X}{T X}@*) c (fun x => ret x) = fmap(*@\inferredfmap{M}{T}@*) c.
\end{coqlisting}

The associativity of the heterogeneous bind operator is a bit more of a mouthful.
We need to consider six monads:
three ``base'' monads \texttt{A}, \texttt{B}, and \texttt{C}
for the arguments of the bind (\texttt{mx}, \texttt{kxy}, and \texttt{kyz}),
one general monad into which the final result is computed,
and two pivot monads \texttt{AB} and \texttt{BC} for the intermediary results.
Figure~\ref{fig:monad:tower} represents these structures,
as well as the various morphisms each bind operator requires
to state the lemma.
We furthermore assume that the three subdiagrams of Figure~\ref{fig:monad:tower} commute.

\begin{coqlisting}
Lemma bindH_bindH : forall A B C AB BC T(*@\filelinkmargin[194]{theories/monad.v}@*)
	(commAB : forall X (m : A X), fmap(*@\inferredfmap{AB}{T}@*) (fmap(*@\inferredfmap{A}{AB}@*) m) = fmap(*@\inferredfmap{A}{T}@*) m)
	(commBC : forall X (m : C X), fmap(*@\inferredfmap{BC}{T}@*) (fmap(*@\inferredfmap{C}{BC}@*) m) = fmap(*@\inferredfmap{C}{T}@*) m)
  (commBT : forall X (m : B X), fmap(*@\inferredfmap{AB}{T}@*) (fmap(*@\inferredfmap{B}{AB}@*) m) = fmap(*@\inferredfmap{BC}{T}@*) (fmap(*@\inferredfmap{B}{BC}@*) m)),

  forall X Y Z (mx : A X) (kxy : X -> B Y) (kyz : Y -> C Z),
		bindH (bindH mx kxy) kyz = bindH mx (fun x => bindH (kxy x) kyz).
\end{coqlisting}

% FIXME: wrapfigure doesn’t work here?
\begin{figure}
\begin{center}
\begin{tikzpicture}[node distance = 15mm]
	\node (A) {\texttt{A}} ;
	\node[right of = A] (B) {\texttt{B}} ;
	\node[right of = B] (C) {\texttt{C}} ;
	\node at ($(A)!.5!(B)$) (AmB) {};
	\node[above of = AmB] (AB) {\texttt{AB}} ;
	\node at ($(B)!.5!(C)$) (BmC) {};
	\node[above of = BmC] (BC) {\texttt{BC}} ;
	\node at ($(AB)!.5!(BC)$) (ACB) {};
	\node[above of = ACB] (T) {\texttt{T}} ;
	\draw[->] (A) -- (AB) ;
	\draw[->] (AB) -- (T) ;
	\draw[->] (C) -- (BC) ;
	\draw[->] (BC) -- (T) ;
	\draw[->] (B) -- (AB) ;
	\draw[->] (B) -- (BC) ;
	\draw[->] (A) to[bend left] (T) ;
	\draw[->] (C) to[bend right] (T) ;
\end{tikzpicture}
\end{center}
	\caption{Monads and their morphisms used to state \ilc{bindH_bindH}}
	\label{fig:monad:tower}
\end{figure}

One may ponder: have we already solved our programming challenge, can we not write
\ilc{main} using \ilc{bindH}?
Well yes, but at great cost!
Looking at the type of \ilc{bindH}, the unifying monad \ilc{T} cannot be
inferred from the type of the computations \ilc{m} and \ilc{k}: the programmer
would essentially have to annotate it manually at every binding point.
Furthermore, while the inference of the morphisms involved via instance
declarations would be possible, this definition provides no safeguard to
guide the programmer, they would have to foresee those they will need.
We can do better.

% We assumed above that type classes would be able to automatically infer
% the monad morphisms of each \ilc{bindH} operator,
% and the programmer would only have to provide the return monad \ilc{T}.
% There are certainly ways to do it
% (for instance declaring an instance of morphism for each pair of monads involved),
% but one has to be careful.
% We probably want to declare an instance to deal with reflexive instances,
% like the one we used to state Lemma \ilc{bindH_extends_bind} above.
% Furthermore, we probably want to automate the construction of some morphisms
% by declaring transitivity as an instance.
% Unfortunately, transitivity and reflexivity tend not to mix well together
% as type class instances, and may lead to \coq's type class inference to not terminate.
% %
% We can probably do better.


% With this heterogeneous bind, the running example can be rewritten as follows.
% We for the moment assume that the monad morphisms can be inferred through type classes.
% % However, the returned monad \ilc{T} can't be inferred
% % and has to be explicitly provided by the programmer at each step. % Actually not true in practice: Coq’s type class mechanism is clever than I thought!
% % There can indeed be several valid solutions for \ilc{T}:
% % in the example, the last three \ilc{@ state} annotations
% % could have been replaced to \ilc{@ read} without any issue.
% \begin{coqlisting}
% Definition main (n : data) :=(*@\filelinkmargin[309]{theories/ExampleCell.v}@*)
% 	init n ;;
% 	v1 <- fetch ;;
% 	v2 <- fetch ;;
% 	ret (v1 =? v2).
% \end{coqlisting}

% We have seen that this heterogeneous bind extends the usual monadic bind,
% but we would also like it to follow some practical intuition from the programmer.
% Conveniently, we can also state and prove a generalisation of the usual three monadic laws.
% \begin{coqlisting}
% Lemma bindH_ret_l : forall M N T(*@\filelinkmargin[115]{theories/monad.v}@*)
% 	(MM : Monad M) (MN : Monad N) (MT : Monad T)
% 	(FMT : MonadMorphism M T) (FNT : MonadMorphism N T),
%   forall X Y (x : X) (k : X -> N Y),
%     bindH (M := M) (T := T) (ret x) k ~~ fmap (k x).

% Lemma bindH_ret_r : forall M N T(*@\filelinkmargin[129]{theories/monad.v}@*)
% 	(MM : Monad M) (MN : Monad N) (MT : Monad T)
% 	(FMT : MonadMorphism M T) (FNT : MonadMorphism N T),
%   forall X (c : M X),
%     bindH (N := N) c (fun x => ret x) ~~ fmap c.
% \end{coqlisting}

% To state the associativity of the heterogeneous bind operator,
% we need to consider six monads:
% three ``base'' monads \texttt{A}, \texttt{B}, and \texttt{C}
% for the arguments of the bind (\texttt{mx}, \texttt{kxy}, and \texttt{kyz}),
% one general monad where the final result is computed,
% and two pivot monads \texttt{AB} and \texttt{BC} for the intermediary result.
% They are represented in Figure~\ref{fig:monad:tower},
% as well as the different morphisms each bind operator require
% to state the lemma.
% We furthermore assume that the three subdiagrams of Figure~\ref{fig:monad:tower} commute.

% \begin{coqlisting}
% Lemma bindH_bindH : forall A B C AB BC T(*@\filelinkmargin[194]{theories/monad.v}@*)
%   (MA : Monad A) (MB : Monad B) (MC : Monad C)
%   (MAB : Monad AB) (MBC : Monad BC) (MT : Monad T)

%   (FAAB : MonadMorphism A AB) (FBAB : MonadMorphism B AB)
%   (FBBC : MonadMorphism B BC) (FCBC : MonadMorphism C BC)
%   (FABT : MonadMorphism AB T) (FBCT : MonadMorphism BC T)
%   (FAT  : MonadMorphism A T)  (FCT  : MonadMorphism C T)

%   (commAB : forall (X : Type) (m : A X),
%       fmap (fmap (N := AB) m) ~~ fmap (N := T) m)
%   (commBC : forall (X : Type) (m : C X),
%       fmap (fmap (N := BC) m) ~~ fmap (N := T) m)
%   (commBT : forall (X : Type) (m : B X),
%       fmap (N := T) (fmap (N := AB) m) ~~ fmap (N := T) (fmap (N := BC) m)),

%   forall X Y Z (mx : A X) (kxy : X -> B Y) (kyz : Y -> C Z),
% 		bindH (bindH mx kxy) kyz ~~ bindH mx (fun x => bindH (kxy x) kyz).
% \end{coqlisting}

\subsection{A Partial Order to Index}
\label{sec:programming:order}

While the bind operator from Section~\ref{sec:programming:morphisms}
captures semantically what we are looking for,
its generality, working over arbitrary monads, leaves both our type checker and
our programmer quite in the dark. Looking back at Figure~\ref{fig:diamonds}, we had
identified the constellation of monads at play starting from observing that we
might work with some signatures (\ilc{void1}, \ilc{Rd}, \ilc{Wr}, \ilc{Rd +'
  Wr}), and that these signatures are naturally ordered by inclusion.

\begin{figure}
\begin{coqlisting}
Variable DD : Type.
Variable reify : DD -> monad.
Notation "'[[' II ']]'" := (reify II).
Hypothesis reify_into_monads : forall II, Monad [[II]].

Variable reify_le : forall II1 II2, II1 LE II2 -> MonadMorphism [[II1]] [[II2]].
Definition climbPO II1 II2 (I12 : II1 LE II2) : forall X, [[II1]] X -> [[II2]] X :=
   fmap (MonadMorphism := reify_le I12).

Hypothesis reify_le_trans :
forall II1 II2 II3 (I12 : II1 LE II2) (I13 : II1 LE II3) (I23 : II2 LE II3) X (m : [[II1]] X),
	climbPO I23 (climbPO I12 m) = climbPO I13 m.
\end{coqlisting}

  \caption{Partial Order interface (\filelink[43]{theories/po.v})}
  \label{fig:interface-po}
\end{figure}

We follow this intuition by requesting the user to provide the interface
described in Figure~\ref{fig:interface-po}.
We assume a domain of indices~\ilc{DD} equipped with a partial order \ilc{LE}
--- for our running example, we build the four valued diamond \{\ilc{v,r,w,rw}\}.
% With the aim to improve our heterogeneous bind operator,
% we need to structure the monads it manipulates:
% the type \ilc{Type -> Type} is extremely large and includes types that the programmer will never use.
% We thus suppose that the programmer has to declare a finite set of monads.
%
% To represent this in \coq, we assume a finite type of indices~\ilc{DD},
Indices are mapped to the monads of interest via a \ilc{reify} function (written
\ilc{[[ _ ]]}).

Intuitively, the ordering placed over our indices abstracts the ability to
transport computations across monadic structures. We formalize this requisite in
the \ilc{reify_le} field: we request the user to explicit how to map proofs of
inequality between indices to monad morphisms between their respective reification.
We define \ilc{climbPO} as the function associating the corresponding \ilc{fmap} to
a given inequality.
Finally, we must ensure some coherence: given two monads \ilc{M} and \ilc{N},
we intuitively want to consider at most one way to transport computations
from \ilc{M} to \ilc{N}. In particular, the provided morphisms must be
compatible with the associativity of the partial order, as spelled out in \ilc{reify_le_trans}.

Given such an interface, we can provide a specialization of
Section~\ref{sec:programming:morphisms}'s operation: rather than provide a
common target monad and morphisms, all we need is to pick an index above the
two indexes involved in the computations being sequenced.
\begin{coqlisting}
Definition bindPO II1 II2 II3 (LT1 : II1 LE II3) (LT2 : II2 LE II3) :(*@\filelinkmargin[68]{theories/po.v}@*)
  forall X Y, [[II1]] X -> (X -> [[II2]] Y) -> [[II3]] Y :=
  bindH (MMT := reify_le LT1) (MNT := reify_le LT2).
\end{coqlisting}

As before, the extended monadic laws remain valid. But where they used to
depend on arbitrary morphisms, they are now specialized to ordered indices:
everything relies on the canonical morphism specified by \ilc{reify_le}.

\begin{coqlisting}
Lemma bindPO_ret_l : forall II1 II2 II3 (I13 : II1 LE II3) (I23 : II2 LE II3),(*@\filelinkmargin[73]{theories/po.v}@*)
	forall X Y (k : X -> [[II2]] Y) (x : X),
    bindPO I13 I23 (ret x) k = climbPO I23 (k x).

Lemma bindPO_ret_r : forall II1 II2 II3 (I13 : II1 LE II3) (I23 : II2 LE II3),(*@\filelinkmargin[81]{theories/po.v}@*)
	forall X (m : [[II1]] X),
    bindPO I13 I23 m (ret (X := X)) = climbPO I13 m.

Lemma bindPO_bindPO : forall II1 II2 II3 II4 II5 II6,(*@\filelinkmargin[89]{theories/po.v}@*)
	(I16 : II1 LE II6) (I46 : II4 LE II6) (I56 : II5 LE II6) (I36 : II3 LE II6)
	(I14 : II1 LE II4) (I24 : II2 LE II4) (I25 : II2 LE II5) (I35 : II3 LE II5),
	forall X Y Z (m : [[II1]] X) (k1 : X -> [[II2]] Y) (k2 : Y -> [[II3]] Z),
    bindPO I46 I36 (bindPO I14 I24 m k1) k2
		= bindPO I16 I56 m (fun x => bindPO I25 I35 (k1 x) k2).
\end{coqlisting}

\begin{figure}
  \begin{center}
    \begin{tikzpicture}[node distance = 10mm]
      \node (i1) {\texttt{\lindex1}} ;
      \node[right of = i1] (i2) {\texttt{\lindex2}} ;
      \node[right of = i2] (i3) {\texttt{\lindex3}} ;
      \node at ($(i1)!.5!(i2)$) (i12) {};
      \node[above of = i12] (i4) {\texttt{\lindex4}} ;
      \node at ($(i2)!.5!(i3)$) (i23) {};
      \node[above of = i23] (i5) {\texttt{\lindex5}} ;
      \node at ($(i4)!.5!(i5)$) (i45) {};
      \node[above of = i45] (i6) {\texttt{\lindex6}} ;
      \draw (i1) -- (i4) -- (i6) -- (i5) -- (i3) ;
      \draw (i4) -- (i2) -- (i5) ;
    \end{tikzpicture}
  \end{center}
	\caption{Hasse Diagram of the order used in \ilc{bindPO_bindPO}}
	\label{fig:po:tower}
\end{figure}

As for \ilc{bindH_bindH}, the associativity law is based on a hierarchy of monads.
But this time, their relation is expressed as an ordering of their indices,
represented in Figure~\ref{fig:po:tower}. Interestingly, the hypotheses of
commutativity are now systematic consequences of \ilc{reify_le_trans}, and can
therefore be omitted.

At its core, no work has of course been saved, all the morphisms at play must still be
provided.
But the necessary proof obligations are now cleanly
identified, and the arguments to \bindn and to the monadic laws are now drawn
from a much simpler structure.

We have shed some light on how to structure the constructions at hand, but have
not addressed the other running issue: the unifying monad \ilc{T} from
Section~\ref{sec:programming:morphisms} has now become a unifying index
\ilc{i3}, but it still needs to be provided.
We remove this thorn in the side by adding slightly more structure to our indexing domain.

% The question is now how to structure this domain~\ilc{DD}.

% The pyramid of morphisms of Figure~\ref{fig:monad:tower}
% hints that the domain~\ilc{DD} should be ordered.
% Following this intuition can be fruitful to structure better the objects we work this,
% keeping in aim automation.
% We accordingly assume (hypothesis \ilc{reify_le} below)
% that an order relation between two elements of~\ilc{DD}
% yields a morphism between the associated monads.

% The picture remains essentially the same, but looks quite a bit cleaner:
% once the user has established \ilc{reify_le}, constraints now amount to checking
% the order relation on indexes rather than looking for monad morphisms.
% This is implemented by \ilc{climbPO} below.

% \begin{coqlisting}
% Variable reify : DD -> monad.(*@\filelinkmargin[53]{theories/po.v}@*)
% Notation "'[[' II ']]'" := (reify II).
% Hypothesis reify_into_monads : forall II, Monad [[II]].
% Hypothesis reify_le : forall II1 II2, II1 LE II2 -> MonadMorphism [[II1]] [[II2]].

% Definition climbPO II1 II2 (I12 : II1 LE II2) :
% 	forall X, [[II1]] X -> [[II2]] X :=
% 	  fmap (MonadMorphism := reify_le I12).
% \end{coqlisting}

% Our heterogeneous bind operator becomes as below.
% It now only takes as argument the order relations between the provided indices.
% \begin{coqlisting}
% Definition bindPO II1 II2 II3 (LT1 : II1 LE II3) (LT2 : II2 LE II3) :(*@\filelinkmargin[68]{theories/po.v}@*)
% 	forall X Y, [[II1]] X -> (X -> [[II2]] Y) -> [[II3]] Y :=
% 		bindH (MMT := reify_le LT1) (MNT := reify_le LT2).
% \end{coqlisting}

% As before, we can extend the monadic laws to this heterogeneous bind.
% \begin{coqlisting}
% Lemma bindPO_ret_l : forall II1 II2 II3 (I13 : II1 LE II3) (I23 : II2 LE II3),(*@\filelinkmargin[73]{theories/po.v}@*)
% 	forall X Y (k : X -> [[II2]] Y) (x : X),
%     bindPO I13 I23 (ret x) k ~~ climbPO I23 (k x).

% Lemma bindPO_ret_r : forall II1 II2 II3 (I13 : II1 LE II3) (I23 : II2 LE II3),(*@\filelinkmargin[81]{theories/po.v}@*)
% 	forall X (m : [[II1]] X),
%     bindPO I13 I23 m (ret (X := X)) ~~ climbPO I13 m.
% \end{coqlisting}

% As for \ilc{bindH_bindH}, the associativity law is based on a hierarchy of monads.
% This time, their relation is expressed as an ordering of their indices,
% represented in Figure~\ref{fig:po:tower}.
% \begin{coqlisting}
% Lemma bindPO_bindPO : forall II1 II2 II3 II4 II5 II6,(*@\filelinkmargin[89]{theories/po.v}@*)
% 	(I16 : II1 LE II6) (I46 : II4 LE II6) (I56 : II5 LE II6) (I36 : II3 LE II6)
% 	(I14 : II1 LE II4) (I24 : II2 LE II4) (I25 : II2 LE II5) (I35 : II3 LE II5),
% 	forall X Y Z (m : [[II1]] X) (k1 : X -> [[II2]] Y) (k2 : Y -> [[II3]] Z),
%     bindPO I46 I36 (bindPO I14 I24 m k1) k2
% 		~~ bindPO I16 I56 m (fun x => bindPO I25 I35 (k1 x) k2).
% \end{coqlisting}

% To prove this law, we furthermore need to assume that \ilc{reify_le} is compatible
% with the transitivity of the order \ilc{LE}.
% Indeed, if \ilc{II1 LE II2 LE II3}, then they are two ways to go
% from \ilc{reify II1} to \ilc{reify II3} using \ilc{climbPO}:
% either directly using the fact that \ilc{II1 LE II3},
% or using \ilc{reify II2} as an intermediary monad.
% This hypothesis states that these two ways lead the same result.
% \begin{coqlisting}
% Hypothesis reify_le_trans :(*@\filelinkmargin[64]{theories/po.v}@*)
% 	forall II1 II2 II3 (I12 : II1 LE II2) (I13 : II1 LE II3) (I23 : II2 LE II3) X (m : [[II1]] X),
% 		climbPO I23 (climbPO I12 m) ~~ climbPO I13 m.
% \end{coqlisting}

\subsection{A Directed Set to Resolve Indecisiveness}
\label{sec:programming:lattice}

The heterogeneous bind still has too much freedom: from having the luxury of
picking the target monad \ilc{T} of its choice, it can now pick its favourite
target index \ilc{i3}.
As a consequence, programmers have to either
manually specify \ilc{II3} for each call of \ilc{bindPO},
or to explicit the return type of each bind operation.

This freedom feels superfluous when coding: taking inspiration from our running
example once again, it seems always safe to look at our index domain as having
the structure of a semi-lattice and taking as target the join of the indices involved.
In practice, picking the smallest upper bound does not matter for soundness, all
we care about is to have a canonical way to chose a binary upper bound: we
request the user to additionally equip their partial order with the structure of a directed set.

\begin{coqlisting}
Variable join : DD -> DD -> DD.(*@\filelinkmargin[18]{theories/lattice.v}@*)
Infix "CUP" := join.
Hypothesis join_le_l : forall II1 II2, II1 LE II1 CUP II2.
Hypothesis join_le_r : forall II1 II2, II2 LE II1 CUP II2.
\end{coqlisting}

% The partially ordered domain of index cleans up things.
% However, the type of \ilc{bindPO} does not constraint \ilc{II3},
% which thus can't be inferred by \coq without further indication:
% \begin{coqlisting}
% bindPO : forall II1 II2 II3 (LT1 : II1 LE II3) (LT2 : II2 LE II3) [X Y],
% 	[[II1]] X -> (X -> [[II2]] Y) -> [[II3]] Y
% \end{coqlisting}
% As a consequence, programmers have to either
% manually specify \ilc{II3} for each call of \ilc{bindPO},
% or to explicit the return type of each bind operation.

% In a lot of usages \ilc{II3} feels inferable, though:
% in the running example, if \(\ilc{II1} = \lfont{V}\) and \(\ilc{II2} = \lfont{R}\),
% then clearly \(\ilc{II3} = \lfont{R}\) is a better choice,
% as \ilc{[[r]] = read} can be mapped to \ilc{[[rw]] = state} if needed be.
% %
% We thus ask the programmer to provide a join operation
% deducting the canonical destination \ilc{II3} from \ilc{II1} and \ilc{II2}.
% %
% This join can then be used by \coq to infer the \ilc{II3} argument of each heterogeneous bind,
% removing the need to manually specify it later on.
% %
% In almost all cases, the specified \ilc{II3} will be the smallest index
% greater than both \ilc{II1} and \ilc{II2}:
% this join operation defines a semi-lattice in practice
% (but we technically only require a directed set).

% Technically, we do not need to require the join operation to be precise:
% \ilc{II1 CUP II2} must be both greater than \ilc{II1} and \ilc{II2},
% but does not have to be the least such value.

% \begin{coqlisting}
% Variable join : DD -> DD -> DD.(*@\filelinkmargin[18]{theories/lattice.v}@*)
% Infix "CUP" := join.
% Hypothesis join_le_l : forall II1 II2, II1 LE II1 CUP II2.
% Hypothesis join_le_r : forall II1 II2, II2 LE II1 CUP II2.
% \end{coqlisting}

The heterogeneous bind specialises \ilc{bindPO} with the two inequalities about \ilc{CUP}.
This time \coq can infer all type arguments from \ilc{(m : [[II1]] X)}
and \ilc{(k : X -> [[II2]] Y)},
discharging a lot of annotations from the programmer.
\begin{coqlisting}
Definition bindL II1 II2 :(*@\filelinkmargin[62]{theories/lattice.v}@*)
	forall X Y, [[II1]] X -> (X -> [[II2]] Y) -> [[II1 CUP II2]] Y :=
		bindPO (join_le_l II1 II2) (join_le_r II1 II2).
\end{coqlisting}

\begin{wrapfigure}{r}{1.9in}
  \vspace{-0.3cm}
\begin{coqlisting}
Definition main (n : data) :=
  init n ;;
  v1 <- fetch ;;
  v2 <- fetch ;;
  ret (v1 =? v2). (*@\filelinkmargin[487]{theories/ExampleCell.v}@*)
\end{coqlisting}
\vspace{-0.3cm}
\end{wrapfigure}
The \ilc{main} function constituting our running example can thus be written
with the desired syntax, with no additional notation, provided that the extended
interface is provided. We furthermore observe that the user can provide
a reification of the indexing diamond into either the free diamond
(left part in Figure~\ref{fig:diamonds}) or the semantic diamond (right part in Figure~\ref{fig:diamonds}).
Depending on their choice, the exact same syntax, depicted to the right
% No annotation is required for binds nor for the returned type of the \ilc{main}:
% the fact that \ilc{main} lives in the \ilc{state} monad is directly inferred from the join operator,
% which follows the monad hierarchy of Figure~\ref{fig:example:morphisms}.
will be valid against opaque computations \ilc{init} and \ilc{fetch} living in
respectively \ilc{free Wr} and \ilc{free Rd}, or in \ilc{write} and \ilc{read}.
Its return type will be directly inferred as respectively \ilc{free (Rd +' Wr)} or
\ilc{state} from the joins involved.

We have overlooked one detail in the code above: the typing of \ilc{ret (v1 =?
  v2)} remains ambiguous.
However this is easily resolved. By definition, \ilc{ret} performs no effect in the monads,
hence if the lattice is equipped with a \(\bot\)~element (typically reified into the \ilc{pure} monad),
then it is safe to always use this element as an index for \ilc{ret}:
we can thus redefine \ilc{ret} to always live in this ``minimal'' monad.

In order to guide the type inference when performing a bind, we have added a computational device,
\ilc{join}, to compute the index governing the monadic type of the result.
Pandora's box is open, we are performing non-trivial dependent programming.
The consequences show up the moment we turn our attention to the monadic laws
for \ilc{bindL}.
Things remain straightforward for the two \retn laws:
\begin{coqlisting}
Lemma bindL_ret_l : forall II1 II2,(*@\filelinkmargin[68]{theories/lattice.v}@*)
	forall X Y (k : X -> [[II2]] Y) (x : X),
		bindL (ret (M := [[II1]]) x) k = climbPO (join_le_r II1 II2) (k x).

Lemma bindL_ret_r : forall II1 II2,(*@\filelinkmargin[75]{theories/lattice.v}@*)
	forall X (m : [[II1]] X),
		bindL m (ret (M := [[II2]])) = climbPO (join_le_l II1 II2) m.
\end{coqlisting}

But the obvious statement for associativity does not even type check!
\begin{coqlisting}
Fail Lemma bindL_bindL_fail : forall II1 II2 II3,
	forall X Y Z (m : [[II1]] X) (k1 : X -> [[II2]] Y) (k2 : Y -> [[II3]] Z),
		bindL (bindL m k1) k2 = bindL m (fun x => bindL (k1 x) k2).
\end{coqlisting}
Indeed, the expression \ilc{bindL (bindL m k1) k2} has type \ilc{[[(II1 CUP II2) CUP II3]] Z}
while the expression \ilc{bindL m (fun x => bindL (k1 x) k2)} has type \ilc{[[II1 CUP (II2 CUP II3)]] Z}.
In other words, we need to assume the associativity of our domain of indices to express the associativity of our heterogeneous bind.
\begin{coqlisting}
Hypothesis join_assoc : forall II1 II2 II3,  II1 CUP (II2 CUP II3) = (II1 CUP II2) CUP II3.(*@\filelinkmargin[21]{theories/lattice.v}@*)
\end{coqlisting}
Luckily, \ilc{climbPO} allows us to follow any inequality proof along our index
domain. It can hence in particular be used to transport equality proofs in our
types: by instantiating it over \ilc{join_assoc}, we get the \ilc{climb_assoc}
transport function (we omit its code here) needed to state the bind associativity.
% on this hypothesis enables us to define the conversion function below.
% This function lets us express the associativity law of our heterogeneous bind.
\begin{coqlisting}
Definition climb_assoc : forall II1 II2 II3 : [[II1 CUP (II2 CUP II3)]] ~> [[(II1 CUP II2) CUP II3]].(*@\filelinkmargin[88]{theories/lattice.v}@*)

Lemma bindL_bindL : forall II1 II2 II3,(*@\filelinkmargin[92]{theories/lattice.v}@*)
	forall X Y Z (m : [[II1]] X) (k1 : X -> [[II2]] Y) (k2 : Y -> [[II3]] Z),
		bindL (bindL m k1) k2 = climb_assoc (bindL m (fun x => bindL (k1 x) k2)).
\end{coqlisting}

% Given that the hypothesis \ilc{join_assoc} above is an equality and not just an inequality,
% we can even go further and remove the \ilc{climbL_assoc},
% but at the cost of a rewriting of the types within the lemma statement.
% The \ilc{rew} notation below comes from the \ilc{EqNotations} module of the standard library.
% Both ways to express this law can be useful depending on the context.

% \begin{coqlisting}
% Lemma bindL_bindL : forall II1 II2 II3,(*@\filelinkmargin[122]{theories/lattice.v}@*)
% 	forall X Y Z (m : [[II1]] X) (k1 : X -> [[II2]] Y) (k2 : Y -> [[II3]] Z),
% 		bindL (bindL m k1) k2
% 		~~ rew [fun II => [[II]] Z] join_assoc II1 II2 II3 in
% 		  bindL m (fun x => bindL (k1 x) k2).
% \end{coqlisting}

At this point, we have reached a first milestone: we have identified a suitable
interface to program in a heterogeneous way across a set of monads organised as
the reification of a pointed set. We now turn our attention back to the free monad.

