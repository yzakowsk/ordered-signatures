\documentclass{article}

\usepackage{xcolor}
\usepackage{xspace}
\usepackage{listings,lstlangcoq}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{todonotes}

\lstdefinestyle{customcoq}{
  columns=flexible,
  mathescape=true,
  belowcaptionskip=1\baselineskip,
  breaklines=true,
  xleftmargin=\parindent,
  language=Coq,
  morekeywords={Variant, fun, Arguments, Type, cofix},
  %morekeywords={SOCKAPI,ITREE,data_at,data_at_},
  emph={%
    SOCKAPI,ITree,data_at,data_at_
  },
  emphstyle={\bfseries\color{green!40!red!80}},
  showstringspaces=false,
  basicstyle=\small\ttfamily,
  keywordstyle=\bfseries\color{green!20!black},
  commentstyle=\itshape\color{red!40!black},
  identifierstyle=\color{violet!50!black},
  stringstyle=\color{orange},
  escapeinside={<@}{@>}
}

\newcommand{\namedsection}[1]{\lstinputlisting[
        style=customcoq,
        basicstyle=\footnotesize\ttfamily,
        rangebeginprefix=(*\ begin\ ,%
        rangebeginsuffix=\ *),%
        rangeendprefix=(*\ end\ ,%
        rangeendsuffix=\ *),
        includerangemarker=false,
        linerange={#1},]{./ICFPPaper.v}}

\newcommand{\inlinecoq}[1]{\mbox{\lstinline[style=customcoq,columns=fixed,basewidth=.48em]{#1}}}
\newcommand{\ilc}[1]{\inlinecoq{#1}}

\newcommand{\Rlang}{R\xspace}
\newcommand{\event}[1]{\ensuremath{\llbracket #1 \rrbracket}}

\newcommand\unit{\ilc{()}}

\newcommand\mb[1]{\todo[size=\scriptsize,color=violet!20!white]{MB: #1}}
\newcommand\yz[1]{\todo[size=\scriptsize]{YZ: #1}}

\begin{document}

\title{On ordering the powerset of effects of a computations into a semi-lattice}

\author{Martin Bodin \and Yannick Zakowski}
\date{}

\maketitle

Idea suggested by Martin, let's try to first pitch it at a high level, and then
sketch some technical details.

Suppose I model a complex language language such as \Rlang using interaction
trees. I will cook up a significant inventory of effects that an \Rlang
computation may produce. In particular among those, a significant part will
interact with the heap.

The usual approach consists in collecting all events interacting with the heap
into a single family of types, define an appropriate monad correctly modelling
the full memory model supporting all these effects, and interpreting this pack
at once into this structure, through an handler.

Now a given piece of computation -- either an open sub-syntactic-component of
the semantics, or a particular \Rlang program -- may be a much simpler object:
we may statically know that it can only emit a strict subset of these
heap-modifying effects; it may even be guaranteed to leave the heap untouched.
In this spirit, Martin would like to associate the tightest effect-signature
possible to bits of computations, derive adequate invariants as a consequence of
this static typing, and still combine all these nice people together.

Injecting smaller signature into bigger contexts is already routinely possible:
one simply uses the \ilc{translate} function. But currently, this is done in a completely adhoc
fashion, and thought of as a way to simply embed computations in context, while
they will eventually all be interpreted into the same monad. Invariants of
peculiar bits of syntax can already be proved, but as Hoare predicates into the
target monad.

The question here is: can we push the idea further by having:
\begin{itemize}
  \item a structured semi-lattice of signatured that gets reified into type
    families of events;
  \item an heterogeneous bind collecting through this lattice the join of the
    signatures;
  \item a different monadic structure interpreting each reified family of events
    differently, itself structured into a semi-lattice;
  \item a clean theory to connect these components?
\end{itemize}

\section{Making things a bit more precise}

Let $(L,\sqsubseteq)$ be a semi-lattice, where each point models a signature for
a computation. Assume $\event{\cdot}$ a function reifying $L$ into \ilc{Type -> Type}.
Let us write $E \subseteq F$ when \ilc{E -< F}.
We posit:

\begin{itemize}
\item $x \sqsubseteq y \rightarrow \event{x} \subseteq \event{y}$
\item $\event{\bot} \equiv \mathtt{void1}$
\item $\event{x \sqcup y} \subseteq \event{x} +' \event{y}$
\end{itemize}

With a bit of work, this structure is sufficient to write a first interesting
piece: a \ilc{bind} heterogeneous in the granularity of interfaces exposed by
computations.
% Dunno how to escape stuff properly, it's a mess, whatev
\ilc{bind (x : itree}
$\event{\ilc{d1}}$
\ilc{ T) (k : T -> itree}
$\event{\ilc{d2}}$
\ilc{ U) : itree}
$\event{\mathtt{d1}\sqcup\mathtt{d2}}$
\ilc{)) U}
%
As well as a \(\ilc{ret}: T \to \ilc{itree}~\event{\top}~T\).
These \ilc{bind} and \ilc{ret} operations follow the monad laws if we fix a given \(\event{d}\).

Now let's say we also ask for some structure on the zoo of monads used to
implement all of these. We first need to associate a monad to each element of
the lattice, and be able to interpret a computation over an interface reified
from an element $x$ into the associated monad:
$$M_L : L \rightarrow (Type \rightarrow Type) \rightarrow Type \rightarrow Type$$
$$h_L : \forall (x:L),~\event{x}\leadsto M_L~x~\event{x}$$
\mb{I don’t understand why \(M_L\) takes the \(\event{x}\) as parameter.
  In particular, are there cases in which we want to consider an expression
	of the form \(M_L~x~\event{y}\) with \(x \neq y\)?}
\yz{I don't think it's necessary, but I had in mind it might be convenient to
  not overly tighten too early the ambient external interactions available}
with \(E_1 \leadsto E_2 \equiv \forall T, E_1~T \to E_2~T\).

This should allow us to define the interpretation of our bits of computations at
various interfaces as follows:

$$ interp_L~(t : itree~\event{x}~X)~: M_L~x~\event{x} := ~\ilc{interp}~(h_L~x)~t$$

We need to ensure that $h_L$ and $M_L$ respects some structure. First, we are
looking for some kind of lattice-like structure over the monads at play. The
order relation should correspond to the existence of a monad morphism:

$$ \forall x~ y,~ x \sqsubseteq y \rightarrow \exists F:~\forall~E~,M_L~x~E
\leadsto M_L~y~E$$

It is a bit unclear how we want to define this family $F$. Clearly we do not
mean this existential to be a propositional one, we want this $F$ to be some
data provided.
Should it be a
$$F~:~\forall x~y~X, x \sqsubseteq y \rightarrow
~M_L~x~\event{x}~X\rightarrow M_L~y~\event{y}~X$$
? Can we avoid this disgusting dependent type?
\mb{If we remove the event argument, it becomes \(F: \forall x~y, x \sqsubseteq y \to M_L~x \leadsto M_L~y\).
	I don’t think that we can nicely remove the \(x \sqsubseteq y\) argument:
	to me, this is pretty fundamental to the approach here.}
\yz{Right, the only hope would be if we can give a sense of this $F$ for all
  $x,y$, but only prove that it soundly transport things under the assumption
  that $x \subseteq y$. But I lean as well toward your opinion, we will probably
end up with a dependent function}

Intuitively at least, we certainly want this lifting function $F$ to be the
identity if we do not move in the lattice, i.e.: $ \forall x, ~F~x~x~X == id$.

We then get to the interaction between the handlers and this lifting.
We want a commuting diagram expressing that if my interface $x$ is below
another interface $y$, then I can either:

\begin{itemize}
\item first lift this computation via \ilc{embed} (since $x\sqsubseteq y \rightarrow \event x \subseteq \event y$)
  before interpreting it (into $M_L~y$ therefore)
\item or interpret it directly (into $M_L~x$ therefore) before transporting it
  via $F$ (into $M_L~y$).
\end{itemize}

Which equationally should correspond to. 
$$ \forall x~y~s.t.~ x\sqsubseteq y, (t:~itree~\event{x} X),~ F~(interp_L~t) == interp_L~(\ilc{embed}~t)$$

Most likely, this fact should be the consequence of the same fact but over the
handlers :
$$ \forall x~y~s.t.~ x\sqsubseteq y,~ (e~:~\event x ~X),~ F~(h_L~e) == h_L~(\ilc{embed}~e)$$

Other properties we want to have (possibly already implied):

\begin{itemize}
	\item Transitivity: Given \(x \sqsubseteq y \sqsubseteq z\), the \(F\) got from \(x \sqsubseteq z\)
		is the same than the composition of both \(F\) got by \(x \sqsubseteq y\) and \(y \sqsubseteq z\):
\[
	\forall x~y~z,
		x \sqsubseteq y \sqsubseteq z \to
		\forall m: M_L~x~\event{x},
		F~y~z~(F~x~y~m : M_L~y~\event{y})
		= F~x~z~m : M_L~z~\event{z}
\]
		\mb{Isn’t very readable in plain math, sorry.}
\end{itemize}

\yz{Random question: what should be the semantic implication of two events being
incomparable in the lattice, if any? Could it implies some commutation property?
I.e. if we think of two memory cells x and y, a computation only interacting
with x cannot touch y and reciprocally. And indeed a computation interacting
with both can be represented as StateT (x*y) M, or StateT x (StateT y M), or
StateT y (StateT x M)}

\section{Examples}

\subsection{A simple Read and Write system}
\label{sec:example:rw:simple}

We consider this semi-lattice~\(L\):
\begin{center}
\begin{tikzpicture}
	\node (RW) {\textit{RW}} ;
	\node [below left = 1cm of RW] (R) {\textit{R}} ;
	\node [below right = 1cm of RW] (W) {\textit{W}} ;

	\draw (R) -- (RW) -- (W) ;
\end{tikzpicture}
\end{center}

We define the following events:
\begin{itemize}
	\item \(\ilc{write} : \ilc{value} \to \event{d}~\ilc{unit}\) for \(d \in \left\{\mathit{W}, \mathit{RW}\right\}\),
	\item \(\ilc{read} : \event{d}~\ilc{value}\) for \(d \in \left\{\mathit{R}, \mathit{RW}\right\}\).
\end{itemize}

We define the following monads:
\begin{itemize}
	\item \(M~\mathit{W} \equiv \lambda T.~ \ilc{option value} \times T\) storing the possibly changed value,
	\item \(M~\mathit{R} \equiv \lambda T.~ \ilc{value} \to T\),
	\item \(M~\mathit{RW} \equiv \lambda T.~ \ilc{value} \to \ilc{value} \times T\), the usual state monad.
\end{itemize}

We can define the following interpretations of actions:
\begin{itemize}
	\item \(h~\mathit{R}~\ilc{read} = \lambda v.~v\),
	\item \(h~\mathit{RW}~\ilc{read} = \lambda v.~(v, v)\),
	\item \(h~\mathit{W}~(\ilc{write}~v') = \lambda v.~ \ilc{Some}~v'\),
	\item \(h~\mathit{RW}~(\ilc{write}~v') = \lambda v.~ (v', \unit)\),
\end{itemize}

The lifting function is defined as follows:
\begin{itemize}
	\item \(F~\mathit{R}~\mathit{RW}~T = \lambda (f : \ilc{value} \to T) (v : \ilc{value}). (f~v, v)\),
	\item \(F~\mathit{W}~\mathit{RW}~T~(\ilc{None}, t) = \lambda v.~(v, t)\),
    \yz{Those two lasts do not typecheck, it should take a value as argument
      before returning, i.e. $F~\mathit{W}~\mathit{RW}~T~(\ilc{None}, t):
      \ilc{value -> value*T}$. Oh got you, fixing}
	\item \(F~\mathit{W}~\mathit{RW}~T~(\ilc{Some}~v', t) = \lambda v.~(v', t)\),
\end{itemize}

We could be tempted to provide a different, much simpler monad for \(M~\mathit{W}\):
as we can never actually read the value in \(\event{W}\), why bother storing its value at all?
Let us thus consider the following alternative definitions:
\begin{itemize}
	\item \(M~\mathit{W} \equiv \lambda T.~ T\),
	\item \(h~\mathit{W}~(\ilc{write}~v') = \unit\),
	\item \(F~\mathit{W}~\mathit{RW}~T~t = \lambda v.~ (v, t)\).
\end{itemize}
This definition would not work because it doesn’t respect the following commutation:
\[F~\mathit{W}~\mathit{RW}~T~(h~\mathit{W}~(\ilc{write}~v')) \neq h~\mathit{RW}~(\ilc{write}~v')\]
This commutation thus forces us to somehow store the values given to \ilc{write}.


\subsection{Two projections of the same cell}
\label{sec:example:projections}

% That’s me trying to speak quantum mechanics.  Sorry if this feels ad hoc.

Let us assume a memory cell storing a vector in \(\mathbb{R}^2\), as well as two non-collinear unit vectors \(\vec{u}_1\) and \(\vec{u}_2\).
Let us assume two \(\ilc{read}_i\) operations over this memory cell, each computing the projection of the stored vector over the associated vector \(\vec{u}_i\) (for \(i \in \left\{1, 2\right\}\)).
The operation \ilc{write} updates the stored vector.

We thus have the following event---let us call it \(\event{\top}\):
\begin{itemize}
	\item \(\ilc{write} : \mathbb{R}^2 \to \event{\top}~\ilc{unit}\),
	\item \(\ilc{read}_i : \event{\top}~\mathbb{R}\) for \(i \in \left\{1, 2\right\}\).
\end{itemize}

We can also define a monad \(M~\top\) storing the vector and interpretation \(h~\top\) for the event \(\event{\top}\):
\begin{itemize}
	\item \(M~\top~T \equiv \mathbb{R}^2 \to \mathbb{R}^2 \times T\),
	\item \(h~\top~(\ilc{write}~\vec{v})~\_ = (\vec{v}, \unit)\),
	\item \(h~\top~\ilc{read}_i~\vec{v} = (\vec{v} \cdot \vec{u}_i, \unit)\) for \(i \in \left\{1, 2\right\}\).
\end{itemize}

However, if we never actually use the two \(\ilc{read}_i\) operations together, we no longer need to store the full vector!
Let us thus fix \(i \in \left\{1, 2\right\}\) and define a subevent of \(\event{\top}\), calling it \(\event{i}\) that only contains \(\ilc{write}\) and this particular \(\ilc{read}_i\).
The associated monad \(M~i\) is then simpler, only storing a scalar value (and thus might be easier to work with in proofs):
\begin{itemize}
	\item \(M~i~T \equiv \mathbb{R} \to \mathbb{R} \times T\),
	\item \(h~i~(\ilc{write}~\vec{v})~\_ = (\vec{v} \cdot \vec{u}_i, \unit)\),
	\item \(h~i~\ilc{read}_i~v = (v, \unit)\).
\end{itemize}

In the case one never actually use any \(\ilc{read}_i\) function, one no longer needs to actually store any information.
Let us call this event \(\event{w}\) and the associated operators:
\begin{itemize}
	\item \(M~w~T \equiv T\),
	\item \(h~w~(\ilc{write}~\vec{v}) = \unit\).
\end{itemize}
\mb{This definition of \(h~w~(\ilc{write}~\vec{v})\) is wrong, sorry. See end comment of Section~\ref{sec:example:rw:simple}.}
\yz{Indeed yes if your computation can write over both components, then you need to
  keep both components even if you only read the first one, or you won't be able
  to ever connect yourself to a computation that will read the second one I
  believe.
  Essentially your local computation $M~ i$ indeed cannot observe the effect of
  updating the second component, but you have still the ambition of plugging it into
  contexts that can observe it.
}

Finally, the only operator associated to \(\event{\bot}\) is \(M~\bot \equiv M~w\).

We order our (semi-)lattice (that’s our \(L\)) as follows:
\begin{center}
\begin{tikzpicture}
	\node (top) {\(\top\)} ;
	\node [below left = 1cm of top] (one) {\(1\)} ;
	\node [below right = 1cm of top] (two) {\(2\)} ;
	\node [below right = 1cm of one] (w) {w} ;
	\node [below = 1cm of w] (bot) {\(\bot\)} ;

	\draw (w) -- (two) -- (top) -- (one) -- (w) -- (bot) ;
\end{tikzpicture}
\end{center}

We finally define our lifting function \(F\) as follows:
\begin{itemize}
	\item \(F~\bot~i~T~t \equiv F~w~i~T~t \equiv \lambda v. (v, t)\),
	\item \(F~i~\top~T~(s : \mathbb{R} \to \mathbb{R} \times T)
		\equiv \lambda \vec{v}.
		(%(\vec{v} \cdot \vec{u}_{2-i}) \vec{u}_{2-i} +
		v \vec{u}_i, t))\)
		with \(s~(\vec{v} \cdot \vec{u}_i) = (v, t)\) for \(i \in \left\{1, 2\right\}\),
	\item \(F~\bot~\top~T~t \equiv F~w~\top~T~t \equiv \lambda \vec{v}. (\vec{v}, t)\).
\end{itemize}
\mb{I don’t think that this respects transitivity.}

In general, the monad \(M~x\) should be simpler (and thus, easier to reason with) than \(M~y\) for \(x \sqsubseteq y\).
As an extreme case, in this example, \(M~\bot\) carries a very small amount of information:
if we do not actually read the state, then we can ignore all its complexity.


\subsection{A too complicated \(\top\)}

Let us take back the previous example (or really, any other semi-lattice of monads),
but change the definition of \(M~\top\) to
\begin{itemize}
	\item \(M~\top~T \equiv \ilc{unit}\),
	\item \(h~\top~(\ilc{write}~\vec{v})~\_ = \unit\),
	\item \(h~\top~\ilc{read}_i = \unit\).
\end{itemize}

This is a dummy monad.
We may want to use such a monad when (1) there is no easy to reason monad with all the operations, and (2) all the operations are not supposed to appear in the same context all at once.
%
For instance, in \Rlang, there are initialising functions, called at the very beginning of the interpreter.
These functions update the values of global variables, that will never change after the initialisation phase.
Furthermore, these functions can’t fail: no error event are expected to be triggered by them.
This means that we expect no functions to be able to both change the values of global variables and be able to fail.
We thus don’t need to create a monad with all these operations at once:
let us define it as a dummy monad as \(M~\top\) above.
%
However, the initialising functions still call functions common to other parts of the interpreter:
we would like to be able to manipulate them in the same context than all the other functions.

The lifting function can be updated as follows:
\begin{itemize}
	\item \(F~i~\top~T~(s : \mathbb{R} \to \mathbb{R} \times T) \equiv \unit\)
	\item \(F~\bot~\top~T~t \equiv F~w~\top~T~t \equiv \unit\).
\end{itemize}

This is an example which is interesting for two reasons.
First, \(M~\top\) is not the most complex monad of our family of monads (it’s actually the simplest!).
Second, we are not interested in what’s happenning in \(M~\top\)---%
in contrary to what is usually assumed in \ilc{itree} (for instance if using the \ilc{translate} function).
Instead, we are interested in what happens in some of the family of monads (in this case, \(M~1\) and \(M~2\)).


\subsection{Two views of the same cell}

As I’m not sure whether the example of Section~\ref{sec:example:projections} is correct,
here is another one where the unit vectors are collinear.
It is based on the following lattice of indices.
\begin{center}
\begin{tikzpicture}
	\node (top) {\(\top\)} ;
	\node [below left = 1cm of top] (plus) {\(+\)} ;
	\node [below right = 1cm of top] (minus) {\(-\)} ;
	\node [below left = 1cm of minus] (bot) {\(\bot\)} ;

	\draw (bot) -- (two) -- (top) -- (one) -- (bot) ;
\end{tikzpicture}
\end{center}

We introduce the following operations:
\begin{itemize}
	\item \(\ilc{write}^\pm : \ilc{int} \to \event{d}~\ilc{unit}\)
		for \(\pm \in \left\{+, -\right\}\) and \(d \in \left\{\top, \pm\right\}\),
	\item \(\ilc{read}^\pm : \event{d}~\ilc{int}\)
		for \(\pm \in \left\{+, -\right\}\) and \(d \in \left\{\top, \pm\right\}\),
\end{itemize}

We define the output monads as follows:
\begin{itemize}
	\item \(M~\top~T \equiv M~+~T \equiv M~-~T \equiv \ilc{int} \to \ilc{int} \times T\),\mb{This \(+\) might not be readable here: it really represents a symbol, not the addition.}
	\item \(M~\bot~T \equiv T\),
	\item \(h~\top~(\ilc{write}^+~i)~\_ = h~+~(\ilc{write}^+~i)~\_ = (i, \unit)\),
	\item \(h~\top~(\ilc{write}^-~i)~\_ = (-i, \unit)\),\mb{same readability comment here.}
	\item \(h~-~(\ilc{write}^-~i)~\_ = (i, \unit)\),
	\item \(h~\top~\ilc{read}^+~i = h~+~\ilc{read}^+~i = (i, \unit)\),
	\item \(h~\top~\ilc{read}^-~i = (-i, \unit)\),
	\item \(h~-~\ilc{read}^-~i = (i, \unit)\),
\end{itemize}
Note how the interpretations of \(\ilc{write}^-\) and \(\ilc{read}^-\) are reversed in \(h~\top\) and \(h~-\).
This is not a mistake: from the point of view of \(M~-\), the monad works exactly the same than \(M~+\).
In other words, if we are only using the \(\ilc{write}^\pm\) and \(\ilc{read}^\pm\) operations for a given \(\pm \in \left\{+, -\right\}\),
then \(\ilc{read}^\pm\) will just read what was previously given to \(\ilc{write}^\pm\), with no sign inversion (even for \(\pm = -\)).

Of course, when manipulating both point of views, we have to choose a way to store these data.
This is way \(M~\top\) has to choose a point of view (in this case, \(M~+\)’s).
The switch of point of view from \(M~-\) to \(M~\top\) can be seen in the definition of the \(F\) function:
\begin{itemize}
	\item \(F~\bot~\pm~T~t \equiv \lambda i. (i, t)\),
	\item \(F~+~\top~T~(s : \ilc{int} \to \ilc{int} \times T) \equiv s\),
	\item \(F~-~\top~T~(s : \ilc{int} \to \ilc{int} \times T) \equiv \lambda i. (-i', t)\) with \(s~i = (i', t)\).
\end{itemize}

In this case, \(M~\top\) follows \(M~+\), but it could completely have followed the point of view of \(M~-\): both are valid.
This formalism thus enables us to choose a more adequate monad for specific use-case:
if we only use \(\ilc{write}^-\) and \(\ilc{read}^-\) events, then we should not have to deal with these opposites.


\subsection{Enabling or disabling logs}

This example is particular as all the elements of the semi-lattice are associated to the same events.
We consider this lattice:
\begin{center}
\begin{tikzpicture}
	\node (on) {\textit{on}} ;
	\node [below = 1cm of on] (off) {\textit{off}} ;

	\draw (on) -- (off) ;
\end{tikzpicture}
\end{center}

We define \(\event{\mathit{on}} = \event{\mathit{off}}\) as including at least the following operation:
\begin{itemize}
	\item \(\ilc{log} : \ilc{int} \to \event{d}~\ilc{unit}\) for \(d \in \left\{\mathit{on}, \mathit{off}\right\}\).
\end{itemize}
These logs could be associated to some specific invariant, but only useful at some places.
In the other places, they would just be another element of the monad \(M~\mathit{on}\), and it might be cumbersome to propagate in some proofs.
We could thus instead choose to interpret an interaction tree within \(M~\mathit{off}\), without the logs, and still get all the results on \(M~\mathit{on}\) by using properties of \(F\).

\subsection{TODO}

\begin{itemize}
	\item Termination monad: if a particular function isn’t using any recursion / loop, then we can remove the termination part without issue.
	\item The (semi)lattice \(L\) could be the abstract domain of a flot-insensitive analysis.
	\item Any types-and-effect system.
	\item Complexity:
		given a syntax with for-loops, in which loops are always from \(1\) to either an argument or the index of another loop (there is thus a specific environment for loop-indices), and where only loops can modify their index,
		could we statically infer some notions of basic complexity?
		Something like \(\mathtt{for} : \mathtt{var} \to \mathcal{O}(n^k) \to \mathcal{O}(n^{k+1})\)?

		% Idées de Yannick :
% calcul fini statiquement implique Finite quand embarqué dans les itrees
% calcul qui ne peut pas échouer embarqué dans une monade d'échec
% read monad into state monad
% state monade sur une liste finie de cellules into state monad sur une famille dénombrable de cellules
% calcul déterministe into monade de non-determinisme (version simple tout fini avec la monade list pour l'instant)
% une cellule avec seulement une opération d'incrément into state monade sur cette cellule. Ça ça me faisait peut-être penser à une classe plus générale : c'est un cas avec des events E qui ont la particularité d'admettre une implèm en terme d'une implèm libre vis à vis d'une autre signature d'events F, i.e. tel que on a un handler E ~> Free F (dans ce cas particulier, h incr ::= v <- rd;; wr (S v)). Je ne sais pas s'il y a un truc intéressant à regarder vis à vis de cette classe en général.
% plus flou à voir : la monade de weakest precondition (X -> Prop) -> Prop et ses variantes est pas mal utilisée pour specifier des programmes, je ne sais pas s'il y a un truc à se demander de ce côté

	\item Fail monad where a program doesn’t use fail.
	\item read monad into state monad
	\item state monad on a finite subset of variables of a larger state monad.
		Note that the lattice can here be infinite.
		Bonus: combination of read/write for each variable separately.
	\item A counting monad (there is just one variable, and it can only increase by calling a \texttt{incr} method, and the monad only stores the diff) into a state monad.
		Generalisation: a subset of events that we can each implement using other (more fundamental) events.
	\item weakest precondition monad.

	\item Promises and values.
		A static value can always be converted into a promise.
		(And a promise into a promise of promise, to build some kind of hierarchy?)
		This means that given \texttt{A : Type}, there is a natural morphism from \texttt{A} to \texttt{Promise<A>}.
		Could this mean that we could just bind something to get its returned value without having to think whether its result is within a promise or not?
		This is quite a big deal in untyped languages: it is sometimes difficult to know whether we need the \texttt{await} keyword,
		but I guess that in a typed language, this just says that we could always use \texttt{bind}, sometimes using the identity monad when there is nothing more to do.
		So maybe it’s not that a good idea.


\end{itemize}

\end{document}

