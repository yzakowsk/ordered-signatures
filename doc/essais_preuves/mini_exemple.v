Class Monad (M : Type -> Type) :=
  {
    ret : forall X, X -> M X;
    bind : forall X Y, M X -> (X -> M Y) -> M Y;
    ret_bind : forall X Y (x : X) (f : X -> M Y), bind X Y (ret X x) f = f x;
    bind_ret : forall X (m : M X), bind X X m (ret X) = m;
    bind_bind :
      forall X Y Z (m : M X) (f : X -> M Y) (g : Y -> M Z),
        bind Y Z (bind X Y m f) g = bind X Z m (fun x => bind Y Z (f x) g);
  }.

Class MonadMorphism {M1 M2 : Type -> Type} {MM1 : Monad M1} {MM2 : Monad M2} (t : forall X, M1 X -> M2 X) :=
  {
    ret_compat : forall (X : Type) (x : X), ret X (x : X) = t X (ret X x);
    bind_compat : forall (X Y : Type) (m : M1 X) (f : X -> M1 Y),
      t Y (bind X Y m f) = bind X Y (t X m) (fun x => t Y (f x))
  }.

(* Let's do this as an axiom for now. *)
Axiom extensionality :
  forall (X Y : Type) (f g : X -> Y),
    (forall x : X, f x = g x) -> f = g.

Definition M (X : Type) : Type := nat -> nat * X.

#[local] Program Instance M_monad : Monad M :=
  {
    ret X x e := (e, x);
    bind X Y m f e := let (e', x) := m e in f x e';
  }.

Next Obligation.
Proof.
  intros. simpl. apply extensionality. reflexivity.
Qed.

Next Obligation.
Proof.
  intros. simpl. apply extensionality. intro e. destruct (m e). reflexivity.
Qed.

Next Obligation.
Proof.
  intros. simpl. apply extensionality. intro e. destruct (m e).
  destruct (f x n). reflexivity.
Qed.

Definition read : M nat :=
  fun e => (e, e).

Definition preserves X (prog : M X) : Prop :=
  forall e, let (e', _) := prog e in e = e'.

Definition myprog :=
  bind nat nat read (fun (a : nat) => bind nat nat read (fun (b : nat) => ret nat (a+b))).

Theorem myprog_preserves : preserves nat myprog.
Proof.
  unfold preserves. intro e. unfold myprog.
  unfold bind. unfold ret. unfold read.
  simpl. reflexivity.
Qed.


Definition M_m (X : Type) := nat -> X.

#[local] Program Instance M_m_monad : Monad M_m :=
  {
    ret X x e := x;
    bind X Y m f e := f (m e) e;
  }.

Next Obligation.
Proof.
  intros. simpl. apply extensionality. reflexivity.
Qed.

Next Obligation.
Proof.
  intros. simpl. apply extensionality. reflexivity.
Qed.

Next Obligation.
Proof.
  intros. simpl. reflexivity.
Qed.

Definition t : forall X, M_m X -> M X :=
  fun X (mm : M_m X) e => (e, mm e).

#[local] Program Instance t_morphism : MonadMorphism t.

Next Obligation.
Proof.
  intros. unfold t. simpl. reflexivity.
Qed.

Next Obligation.
Proof.
  intros. unfold t. simpl. reflexivity.
Qed.
