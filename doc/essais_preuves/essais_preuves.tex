\nonstopmode
\documentclass{article}

\usepackage[french]{babel}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{stmaryrd}
\usepackage{mathtools}

\usepackage[french]{babel}
\usepackage{tikz-cd}

\newcommand{\defeq}{\vcentcolon=}
\newcommand{\bind}{>\!\!>\!=}
\newcommand{\lamb}[1]{\lambda #1 . \,}
\newcommand{\into}{\rightarrow}
\newcommand{\iiv}[1]{\llbracket #1 \rrbracket}
\newcommand{\cart}{\times}
\DeclareMathOperator{\lett}{let}
\DeclareMathOperator{\inn}{in}
\DeclareMathOperator{\ret}{ret}
\DeclareMathOperator{\readd}{read}
\DeclareMathOperator{\writee}{write}

\renewcommand{\O}{\varnothing}

\begin{document}

\subsubsection*{Première formulation de lecture/écriture}

On modélise une cellule de mémoire contenant une valeur de type
$C$. Il est possible de lire et d'écrire dans cette cellule.  On
définit quatre monades qui correspondent à des niveaux de permissions
différents, à savoir aucune interaction avec la cellule ($\O$),
interaction en lecture seule ($R$), interaction en écriture seule
($W$) et interaction en lecture et écriture ($RW$). $M_\O$ est
la monade identité:

\begin{align*}
  M_\O X &= X \\
  \ret_\O &: \forall X, X \into M_\O X \\
  \ret_\O x &= x \\
  \bind_\O &: \forall X Y, M_\O X \into (X \into M_\O Y) \into M_\O Y \\
  x \bind_\O f &= f x
\end{align*}



Les instances de $M_R$ sont des fonctions de la valeur de
la cellule:

\begin{align*}
  M_R X &= C \into X \\
  \ret_R &: \forall X, X \into M_R X \\
  \ret_R x &= \lamb{c} x \\
  \bind_R &: \forall X Y, M_R X \into (X \into M_R Y) \into M_R Y \\
  m \bind_R f &= \lamb{c} (f (m c)) c \\
  \readd_R &: M_R C \\
  \readd_R &= \lamb{c} c
\end{align*}

De même, les instances de $M_W$ prennent la valeur initiale
en paramètre. Elles renvoient un couple formé d'un résultat
et de la nouvelle valeur de la cellule.

\begin{align*}
  M_W X &= C \into X \cart C \\
  \ret_W &: \forall X, X \into M_W X \\
  \ret_W x &= \lamb{c} (x, c) \\
  \bind_W &: \forall X Y, M_W X \into (X \into M_W Y) \into M_W Y \\
  m \bind_W f &= \lamb{c} \lett (x, c') = m c\ \inn\ f x c' \\
  \writee_W &: C \into M_W unit \\
  \writee_W c &= \lamb{c'} ((), c) \\
\end{align*}

Enfin, la monade $M_{RW}$ combine ces deux effets. Le type est le même
que $M_{W}$, ainsi que les opérations $\ret$ et $\bind$ . On ajoute
simplement $\readd_{RW}$.

\begin{align*}
  \readd_{RW} &: M_{RW} C \\
  \readd_{RW} &= \lamb{c} (c, c)
\end{align*}

\underline{$M_\O$ est une monade:} On a bien

\begin{align*}
  \ret_\O x \bind_\O f
  &= x \bind_\O f \\
  &= f x \\
  x \bind_\O \ret_\O
  &= \ret_\O x \\
  &= x \\
  (x \bind_\O f) \bind_\O g
  &= g (f x) \\
  &= (g \circ f) x \\
  &= x \bind_\O \lamb{y} (f y \bind_\O g)
\end{align*}

\underline{$M_R$ est une monade:}

\begin{align*}
  \ret_R x \bind_R f &= \lamb{c} f (\ret_R x c) c = \lamb{c} f x c = f x \\
  m \bind_R \ret_R &= \lamb{c} \ret_R (m c) c = \lamb{c} m c = m \\
   (m \bind_R f) \bind_R g &= \lamb{c} g ((\lamb{c'} f (m c') c') c) c \\
  &= \lamb{c} g (f (m c) c) c \\
  &= \lamb{c} (\lamb{c'} g (f (m c) c') c') c \\
  &= \lamb{c} (\lamb{y} (\lamb{c'} g (f y c') c')) (m c) c \\
  &= \lamb{c} (\lamb{y} (f y \bind_R g)) (m c) c \\
  &= m \bind_R \lamb{y} (f y \bind_R g) \\
\end{align*}

\underline{$M_W$ est une monade:}

\begin{align*}
  \ret_W x \bind_W f &= \lamb{c} \lett (x, c') = (\lamb{c''} (c, c'')) c \inn f x c' \\
  &= \lamb{c} \lett (x, c') = (c, c) \inn f x c' \\
  &= \lamb{c} f x c \\
  &= f x \\
  m \bind_W \ret_W  &= \lamb{c} \lett (x, c') = m c \inn (\lamb{c''} (x, c'')) c \\
  &= \lamb{c} \lett (x, c') = m c \inn (x, c') \\
  &= \lamb{c} m c \\
  &= m \\
  (m \bind_W f) \bind_W g &= \lamb{c} \lett (x, c') = (\lamb{c''} \lett (x', c''') = m c'' \inn f x' c''') c \inn g x c' \\
  &= \lamb{c} \lett (x, c') = (\lett (x, c''') = m c \inn f x' c''') \inn g x c' \\
  &= \lamb{c} \lett (x, c''') = m c \inn \lett (x, c') = f x' c''' \inn g x c' \\
  &= \lamb{c} \lett (x, c''') = m c \inn \\
  &= \lamb{c} \lett (x, c''') = m c \inn (\lamb{c''} \lett (x, c') = m c'' \inn f x c') c''' \\
  &= \lamb{c} \lett (x, c''') = m c \inn (f x \bind_W g) c''' \\
  &= \lamb{c} \lett (x, c''') = m c \inn (\lamb{y} f y \bind_W g ) x c''' \\
  &= m \bind_W (\lamb{y} f y \bind_W g)
\end{align*}

\underline{$M_{RW}$ est une monade:} Ses opérations sont les mêmes
que celles de $M_W$.

On pose ensuite des morphismes de monades selon ce diagramme (dont
on prouvera plus tard qu'il est commutatif):

$$\begin{tikzcd}
  & M_{RW} \\
  M_R\arrow{ur}{t_{R \into RW}} & & M_W\arrow[swap]{ul}{t_{W \into RW}} \\
  & M_\O\arrow{ul}{t_{\O \into R}} \arrow[swap]{ur}{t_{\O \into W}} \arrow[bend right]{uu}{t_{\O \into RW}}
\end{tikzcd}$$

Définissons les quatre morphismes du losange:

\begin{align*}
  t_{\O \into R} m &= \lamb{c} m \\
  t_{\O \into W} m &= \lamb{c} (m, c) \\
  t_{R \into RW} m &= \lamb{c} (m c, c) \\
  t_{W \into RW} m &= m
\end{align*}

\underline{$t_{\O \into R}$ est un morphisme:}

\begin{align*}
  t_{\O \into R} (\ret_\O x) &= \lamb{c} x \\
  &= \ret_R x \\
  t_{\O \into R} (m \bind_\O f) &= \lamb{c} (f m) \\
  &= \lamb{c} (f m) \\
  &= \lamb{c} ((\lamb{y} \lamb{c''} (f y)) m) c \\
  &= \lamb{c} ((\lamb{y} \lamb{c''} (f y)) ((\lamb{c'} m) c)) c \\
  &= \lamb{c'} m \bind_R \lamb{y} \lamb{c''} (f y) \\
  &= t_{\O \into R} m \bind_R \lamb{y} t_{\O \into R} (f y)
\end{align*}

\underline{$t_{\O \into W}$ est un morphisme:}

\begin{align*}
  t_{\O \into W} (\ret_\O x) &= \lamb{c} (x, c) = \ret_R x \\
  t_{\O \into W} (m \bind_\O f) &= \lamb{c} (f m, c) \\
  &= \lamb{c} \lett (x, c') = (m, c) \inn (f x, c') \\
  &= \lamb{c} \lett (x, c') = (\lamb{c''} (m, c'')) c \inn
    (\lamb{y} \lamb{c'''} (f y, c''')) x c' \\
  &= t_{\O \into W} m \bind_W \lamb{y} t_{\O \into W} (f y)
\end{align*}

\underline{$t_{R \into RW}$ est un morphisme:}

\begin{align*}
  t_{R \into RW} (\ret_R x)
  &= \lamb{c} ((\lamb{c} x) c, c) \\
  &= \lamb{c} (x, c) \\
  &= \ret_{RW} x \\
  t_{R \into RW} (m \bind_R f)
  &= \lamb{c} ((\lamb{c'} f (m c') c') c, c) \\
  &= \lamb{c} (f (m c) c, c) \\
  &= \lamb{c} \lett (x, c') = (m c, c) \inn (f x c', c') \\
  &= \lamb{c} \lett (x, c') = (\lamb{c''} (m c'', c'')) c
    \inn (\lamb{y} \lamb{c'''} (f y c''', c''')) x c' \\
  &= \lamb{c''} (m c'', c'') \bind_{RW} \lamb{y} \lamb{c'''} (f y c''', c''') \\
  &= t_{R \into RW} m \bind_{RW} \lamb{y} t_{R \into RW} (f y)
\end{align*}

\underline{$t_{W \into RW}$ est un morphisme:} on peut en fait le voir comme l'identité
sur $M_W$.

\begin{align*}
  t_{W \into RW} (\ret_W x)
  &= \ret_W x \\
  &= \ret_{RW} x \\
  &= \ret_{RW} (t_{W \into RW} x) \\
  t_{W \into RW} (m \bind_W f)
  &= m \bind_W f \\
  &= m \bind_{RW} f \\
  &= t_{W \into RW} m \bind_{Rw} \lamb{y} t_{W \into RW} (f y)
\end{align*}

La commutativité du losange est immédiate:

\begin{align*}
  t_{R \into RW} (t_{\O \into R} m)
  &= \lamb{c} ((\lamb{c'} m) c, c) \\
  &= \lamb{c} (m, c) \\
  &= t_{W \into RW} (t_{\O \into W} m)
\end{align*}

On pose alors
$t_{\O \into RW} = t_{R \into RW} \circ t_{\O \into R} = t_{W \into
  RW} \circ t_{\O \into W}$, soit
$t_{\O \into RW} m = \lamb{c} (m, c)$, de sorte que le diagramme
entier commute. $t_{\O \into RW}$ est un morphisme par composition
de morphismes.

\subsubsection*{Deuxième formulation}

Au lieu de prendre une unique monade de lecture dont les
instances sont des fonctions de la valeur stockée dans la
cellule, on peut imaginer étendre le treillis en définissant
une monade par valeur de type $C$. Ce n'est pas possible
en pratique en OCaml, car un type ne peut pas être paramétré
par une valeur qui n'est pas un type. En revanche, c'est bien
possible en Coq.

De façon similaire, on peut reformuler la monade d'écriture
en supprimant le paramètre de valeur initiale, et en faisant
retourner au calcul une valeur option qui vaut $\mathrm{None}$ si
aucune écriture n'a été faite, et $\mathrm{Some}\ c$ si la dernière
écriture effectuée a écrit la valeur $c$.

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
