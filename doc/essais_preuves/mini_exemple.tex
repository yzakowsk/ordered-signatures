\nonstopmode
\documentclass{article}

\usepackage[french]{babel}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{stmaryrd}
\usepackage{mathtools}

\usepackage[french]{babel}
\usepackage{tikz-cd}

\newcommand{\defeq}{\vcentcolon=}
\newcommand{\bind}{>\!\!>\!=}
\newcommand{\lamb}[1]{\lambda #1 . \,}
\newcommand{\into}{\rightarrow}
\newcommand{\iiv}[1]{\llbracket #1 \rrbracket}
\newcommand{\cart}{\times}
\DeclareMathOperator{\lett}{let}
\DeclareMathOperator{\inn}{in}
\DeclareMathOperator{\ret}{ret}
\DeclareMathOperator{\readd}{read}
\DeclareMathOperator{\writee}{write}

\renewcommand{\O}{\varnothing}
% https://tex.stackexchange.com/questions/149710/how-to-write-math-symbols-in-a-verbatim/345983#345983?newreg=93a91c392a2844dba5ee5c65fe5f3277
\usepackage{unicode-math}
\setmonofont{DejaVu Sans Mono}

\begin{document}

Programme:

\begin{verbatim}
let a : int = read in
let b : int = read in
ret (a+b)
\end{verbatim}

Monade RW:

\begin{verbatim}
M : Type → Type
M X = Env → (Env × X)
Env = nat
ret x = λe. (e, x)
bind m f = λe. let (e', x) = m e in f x e'
read : M nat
read = λe.(e,e)
write : nat → M unit
write n = λe.(n, tt)
\end{verbatim}

Propriété \og préserve l'état \fg:

\begin{verbatim}
preserves (X : Type) (prog : M X) := ∀ e, let (e', _) = P e in e' = e
\end{verbatim}

On cherche à prouver \verb+preserves P+, où \verb+P+ est le programme
donné plus haut.

Remarque : cette définition suppose un peu que la modélisation est
faite en Coq car on fait un ∀ sur une variable qui est dans le langage
cible. En fait, il faudrait dire plus exactement : \og pour tout
contexte dans le langage cible qui résout les variables libres
de\verb+prog+, le programme résultat appliqué à n'importe quelle
valeur du langage préserve l'état \fg. Bref, il faut une formalisation
du langage cible. TODO: voir comment cela se goupille avec R.

Preuve sans mini-monade:

\begin{itemize}
\item Lemme : pour tout \verb+x+, \verb+ret x+ préserve l'état. (Trivial.)
\item Lemme : si \verb+m+ préserve l'état, et pour tout \verb+x+, \verb+f x+
  préserve l'état, alors \verb+bind m f+ préserve l'état.

  Preuve (tout aussi triviale) : dans l'écriture du \verb+bind+, si \verb+m+ préserve l'état,
  on a \verb+e' = e+, puis si \verb+f x+ préserve l'état, le résultat est égal
  à \verb+e'+.
\item Lemme : \verb+read+ préserve l'état. (Trivial.)
\end{itemize}

Ensuite, on regarde comment le programme se traduit monadiquement:

\begin{verbatim}
read >>= λa. read >>= λb. ret (a+b)
\end{verbatim}

\begin{itemize}
\item \verb|ret (a+b)| préserve l'état car c'est une application de \verb+ret+.
\item Donc le résultat de \verb|λb. ret (a+b)| sur tout argument préserve l'état.
\item De plus \verb|read| préserve l'état.
\item Donc \verb|read >>= λb. ret (a+b)| préserve l'état.
\item Donc le résultat de \verb|λa. read >>= λb. ret (a+b)| sur tout argument
  préserve l'état.
\item Et \verb|read| préserve (toujours) l'état.
\item Donc le terme entier préserve l'état.
\end{itemize}

Preuve avec mini-monade:

On introduit la monade

\begin{verbatim}
M_m : Type → Type
M_m X = Env → X
Env = nat
ret_m x = λe. x
bind_m m f = λe. f (m e) e
read_m : M nat
read_m = λe.e
\end{verbatim}

et l'application

\begin{verbatim}
t : ∀ X, M_I X → M X
t {X} m = λe.(e, m e)
\end{verbatim}


On prouve alors que \verb|M_m| est une monade (à faire, trivial), et surtout que
\verb|t| est compatible avec toutes les opérations.

\begin{verbatim}
∀ x, t (ret_m x) = ret x
∀ m f, t (bind_m m f) = bind (t m) (t ◦ f)
read = t read_m
\end{verbatim}

Preuves (avec l'axiome d'extensionnalité!):

\begin{verbatim}
1. t (ret_m x) e = ((ret_m x) e, e) = (x, e) = ret x e
2. t (bind_m m f) e = ((bind_m m f) e, e)
                    = (f (m e) e, e)

   bind (t m) (t ◦ f) e = (let (e', x) = t m e in (t ◦ f) x e')
                        = (let (e', x) = (e, m e) in t (f x) e')
                        = t (f (m e)) e
                        = (f (m e) e, e)
3. read e = (e, e)
   t read_m t = (e, read_m e)
              = (e, e)
\end{verbatim}

Dernière chose, on prouve que les programmes dans l'image de \verb|t|
préservent l'état:

\begin{verbatim}
∀ m : M_m, preserves (t m)
\end{verbatim}

Preuve immédiate par définition de \verb|t|.

On reprend le programme:

\begin{verbatim}
P = read >>= λa. read >>= λb. ret (a+b)
\end{verbatim}

On le réécrit (\og on le retype \fg, ou de préférence l'inférence
de types s'occupe automatiquement de traduire la notation let en ceci
plutôt qu'en le programme décrit plus haut):

\begin{verbatim}
P = read >>= λa. read >>= λb. t (ret_m (a+b))
  = read >>= λa. t read_m >>= t ◦ (ret_m (a+b))
  = read >>= λa. t (read_m >>=_m ret_m (a+b))
  = t read_m >>= t ◦ λa. (read_m >>=_m ret_m (a+b))
  = t (read_m >>=_m λa. (read_m >>=_m ret_m (a+b)))
\end{verbatim}

(L'égalité ici fait appel à l'extensionnalité, en Coq il serait peut-être
plus propre de ne pas poser l'extensionnalité comme axiome mais de dire
simplement que les programmes sont équivalents selon la relation d'équivalence
qui associe deux programmes s'ils ont la même sortie sur toute entrée.
Bien sûr, cette relation est compatible avec la propriété \og préserver
l'état \fg.)

Pour finir, on a réécrit \verb|P| comme un programme image par \verb|t| de
quelque chose, ce qui assure qu'il préserve l'état.

\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "essais_preuves"
%%% End:
