% Pour compiler:
% xelatex -shell-escape rapport && bibtex rapport && xelatex -shell-escape rapport.tex && xelatex -shell-escape rapport.tex

\nonstopmode
\documentclass{article}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{stmaryrd}
\usepackage{mathtools}

\usepackage[french]{babel}

\usepackage{tikz-cd}

\newcommand{\defeq}{\vcentcolon=}
\newcommand{\bind}{>\!\!>\!=}
\newcommand{\lamb}[1]{\lambda #1 . \,}
\newcommand{\into}{\rightarrow}
\newcommand{\iiv}[1]{\llbracket #1 \rrbracket}
\newcommand{\cart}{\times}
\DeclareMathOperator{\lett}{let}
\DeclareMathOperator{\inn}{in}
\DeclareMathOperator{\ret}{ret}
\DeclareMathOperator{\readd}{read}
\DeclareMathOperator{\writee}{write}
\DeclareMathOperator{\opt}{Option}
\DeclareMathOperator{\none}{None}
\DeclareMathOperator{\some}{Some}
\DeclareMathOperator{\match}{match}
\DeclareMathOperator{\with}{with}
\DeclareMathOperator{\type}{Type}
\DeclareMathOperator{\unit}{unit}

\renewcommand{\O}{\varnothing}

\newcommand{\RW}{\mathsf{RW}}
\newcommand{\RWO}{\mathsf{RWO}}

\usepackage{minted}

\newminted[coqe]{coq}{}

\newmintinline[coq]{coq}{}
\newmintinline[scheme]{scheme}{}
\newmintinline[ocaml]{ocaml}{}
\newmintinline[haskell]{haskell}{}

% https://tex.stackexchange.com/questions/149710/how-to-write-math-symbols-in-a-verbatim/345983#345983?newreg=93a91c392a2844dba5ee5c65fe5f3277
\usepackage{unicode-math}
\setmonofont{DejaVu Sans Mono}

\usepackage{hyperref}

%\usepackage[lmargin=1.5cm]{geometry}

\setcounter{secnumdepth}{4}
\setcounter{tocdepth}{3}
\makeatletter
\newcounter {subsubsubsection}[subsubsection]
\renewcommand\thesubsubsubsection{\thesubsubsection .\@alph\c@subsubsubsection}
\newcommand\subsubsubsection{\@startsection{subsubsubsection}{4}{\z@}%
                                     {-3.25ex\@plus -1ex \@minus -.2ex}%
                                     {1.5ex \@plus .2ex}%
                                     {\normalfont\normalsize\bfseries}}
\newcommand*\l@subsubsubsection{\@dottedtocline{3}{10.0em}{4.1em}}
\newcommand*{\subsubsubsectionmark}[1]{}
\makeatother


\title{Rapport de stage de L3}
\author{Jean Abou Samra \and Stage réalisé sous la direction de Martin Bodin}
\date{}



\begin{document}

\maketitle


\begin{abstract}

Résumé ...

\end{abstract}


\section{Contexte du stage}

Dans cette section, les notations sont inspirées de Coq, mais normalement
compréhensibles même sans connaissance préalable de sa syntaxe.

\subsection{Monades: définition et exemples fondamentaux}

\subsubsection{Motivation}

Les monades sont un type de structures de données et un style de
programmation qui permettent d'écrire simplement dans des langages
fonctionnels des programmes qui seraient au départ plus simples à
écrire dans un langage impératif.  Elles permettent d'encoder des
effets, tels que les entrées et sorties, les exceptions, ou encore la
mutation d'un état.

On peut se demander pourquoi chercher à écrire dans un langage fonctionnel un
programme qui se prêterait mieux à un langage impératif. Les raisons sont
multiples. Tout d'abord, on peut pragmatiquement souhaiter conserver un unique
langage pour un projet déjà écrit dans un langage fonctionnel.  Cette motivation
n'est pourtant pas la plus convaincante, puisqu'il existe bel et bien des
langages de style fonctionnel, tels que OCaml, qui permettent néanmoins les
effets de bord. Une autre motivation est qu'un langage purement fonctionnel peut
être paresseux. En effet, si le langage a la garantie qu'une expression n'a
aucun effet de bord possible, cette expression peut être évaluée à la demande,
sans risquer de manquer un effet de bord nécessaire ou de changer l'ordre des
effets de bord d'une manière qui affecte la correction du programme. C'est le
cas du langage Haskell.  Enfin, et c'est la motivation principale pour ce
travail, le style purement fonctionnel permet de raisonner plus simplement sur
les programmes, car un raisonnement qui s'applique de manière locale n'a jamais
besoin d'être révisée lorsque le petit programme est inséré dans un programme
plus large à cause de modifications d'un état. Ce travail a pour point de départ
un problème de passage à l'échelle dans l'assistant de preuve Coq, dont le
fondement est un langage purement fonctionnel, Gallina.

Afin d'introduire la notion de monade, prenons un exemple d'une
construction qui s'y apparente dans le langage Scheme.  En Scheme,
les variables locales s'introduisent la plupart à l'aide d'une
S-expression \scheme{let*}, qui prend la forme:

\begin{minted}{scheme}
(let* ((variable1 valeur1)
       (variable2 valeur2)
       ...)
  expression1
  expression2
  ...)
\end{minted}

Il s'agit d'un équivalent de ce qui en OCaml s'écrirait

\begin{minted}{ocaml}
let variable1 = valeur1 in
let variable2 = valeur2 in
... in
expression1;
expression2;
...
\end{minted}

La SRFI 2 \footnote{Les SRFI sont les \og Scheme Request For
Implementation \fg, des documents qui proposent une extension du
langage Scheme. Ils sont débattus par la communauté. Ils visent à
tendre vers une plus grande standardisation des nombreuses extensions
au langage.}  \cite{srfi2} propose la construction
\scheme{and-let*}. Elle s'écrit exactement comme \scheme{let*}, en
remplaçant \scheme{let*} par \scheme{and-let*}.  Elle ajoute au \scheme{let*}
un comportement semblable à la construction \scheme{and} (qui est
similaire à celle d'autre langages, en particulier paresseuse). Avec
\scheme{and-let*}, les variables sont évaluées dans l'ordre, mais dès que
l'une vaut \scheme{#f}, la valeur \og faux \fg, l'expression cesse d'être
évaluée et la valeur \scheme{#f} est renvoyée. On a ainsi une
construction purement fonctionnelle qui émule sous une forme édulcorée
les exceptions. Dès que l'un des calculs intermédiaires renvoie
\scheme{#f}, valeur généralement utilisée pour les résultats invalides ou
manquants, l'expression entière prend elle-même cette valeur indiquant
une forme d'erreur. Voici un exemple simple:

\begin{minted}{scheme}
(and-let* ((paramètres (assoc-ref paramètres-utilisateurs nom-personne))
           (année-naissance (assq-ref paramètres 'année-naissance)))
  (- année-courante année-naissance))
\end{minted}

Si l'une des requêtes \scheme{(assoc-ref paramètres-utilisateurs
  nom-personne)} (rechercher les données associées à un utilisateur)
et \scheme{(assq-ref paramètres 'année-naissance)} (rechercher l'année
de naissance dans ces données) échoue, l'expression entière renvoie
\scheme{#f}.  Sinon, elle renvoie l'âge courant de l'utilisateur (à un
an près).

Les monades sont une vaste généralisation de ce procédé. Il s'agit,
pour faire simple, de définir des opérateurs de liaison des variables
qui aient des comportements particuliers tout en se comportant comme
ce que l'on attendrait d'un tel opérateur. Pour la théorie, on se
place dans le $\lambda$-calcul, et on pose \coq{bind} un opérateur qui
prend une valeur monadique et une fonction, et effectue ce à quoi on
pense comme une variante de l'appel de la fonction. Ainsi, ce qui en
Scheme s'écrirait \scheme{(and-let* ((variable valeur)) expression)}
sera noté \coq{bind valeur (λvariable. expression)}.

Il se pose une complication lorsque l'on passage de Scheme à un
langage fonctionnel typé statiquement, ou au $\lambda$-calcul typé. En
effet, si le résultat est valide, il ne peut pas être donné tel quel
mais doit être renvoyé comme une instanciation d'un constructeur, afin
de préserver le typage. C'est le constructeur \ocaml{Some}
du type \ocaml{option} en OCaml ou Coq, et le constructeur
\haskell{Just} de \haskell{Maybe} en Haskell.
Il faut donc un moyen de transformer un résultat valide obtenu en une
valeur du type monadique \og option \fg. On appellera cette fonction
\coq{ret}.

\subsubsection{Définition, axiomes et notations}

Une \textit{monade} est un type paramétré \coq{M : Type → Type} muni de
deux opérations:

\begin{coqe}
ret : ∀ (X : Type), X → M X
bind : ∀ (X Y : Type),  M X → (X → M Y) → M Y
\end{coqe}

vérifiant les trois lois suivantes:

\begin{coqe}
∀ (X Y : Type) (x : X) (f : X → M Y), bind (ret x) f = f x
∀ (X : Type) (m : M X), bind m ret = m
∀ (X Y Z : Type) (m : M X) (f : X → M Y) (g : Y → M Z),
  bind (bind m f) g = bind m (λx. bind (f x) g)
\end{coqe}

On introduit souvent la notation \coq{m >>= f} pour \coq{bind m f}.
Cette notation est par exemple présente dans Haskell. Les trois lois
se réécrivent alors:

\begin{coqe}
ret x >>= f = f x
m >>= ret = m
(m >>= f) >>= g = m >>= λx.(f x >>= g)
\end{coqe}

Il est possible de réécrire la troisième loi sous la forme suivante,
où \coq{f} et \coq{g} sont des $\lambda$-termes (à condition que la variable
\coq{x} n'apparaisse pas libre dans \coq{g}):

\begin{coqe}
(m >>= λx.f) >>= λy.g = m >> λx.(f >>= λy.g)
\end{coqe}

Sur cette écriture apparaît la raison pour laquelle cette loi est appelée
\og associativité \fg. Cependant, il existe une raison plus profonde, expliquée
ci-dessous.

Remarquons enfin qu'en notant \verb+let variable = valeur in expression+
pour \coq{valeur >>= (λvariable. expression)}, les lois se réécrivent
encore:

\begin{verbatim}
let variable = ret valeur in (fonction variable)
⇔
(fonction valeur)

let variable = valeur in ret variable
⇔
ret valeur

let variable = (let variable2 = valeur in expression2) in expression
⇔
let variable2 = valeur in let variable = expression2 in expression
\end{verbatim}

En pensant au \coq{bind} comme à un analogue d'une construction \verb+let+,
les trois lois deviennent relativement naturelles.


\subsubsection{Monade option}

Cette monade est connue sous le nom de \ocaml{option} en OCaml et Coq,
et \haskell{Maybe} en Haskell. Elle se définit comme un type inductif
à deux constructeurs, qui en OCaml et Coq sont nommés \ocaml{Some}
et \ocaml{None}. La valeur \ocaml{None} représente l'absence de valeur,
un calcul non abouti, une clé non trouvée dans une base de données, etc.
Une valeur \ocaml{Some x} représente un résultat ayant abouti (pas d'erreur,
valeur trouvée, etc.) vers la valeur \ocaml{x}. Voici comment on pourrait
redéfinir cette monade en Coq:

\begin{coqe}
Inductive Option (X : Type) :=
  | Some (x : X)
  | None.

Definition ret (X : Type) (x : X) : Option X :=
  Some X x.

Definition bind (X Y : Type) (m : Option X) (f : X -> Option Y) :=
  match m with
    | None _ => None Y
    | Some _ x => f x
  end.
\end{coqe}

Ainsi, le \coq{ret} encapsule simplement son argument dans un
\coq{Some}, tandis que le \coq{bind} compose les erreurs, en renvoyant
\coq{None} si le premier calcul s'évalue à \coq{None}, et sinon la
valeur du deuxième calcul (qui peut être \coq{Some} ou \coq{None})
avec pour argument le résultat que le premier calcul a renvoyé à
l'intérieur de \coq{Some}.

Afin de rendre l'utilisation de cette monade plus pratique, on peut
définir une notation pour le \coq{bind}. Contrairement à Haskell, où
les monades sont prédéfinies avec la notation \haskell{x <- m;; f x}
pour \haskell{bind m f}, Coq ne reconnaît pas de notation par défaut
pour \coq{bind} (ni ne possède les axiomes monadiques). Cependant,
il est possible de définir des notations personnalisées. Écrivons donc
(la syntaxe importe peu ici):

\begin{coqe}
Notation "x <- m ;; t"
  := (bind _ _ m (fun x => t))
       (at level 61, m at next level, right associativity).
\end{coqe}

Avec cette syntaxe, l'exemple d'utilisation de \scheme{and-let*}
ci-dessus peut s'exprimer simplement en Coq:

\begin{coqe}
paramètres <- get paramètres_utilisateurs nom_personne;;
année_naissance <- get paramètres "année_naissance";;
année_courante - année_naissance
\end{coqe}


\subsubsection{Monade d'état}

Passons maintenant à une autre monade, qui montre mieux comment les
monades permettent de traduire simplement des programmes impératifs
dans un langage purement fonctionnel. Une valeur de la monade d'état
représente un calcul qui commence avec un état d'entrée, peut lire et
écrire dans cet état, et renvoie un valeur en laissant après lui un
certaine valeur dans l'état. Il n'y a donc pas une seule monade
d'état, mais une pour chaque type possible d'état.

\begin{coqe}
Definition State (E : Type) (X : Type) := E -> X * E.
\end{coqe}

La monade est ici \coq{State E} quel que soit \coq{E}.  Son \coq{ret}
est un calcul trivial qui renvoie immédiatement une valeur sans
modifier l'état (c'est l'origine du nom \og ret \fg, pour \og return
\fg).

\begin{coqe}
Definition ret (E : Type) (X : Type) (x : X) : État E X :=
  fun e => (x, e).
\end{coqe}

Le \coq{bind} fait la composition de deux calculs, en passant
au second l'état laissé par le premier

\begin{coqe}
Definition bind (E : Type) (X Y : Type) (m : État E X) (f : X -> État E Y) : État E Y :=
  fun e => let (x, e') := m e in f x e'.
\end{coqe}

Ainsi, on a modélisé un programme impératif, plongé dans un état
ambiant, par une fonction de l'état.

L'intérêt cette construction est de pouvoir écrire des programmes en
combinant des termes monadiques sans les définir explicitement comme
des fonctions de l'état. Cela n'est pas encore possible à ce stade,
puisque l'on n'a aucun outil pour lire ou écrire l'état sans écrire
explicitement un élément de la monade comme une fonction.
Introduisons à cet effet quelques fonctions de base. La fonction
\coq{read} est un terme monadique qui lit l'état.

\begin{coqe}
Definition read (E : Type) : État E E :=
  fun e => (e, e).
\end{coqe}

Le résultat de la fonction \coq{write} appliquée à un argument est un
terme monadique qui écrit l'argument dans l'état. La valeur de retour
du calcul n'est pas importante, on la prendra comme \coq{tt}, qui est
l'unique habitant du type \coq{unit} (semblable à celui de OCaml).

\begin{coqe}
Definition write (E : Type) (e : E) : État E unit :=
  fun _ => (tt, e).
\end{coqe}


Munis de la notation pour le \coq{bind} et de ces deux fonctions, on
peut écrire un programme d'une manière qui ressemble fortement au
style habituel d'un langage impératif. Supposons que l'état soit
une liste d'identifiants utilisateurs. On peut ajouter un identifiant
comme ceci:

\begin{coqe}
Definition add_id (id : nat) : État (list nat) unit :=
  ids <- read (list nat);;
  write (list nat) (cons id ids).
\end{coqe}

On peut également supprimer un identifiant et le renvoyer, comme ceci:

\begin{coqe}
Definition add_id (id : nat) : État (list nat) unit :=
  ids <- read (list nat);;
  write (list nat) (cons id ids).

Definition pop_id (id : nat) : État (list nat) (option nat) :=
  ids <- read (list nat);;
  match ids with
  | nil => ret (list nat) (option nat) None
  | cons id _ => ret (list nat) (option nat) (Some id)
  end.
\end{coqe}

(Dans ces exemples, les types sont écrits explicitement, ce qui est relativement
lourd. Il est possible de les rendre implicites, ce qui n'est toutefois pas fait
ici par souci de simplicité.)

\subsubsection{Lien avec la théorie des catégories}

% TODO: enlever si ce n'est pas utile.

Posons une monade \coq{M}. Définissons

\begin{coqe}
Definition K (X : Type) (Y : Type) = X → M Y.
\end{coqe}

On peut alors interpréter le typage de \coq{ret} et \coq{bind}
différemment:

\begin{coqe}
ret : ∀ (X : Type), K X X
>>= : ∀ (X Y : Type), K X Y
\end{coqe}

Posons alors un nouvel opérateur:

\begin{coqe}
compose : ∀ (X Y Z : Type), K X Y → K Y Z → K X Z
compose X Y Z f g = λx. f x >>= g
\end{coqe}

On note introduit la notation \coq{>=>} pour \coq{compose} (toujours
en suivant Haskell).

Soient \coq{f}, \coq{g}, \coq{h} de type \coq{K}. Par les lois monadiques, on a

\begin{coqe}
ret >=> f = λx. (ret x >>= f)
          = λx. f x
          = f
f >=> ret = λx. (f x >>= ret)
          = λx. f x
          = f
(f >=> g) >=> h = λx. ((f x >>= g) >>= h)
                = λx. f x >>= (λy. g y >>= h)
                = λx. f x >>= (g >=> h)
                = f >=> (g >=> h)
\end{coqe}

Ces propriétés signifient exactement que \coq{K}, munie de l'élément neutre
\coq{ret} et de la composition \coq{>=>}, forme une catégorie.  On l'appelle
\emph{catégorie de Kleisli} associée à la monade \coq{M}.  Ses objets sont les
couples de deux types \coq{X} et \coq{Y}.  Ses morphismes sont les fonctions de
\coq{X} dans \coq{M Y}.  L'étude des monades provient au départ de la théorie
des catégories. Ce n'est que plus tard que leur application à la programmation a
été inventée.


\subsection{Bases de Coq}

\subsubsection{Gallina et le calcul (inductif) des constructions}

Le propos de cette section n'est pas de fournir une introduction complète qui
permettrait au lecteur de se servir de Coq, mais simplement d'introduire
brièvement quelques notions sur son fonctionnement qui permettront de sentir
quelles preuves manuelles sont plus simples ou plus laborieuses en Coq, et
pourquoi.

Coq est un assistant de preuve développé par l'Inria. Il est fondé sur le calcul
des constructions, qui est un $λ$-calcul typé, permettant le polymorphisme, et
où les types sont des objets de première classe. Les preuves sont exprimées en
Coq comme des termes de types correspondant à la proposition qu'ils
prouvent. Ainsi, une proposition n'est rien d'autre que le type de ses preuves,
idée désignée sous le nom de \textit{correspondance de Curry-Howard}.  Ce
principe fait que les définitions et les théorèmes ont en Coq des natures très
similaires.  Ce sont tous deux des termes du calcul des constructions.

Le fondement de Coq est le langage Gallina, qui est une incarnation pratique du
calcul des constructions. (On parle en réalité de \textit{calcul des
constructions inductives} pour Coq, car Gallina ajoute au calcul des
constructions d'origine les types algébriques, ou types inductifs, qui sont
semblables à ceux de OCaml ou Haskell.)  Étant donné qu'une preuve n'est qu'un
terme, la fonction essentielle du cœur de Coq est prouver que des termes donnés
sont bien formés et bien typés.

Gallina possède une autre contrainte, qui est que toutes les fonctions doivent
terminer, d'une manière prouvable statiquement. En effet, une preuve de $A
\implies B$ par exemple, dans la correspondance de Curry-Howard, est une
fonction qui à une preuve de $A$ associe une preuve de $B$. Si la récursion
était autorisée sans restriction, il serait possible d'écrire une fonction de
type $A \implies \bot$ pour toute proposition $A$, qui ne ferait que boucler
sans jamais renvoyer de valeur. Cette fonction serait bien typée: bien qu'il ne
soit pas possible de construire une valeur de type $\bot$ (qui est un type
inductif sans \emph{aucun} constructeur), la récursion infinie permettrait de
s'affranchir de la contrainte de construire un tel terme.

\begin{coqe}
Fixpoint loop (p : 0 = 0) : False :=
  loop p.
\end{coqe}

Étant donné que le but de Coq est d'écrire des preuves, écrire une fonction de
preuve ne peut évidemment pas nécessiter de prouver que cette fonction termine,
puisque la preuve elle-même nécessiterait de prouver une terminaison, etc. Aussi
Coq emploie-t-il une méthode simple, mais généralement suffisante, pour
s'assurer de la terminaison, appelée \emph{guardedness condition}, que l'on peut
nommer en français \og condition de bien-fondation \fg. La terminaison d'une
fonction récursive Gallina est assurée par le fait qu'il existe l'un de ses
arguments tel que tous ses appels récursifs se font sur un sous-terme strict de
cet argument. Étant donné que les types de données sont tous inductifs, la
définition de fonction est tout simplement inductive sur cet argument.


% Dans
% certains cas, la vérification de la condition de bien-fondation échoue alors que
% l'on voit, en tant qu'humain, que la fonction termine. Il faut alors introduire
% un argument auxiliaire qui sert seulement à permettre la preuve de
% terminaison. Voici un exemple simple. La fonction d'Ackermann écrite avec sa
% définition habituelle ne satisfait pas la condition de bien-fondation.

% \begin{coqe}
% Fixpoint Ackermann (m n : nat) : nat :=
%   match (m, n) with
%   | (0, _) => S n
%   | (S m', 0) => Ackermann m' 1
%   | (S m', S n') => Ackermann m' (Ackermann m n')
%   end.
% \end{coqe}

% En effet, l'appel récursif \coq{Ackermann m n'} ne peut satisfaire la condition
% de bien-fondaton que si l'argument décroissant est \coq{n'}, puisque \coq{m} est
% inchangé. Cependant, dans l'appel \coq{Ackermann m' (Ackermann m n')}, il
% faudrait cette fois que l'argument décroissant soit le premier, puisque rien ne
% garantit que \coq{Ackermann m n'} soit un sous-terme de \coq{n}. Voici en
% revanche un code accepté par Coq qui calcule la fonction d'Ackermann:

% \begin{coqe}
% Fixpoint Ackermann (m n : nat) : nat :=
%   match m with
%   | 0 => S n
%   | S m' => let fix Ackermann_m (n : nat) :=
%               match n with
%               | 0 => Ackermann m' 1
%               | S n' => Ackermann m' (Ackermann_m n')
%               end
%             in Ackermann_m n
%   end.
% \end{coqe}

% L'idée est de remarquer que la récursion problématique, \coq{Ackermann
%   m n'}, diminue tout de même l'argument \coq{n} de 1 en le remplaçant
% par \coq{m'}. On peut donc définir une fonction auxiliaire locale,
% elle-même récursive, qui correspond à la currification \coq{Ackermann m}.
% Cette fonction est définie de manière bien fondée inductivement.
% Les appels à \coq{Ackermann} qui s'y trouvent diminuent bien
% l'argument \coq{m} en le remplaçant par \coq{m'}.


\subsubsection{Ltac, le langage de tactiques}

Une preuve Coq peut donc être écrite entièrement en Gallina. Cependant, écrire
le terme de preuve entier se révèle très lourd en pratique. C'est pourquoi on
préfère écrire les preuves à l'aide de \og tactiques \fg, qui sont des
programmes construisant des preuves. Coq utilise un langage de tactiques séparé
de Gallina, nommé Ltac. Contrairement au langage des termes de preuves, qui est
le fondement de l'outil, Ltac n'est pas essentiel à la correction des
preuves. On pourrait tout aussi bien utiliser un autre langage pour les
tactiques (de fait, il en existe d'autres, Ltac étant simplement le langage par
défaut). Les programmes Ltac ne sont \emph{pas} soumis à la condition de
bien-fondation. En effet, ils ne sont pas eux-mêmes des preuves, mais simplement
des générateurs de preuve. Si le générateur ne termine pas, l'utilisateur
n'obtient tout simplement pas de preuve, ce qui n'est pas grave (alors que si la
fonction de preuve ne terminait pas, on pourrait obtenir une preuve de Faux, ce
qui n'est pas l'objectif souhaité).


\subsection{Arbres d'interaction}

Lorsque l'on souhaite formaliser un langage de programmation, il est nécessaire
de choisir un type de données pour les programmes. En effet, dès qu'il est
d'écrire dans langage modélisé un programme qui ne termine pas, les fonctions du
langage ne peuvent pas être modélisées par des fonctions Gallina en raison de la
condition de bien-fondation. Il en est de même des fonctions impures, puisque
Gallina est un langage purement fonctionnel. Dans l'article \cite{itrees}, les
auteurs (dont Yannick Zakowski, qui a également collaboré à ce travail)
proposent de modéliser un programme à l'aide d'un type coinductif dont les
objets sont appelés \og arbres d'interaction \fg. On définit pour cela des
événements, qui correspondent aux effets impurs autorisés. Les arbres
d'interaction pour un certain type d'événements peuvent être munis d'une
structure de monade.

Étant donné que la problématique du stage a été progressivement simplifiée pour
se détacher des arbres d'interaction, leur théorie n'est pas développée
davantage dans ce rapport. Ils constituent cependant le point de départ du
stage.


\subsection{Origine du travail proposé}

Ce travail trouve sa source dans une formalisation du langage de programmation R
en Coq, réalisée par mon tuteur, Martin Bodin. L'objectif était de donner une
sémantique formelle de R en Coq, sur laquelle soient prouvés des théorèmes comme
le progrès et l'absence d'erreur interne de l'interpréteur. Cette formalisation
se veut proche du code source de l'interpréteur R qui est le standard \emph{de
facto}, GNU R. L'interpréteur est modélisé comme un arbre d'interaction, ce qui
permet d'encoder le fait qu'il modifie en permanence un état global formé du tas
ainsi que de certaines variables globales.

Il est apparu la nécessité, pour prouver certains théorèmes, de montrer par
exemple que le code d'initialisation de l'intepréteur ne modifiait pas le
tas. Cette démonstration était très simple car ce code ne comportait tout
simplement aucune opération qui modifie le tas. Cependant, la taille de la
preuve Coq était linéaire en la taille de la partie de l'interpréteur en
question. Bien que ce problème puisse se résoudre en partie à l'aide de
tactiques sur mesure, il est apparu plus simple de retyper cette partie de
l'interpréteur dans des arbres d'interaction avec moins d'actions
possibles. C'est de là que provient l'idée de programmer avec plusieurs monades
ordonnées et des morphismes entre elles.


\section{Apport du stage}

Comme expliqué dans la partie précédente, les monades permettent de modéliser de
façon élégante les effets impératifs dans un langage fonctionnel. Cependant, un
problème a rapidement été identifié lorsqu'elles ont été introduites: il
n'existe pas d'opération naturelle de combinaison de deux monades.  Il est
pourtant fréquent d'avoir besoin de plusieurs effets différents, comme les
erreurs (monade option) et la mutation (monade d'état).  L'approche la plus
courante consiste à se détacher des monades pour définir des transformateurs de
monades, tels que décrits par exemple dans \cite{transformers}.  Cela permet de
définir chacun des effets séparément et d'obtenir automatiquement une monade
combinée en partant de la monade identité et en appliquant successivement les
transformateurs qui correspondent aux effets (NB: ils n'ont pas de raison en
général de commuter). De cette manière, on s'épargne une définition triviale
mais longue des opérations de la monade.

Cependant, ceci implique que le programme monadique entier est écrit à l'aide
d'une monade potentiellement complexe. L'ajout d'un effet nécessaire dans une
petite partie du programme oblige à retyper les autres parties, ce qui peut être
problématique pour plusieurs raisons:

\begin{itemize}
\item
  Dans le cadre d'une modélisation formelle, il peut falloir modifier des preuves
  triviales mais longues.

\item
  Dans la perspective d'une compilation séparée de différents modules, il faut
  recompiler tout module qui utilisait la monade générale. Si le programme
  utilise cette monade partout, tout doit être recompilé alors qu'un seul module
  a été modifié.

\item
  Il est possible que l'utilisation de la monade générale implique des
  opérations qui pourraient être évitées la plupart du temps (par exemple
  l'allocation d'un tableau dans la monade état).
\end{itemize}

Étant données ces considérations, nous proposons l'utilisation d'une opération
différente du \og bind \fg pour l'écriture de programmes monadiques, qui permet
d'écrire de larges parties d'un programme en n'utilisant qu'une monade plus
petite que la monade générale.

Les deux apports principaux du stage sont d'une part, la contribution à la
réflexion sur le choix d'une axiomatisation qui soit utile en pratique, et
d'autre part, la formalisation de cette axiomatisation en Coq.


\subsection{\og Bind généralisé \fg}

Un morphisme entre les monades $M$ et $N$ est une application $\phi: \forall X,
M(X) \into N(X)$ compatible avec les opérations des monades:

\begin{align*}
\phi(\ret_A(x)) = \ret_B(x) \\
\phi(m \bind_A f) = \phi(m) \bind_B (\phi \circ f)
\end{align*}

Soient trois monades et deux morphismes:

\begin{tikzcd}
    &  C  & \\
  A\arrow[ur, "t_{AC}"] &     &  B\arrow[ul, swap, "t_{BC}"]
\end{tikzcd}

On définir le \og bind généralisé \fg comme:

\begin{align*}
\bind_{ABC} : \forall X Y, A X \into (X \into B Y) \into C Y \\
x \bind_{ABC} f := t_{AC}(x) \bind_C (λx' \cdot t_{BC}(f(x')))
\end{align*}

En termes intuitifs, le terme monadique $x$ et la fonction $f$ sont \og
transportés \fg dans la monade $C$ à l'aide des morphismes, puis l'opération \og
bind \fg est effectuée dans la monade $C$.  Ce bind généralisé est paramétré par
les monades ($A$, $B$, $C$) et les morphismes entre elles. Il satisfait à des
propriétés similaires aux lois monadiques (voir la formalisation Coq pour les
preuves, qui sont directes). Avec trois monades comme ci-dessus, on a

\begin{align*}
\ret_A x \bind_{ABC} f = t_{BC} (f(x)) \\
m \bind_{ABC} \ret_B = t_{AC} (m)
\end{align*}

Pour énoncer la troisième loi, supposons une hiérarchie de monades:

\begin{tikzcd}
  &   & T &   & \\
  & A\arrow[ur, "t_{AT}"] &   & B\arrow[ul, swap, "t_{BT}"] & \\
M\arrow[ur, "t_{MA}"] &  &  \arrow[ul, swap, "t_{NA}"] N \arrow[ur, "t_{NB}"] &  &  P\arrow[ul, swap, "t_{PB}"]
\end{tikzcd}

On a alors:

\begin{align*}
(m \bind_{MNA} f) \bind_{APT} g = m \bind_{MBT} (λy \cdot (f y) \bind_{NPB} g)
\end{align*}

\subsubsection{Variante}

Ce bind généralisé, qui consiste à plonger deux termes dans une monade plus
grande afin de pouvoir les combiner par un \og bind \fg, nécessite de préciser à
chaque fois de quelle monade il s'agit, ainsi que de définir les deux morphismes
qui permettent le plongement. Cependant, un intérêt des monades est de se prêter
à l'inférence de types. On trouvera dans \cite{wadlertut} des exemples où trois
interpréteurs d'un mini-langage sont écrits exactement de la même façon, avec
trois définitions de monade différentes, et des résultats différents. Avec, par
exemple, la notation \verb+do+ de Haskell, les programmes monadiques sont tous
écrits dans le même style, avec des fonctions \verb+ret+ et \verb+bind+ prises
automatiquement dans la bonne monade (grâce au mécanisme dit des \og classes de
type \fg, qui ne sera pas expliqué en détail ici). On s'attend donc à ce que la
plupart du temps, l'utilisateur qui se sert d'un bind généralisé définisse un
treillis de monades, avec une opération \textit{sup} qui prenne deux monades $A$
et $B$, et renvoie une monade $C$ et deux morphismes, l'un de $A$ dans $C$,
l'autre de $B$ dans $C$.

% Pour faire sentir les questions qui se sont posées afin d'aboutir à ce choix,
% mentionnons également quelques alternatives écartées.

% On aurait pu définir une notion de \og monade hétérogène \fg, avec une famille
% de types paramétrés $M_i$, où l'opération \og bind \fg ne soit pas de signature
% $\forall X Y, M(X) \into (X \into M(Y)) \into M(Y)$ comme dans une monade
% classique, mais $\forall i j k X Y, (i \leq k \land j \leq k) \into (M_i(X)
% \into (X \into M_j(Y)) \into M_k(Y))$, et exiger comme axiomes les trois lois du
% bind généralisé données plus haut.


\subsection{Formalisation en Coq}



\bibliographystyle{plain}
\bibliography{rapport}

\end{document}
