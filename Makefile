.PHONY: build

# Removes a lot of warnings from dune complaining that HOME is not defined.
HOME ?= ${TARGETDIR}
export HOME

build:
	dune build @all

clean-local:
	# Clean-up emacs-generated file not following Dune’s conventions.
	rm theories/.*.aux || true
	rm theories/.lia.cache || true
	for ext in aux glob vo vos vok; do \
		rm theories/*.$${ext} || true; \
	done

