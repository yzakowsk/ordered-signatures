# Ordered Signatures for itree-based computations

We investigate Martin's idea of organizing the set of signatures computations
can statically type-check against into a semi-lattice in order to systematically
leverage invariants of computations when composing them.

## Meta

- Author(s):
  - Martin Bodin
  - Yannick Zakowski
- License: TODO
- Compatible Coq versions: 8.14 or later
- Additional dependencies:
  - [InteractionTrees](https://github.com/DeepSpec/InteractionTrees) 
  - [TLC](https://github.com/charguer/tlc) 
- Coq namespace: `OS`

## Building instructions

### Installing dependencies

```shell
opam install coq-itree
opam install coq-tlc
```

### Obtaining the project

```shell
git clone https://gitlab.inria.fr/yzakowsk/ordered-signature
cd ordered-signature
```

### Building the project using dune

```shell
dune build
```

### Building the project using esy

```shell
esy
```
This will also install all dependencies in the `~/.esy/` folder.
To run an IDE within the right environment, prefix it with `esy` (e.g. `esy emacs theories/*.v`).
Proof General might generate files that conflict with Dune’s conventions: to clean these files, run `make clean-local`.

