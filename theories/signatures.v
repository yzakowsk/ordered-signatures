From OrderedMon Require Import
  utils.

Class Subop (Op1 Op2 : signature) := inject : Op1 ~> Op2.
Notation "E -< F" := (Subop E F) (at level 90).

Variant sum1 (E F : signature) : signature :=
  | inl1 {X} (e : E X) : sum1 E F X
  | inr1 {X} (f : F X) : sum1 E F X.
Variant void1 : signature :=.
Infix "+'" := sum1 (at level 10).
Notation "∅" := void1.

Definition inject_id {E} : E -< E :=
  fun _ x => x.

Definition inject_trans {E F G} `{E -< F} `{F -< G} : E -< G :=
  fun _ e => inject _ (inject _ e).

Definition inject_eq {E F} (inj1 inj2: E -< F) :=
  forall X (e : E X), @inject _ _ inj1 _ e = @inject _ _ inj2 _ e.

#[local] Instance inject_eq_trans : forall E F, Transitive (@inject_eq E F).
Proof.
  intros ? ? ? ? ? eq1 eq2 ? ?.
  now rewrite eq1.
Qed.

#[local] Instance inject_eq_sym : forall E F, Symmetric (@inject_eq E F).
Proof.
  now auto.
Qed.

#[local] Instance inject_eq_refl : forall E, Reflexive (@inject_eq E E).
Proof.
  now auto.
Qed.

#[global] Instance Subop_refl {E} : E -< E := inject_id.
#[global] Instance Subop_void {E} : ∅ -< E := fun _ x => match x with end.
#[global] Instance Subop_right {E F G : signature} `{E -< F} : E -< F +' G :=
  fun _ x => inl1 _ _ (inject _ x).
#[global] Instance Subop_left {E F G : signature} `{E -< F} : E -< G +' F :=
  fun _ x => inr1 _ _ (inject _ x).

