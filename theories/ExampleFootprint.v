(*|
Case study: static read and write footprints
============================================

.. coq:: none
|*)
From Coq Require Import Nat Program List.

From OrderedMon Require Import
  utils
  monad
  po
  lattice
  signatures
  free.
Import EqNotations.
Import SigTNotations.
(*|
Concrete monads
---------------

Given a system equipped with a single memory cell storing a [Nat],
we may work with four kinds of monadic computations:

- [pure] computations do not interact with the cell at all
- [read] computations may read, but not modify the cell
- [write] computations may update the cell, but not read it
- [state] computations may both read and update the cell

We reflect these intents structurally in the four distinct associated monads.

.. coq::
|*)

Definition loc    := nat.
Definition val    := nat.
Definition map := loc -> val.
Definition upd (s : map) (l : loc) (v : val) : map :=
  fun l' => if l =? l' then v else s l.
Definition lu (s : map) (l : loc) : val :=
  s l.
Definition eq_map (m1 m2 : map) := forall l, m1 l = m2 l.
Infix "≅" := eq_map (at level 20).
Definition eq_res {X : Type} (r1 r2 : map * X) :=
  eq_map (fst r1) (fst r2) /\ snd r1 = snd r2.
Infix "≡" := eq_res (at level 20).

Ltac break_let_ t :=
  match t with
  | let (_,_) := ?y in _ => break_let_ y
  | _ => destruct t eqn:?
  end.

Ltac break_let :=
  match goal with
    |- context [let (_,_) := ?t in _] =>
      break_let_ t
  end.

Ltac break_let_hyp :=
  match goal with
    | h: context [let (_,_) := ?t in _] |- _ =>
        break_let_ t
  end.

Module Setoid.

  Record Setoid := {
      type_of:> Type;
      equiv: relation type_of;
      equiv_is_equiv: Equivalence equiv
    }.
  Record FunSetoid (X Y : Setoid) := {
      body        :> X -> Y;
      body_proper : Proper (equiv X ==> equiv Y) body
    }.

  Definition mapS : Setoid :=
    {|
      type_of := map;
      equiv := pw eq
    |}.

  Definition mapX X : Type := map * X.
  Definition mapX_equiv (X : Type) : relation (mapX X) :=
    fun '(s,x) '(s', x') => equiv mapS s s' /\ x = x'.

  #[global] Instance mapX_equiv_equiv (X : Type) : Equivalence (mapX_equiv X).
  Proof.
    split.
    intros []; now cbn.
    intros [] [] []; subst; now cbn.
    intros [] [] [] [] [].
    subst; split; auto; cbn.
    now intros l; rewrite (H l), (H1 l).
  Qed.

  Definition mapXS (X : Type) : Setoid :=
    {| type_of := mapX X;
      equiv := mapX_equiv X;
      equiv_is_equiv := mapX_equiv_equiv X |}.

  Definition state (X : Type) := FunSetoid mapS (mapXS X).
  Definition mk_st [X] body := Build_FunSetoid mapS (mapXS X) body.

  Definition state_monad : Monad state.
    split.
    - refine (fun _ x => mk_st (fun s => (s,x)) _).
      intros s s' eqs; split; auto.
    - refine (fun _ _ c k => mk_st (fun s => let '(s',x) := c s in k x s') _).
      intros s s' eqs.
      cbn.
      destruct c as [c Hc].
      cbn.
      specialize (Hc _ _ eqs); cbv in Hc.
      destruct (c s) eqn:EQ.
      destruct (c s') eqn:EQ'.
      red.
      destruct Hc as [Hc <-].
      cbn in *.
      destruct (k t) as [k' Hk].
      cbn in *.
      specialize (Hk _ _ Hc); cbv in Hk.
      destruct (k' m) eqn:EQ1.
      destruct (k' m0) eqn:EQ2.
      destruct Hk as [Hk <-].
      split; auto.
    - refine (fun _ c1 c2 =>
                forall (s1 s2 : mapS), equiv _ s1 s2 -> equiv _ (c1 s1) (c2 s2)).
  Defined.

  #[global, refine] Instance state_mon : Mon state :=
    {|
      MonM := state_monad;
    |}.
  Proof.
    split.
    - intros ?? c k s1 s2 eqs. cbn in *.
      destruct (k c) as [kc Hkc]; apply Hkc; auto.
    - intros ? c s1 s2 eqs.
      cbn in *.
      destruct c as [c Hc]; cbn.
      apply Hc in eqs.
      cbv in eqs.
      destruct (c s1), (c s2).
      cbv; intuition.
    - intros ??? c k g s1 s2 eqs; cbn in *.
      destruct c as [c Hc]; cbn in *.
      apply Hc in eqs. cbv in eqs. clear Hc.
      repeat break_let_hyp.
      intuition; subst.
      clear c Heqm Heqm0.
      destruct (k x0) as [k' Hk]; cbn in *.
      apply Hk in H.
      cbv in H.
      repeat break_let.
      destruct H as [H <-].
      clear Hk.
      cbv.
      destruct (g y) as [g' Hg]; cbn in *.
      apply Hg in H.
      cbv in H.
      repeat break_let.
      destruct H as [H <-].
      intuition.
    - split.
      + intros [x H] s1 s2 EQs. cbn in *.
        now apply H in EQs.
      + intros [m1 H1] [m2 H2] EQ s1 s2 EQs; cbn in *.
        symmetry in EQs.
        apply EQ in EQs.
        now symmetry.
      + intros [m1 H1] [m2 H2] [m3 H3] EQ1 EQ2 s1 s2 EQs; cbn in *.
        specialize (EQ1 _ _ EQs).
        specialize (EQ2 s2 s2 ltac:(reflexivity)).
        etransitivity; eauto.
    - intros ?? c1 c2 EQ1 k1 k2 EQ2 s1 s2 EQs.
      cbn in *.
      apply EQ1 in EQs.
      repeat break_let.
      destruct EQs as [? <-].
      now specialize (EQ2 x _ _ H).
  Defined.

End Setoid.

Module Syntax.

  Variant RW : signature :=
    | rd (l: loc) : RW val
    | wr (l : loc) (v : val) : RW unit.
  Definition term := free RW.

  Definition state X := map -> (map * X).

  Definition keq {T} (m n : state T) : Prop :=
    forall s1 s2, s1 ≅ s2 -> m s1 ≡ n s2.

  #[global] Instance state_monad : Monad state :=
    {|
      ret _ x      := fun s => (s,x);
      bind _ _ m k := fun s => let '(s',x) := m s in k x s';
      eqM          := @keq
    |}
  .

  Definition hRW : RW ~> state :=
    fun T e => match e with
            | rd l   => fun s => (s, lu s l)
            | wr l v => fun s => (upd s l v, tt)
            end.
  Definition sem : term ~> state := interp hRW.
  Arguments sem [X].
  Arguments exist [A P].
  Definition state' (X : Type) : Type :=
    {c : (state X * term X) | keq (sem (snd c)) (fst c)}.
  Definition gets {X} (s : state' X) : state X :=
    fst (` s).
  Definition gett {X} (s : state' X) : term X :=
    snd (` s).

  Ltac bk :=
    match goal with
    | h: state' _ |- _ =>
        destruct h as [[?c ?t] ?pf]
    end.

  #[global, program] Instance state'_monad : Monad state' :=
    {|
      ret T (x : T) := exist (ret x, pure x) _;
      bind _ _ m k  := _;
        (* match m with *)
        (* | exist  *)
        (* let '(exist a pf) := m in *)
        (* exist ((bind a (fun x => _)), _) _; *)
      eqM _ m n     := _;
    |}
  .
  Next Obligation.
    intros ?? eqs; cbn.
    split; auto.
  Qed.
  Next Obligation.
    refine
      (exist
         (bind (gets m) (fun x => gets (k x)),
           bind (gett m) (fun x => gett (k x))) _).
    cbn.
    bk.
    cbn in *.
    revert c pf.
    induction t; intros; intros s1 s2 eqs.
    - cbn.
      split.
      destruct (k x) as [[?c ?t] ?pf] eqn:EQ.
      cbn in *.
      pose proof pf0 s1 s2 eqs.
      destruct H.
      rewrite H.
      pose proof pf0 s1 s2 eqs.
      

    intros s1 s2 eqs.
    apply pf in eqs.
    subst.
    induction t.
    - cbn.

    cbn [snd].
    unfold sem at 1.
    rewrite interp_bind.
    cbn in *.
    subst.
    rewrite interp_bind.
    rewrite 
    bk. cbn in *; subst.



  exists (pure x); reflexivity.
  Qed.
  Next Obligation.
    destruct m as [m [t <-]].
    pose proof (bind (Y := T0) t).
    refine (let y := fun x => let 'exist a (ex_intro _ t _) := k x in t in _).
    specialize (X (fun x => 
    cbn in *.
    exists (bind t (fun x => proj1_sig (k x))).
    eexists.
    destruct (sem t) eqn:EQ.
  Definition writes := list loc.
  Definition reads  := list loc.
  Definition map    := loc -> val.
  (* Definition fin_map (support : list loc) : Type. *)


#[local] Instance eq_map_refl : Reflexive eq_map := fun _ l => eq_refl.
#[local] Instance eq_map_sym : Symmetric eq_map.
intros ? ? EQ l; now rewrite (EQ l).
Qed.
#[local] Instance eq_map_trans : Transitive eq_map.
intros ? ? ? EQ1 EQ2 l; now rewrite (EQ1 l).
Qed.
#[local] Instance eq_res_refl {X} : Reflexive (@eq_res X).
intros []; now split.
Qed.

Definition agrees_on (support : list loc) (s s' : map) :=
  forall l, ~ In l support -> s l = s' l.

Definition stw     : writes -> monad := fun ω X => forall s: map, {s' : map & agrees_on ω s s'} * X.
Definition str     : reads -> monad := fun ρ X => map -> map * X.
Definition strw    : writes -> reads -> monad := fun ω ρ X => map -> map * X.
Definition state   : monad := fun X => map -> map * X.

Definition state_monad : Monad state :=
  {|
    ret _ x      := fun s => (s,x);
    bind _ _ m k := fun s => let '(s',x) := m s in k x s';
    eqM _ m n    := forall s, m s = n s
    (* eqM _ m n    := forall s1 s2, s1 ≅ s2 -> m s1 ≡ n s2 *)
    (* eqM _ m n    := forall s, m s ≡ n s *)
  |}
.

#[global, refine] Instance state_mon : Mon state :=
  {|
    MonM := state_monad;
  |}.
Proof.
  split.
  - intros ?? c k s.
    now cbn.
  - intros * c; cbn.
    now destruct (m c).
  - intros * c; cbn.
    destruct (m c); cbn.
    now destruct (k x m0); cbn.
  - split; cbv; intuition.
    now rewrite H.
  - intros ?? c1 c2 EQ1 k1 k2 EQ2 s.
    cbn; rewrite EQ1.
    destruct (c2 s).
    apply EQ2.
Defined.
Arguments existT [A P].

Lemma agrees_on_refl support : Reflexive (agrees_on support).
Proof.
  intros s l ?; reflexivity.
Qed.

Lemma agrees_on_trans support : Transitive (agrees_on support).
Proof.
  intros ??? H1 H2 l NIN.
  rewrite H1,H2;auto.
Qed.
Arguments agrees_on_trans support [_ _ _] pf1 pf2.

Definition stw_monad ω : Monad (stw ω).
  split.
  refine (fun _ x s => (existT s (agrees_on_refl ω s), x)).
  refine (fun _ _ c k s =>
            let '((s';pf'), x) := c s in
            let '((s'';pf''), y) := k x s' in
            ((s''; agrees_on_trans ω pf' pf''), y)).
  refine (fun _ c1 c2 => _).

  assert (forall support s, agrees_on support s s).
  intoos..
  :=
  {|
    ret _ x      := fun s => (s,x);
    bind _ _ m k := fun s => let '(s',x) := m s in k x s';
    eqM _ m n    := forall s, m s = n s
    (* eqM _ m n    := forall s1 s2, s1 ≅ s2 -> m s1 ≡ n s2 *)
    (* eqM _ m n    := forall s, m s ≡ n s *)
  |}
.


Definition st_monad ω ρ : Monad (st ω ρ) :=
  {|
    ret _ x      := fun s => (s,x);
    bind _ _ m k := fun s => let '(s',x) := m s in k x s';
    eqM _ m n    := forall s, m s = n s
    (* eqM _ m n    := forall s1 s2, s1 ≅ s2 -> m s1 ≡ n s2 *)
    (* eqM _ m n    := forall s, m s ≡ n s *)
  |}
.
