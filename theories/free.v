From OrderedMon Require Import
     utils monad po lattice signatures.
From Coq Require Import Program.Equality.

Inductive free (E : signature) (X : Type) : Type :=
| pure (x : X)
| op {Y} (e : E Y) (k : Y -> free E X).
Arguments pure {E X}.
Arguments op {E X Y}.

Inductive eqMfree {E X} : relation (free E X) :=
| eq_pure x : eqMfree (pure x) (pure x)
| eq_op Y (e : E Y) k k'
    (EQk : pw (@eqMfree E X) k k') :
  eqMfree (op e k) (op e k').

Fixpoint bindfree {E X Y} (m : free E X) (k : X -> free E Y)
  : free E Y :=
  match m with
  | pure x => k x
  | op e g => op e (fun x => bindfree (g x) k)
  end.

#[global] Instance freeMonad {E} : Monad (free E) :=
  {|
    ret  := @pure E;
    bind := @bindfree E;
    eqM  := fun X => @eqMfree _ _
  |}.

Lemma freeEqMbind {E X Y} : Proper (eqMfree ==> pw eqMfree ==> eqMfree) (@bindfree E X Y).
Proof.
  intros ? ? EQ ? ? EQk.
  induction EQ.
  - apply EQk.
  - now cbn; constructor; intros ?.
Qed.

#[global] Instance freeEqMEquiv {E X} : Equivalence (@eqMfree E X).
Proof.
  split.
  - red.
    induction x; now constructor.
  - red; induction 1; now constructor.
  - intros x y z EQ1; revert z.
    induction EQ1.
    intros ? ?; auto.
    intros ? EQ2.
    dependent induction EQ2.
    constructor.
    intros ?.
    now apply H.
Qed.

#[global] Instance freeMonadLaws {E} : MonadLaws (free E).
Proof.
  split.
  - intros; apply freeEqMEquiv.
  - induction m as [x | ? e k].
    apply freeEqMEquiv.
    cbn; constructor.
    intros ?; apply H.
  - intros.
    induction m as [x | ? e mk].
    + apply freeEqMEquiv.
    + intros; cbn.
      constructor; intros x.
      cbn in H.
      apply (H x).
  - intros; apply freeEqMEquiv.
  - intros; apply freeEqMbind.
Qed.

#[global] Instance freeMon {E} : Mon (free E) :=
  {|
    MonM  := @freeMonad E;
    MonML := @freeMonadLaws E;
  |}.

Definition perform {E X} (e : E X) : free E X :=
  op e (fun x => ret x).

Fixpoint interp {E M} `{Monad M} (h : E ~> M) [X] (m : free E X)
  : M X :=
  match m with
  | pure x => ret x
  | op e k => bind (h _ e) (fun x => interp h (k x))
  end.

Definition translate {E F} (h : E ~> F) : free E ~> free F :=
  interp (M := free F) (fun _ e => perform (h _ e)).
Arguments translate {_ _} h [X].

Definition inline {E F} (implem : E ~> free F) : free E ~> free F :=
  interp implem.

Lemma interp_ret {E M} `{Monad M} `{MonadLaws M} (h : E ~> M) {X} (x : X) :
  interp h (ret x) ≈ ret x.
Proof.
  reflexivity.
Qed.

Lemma interp_bind {E M} `{Monad M} `{MonadLaws M} (h : E ~> M)
  {X Y} (m : free E X) (k : X -> free E Y) :
  interp h (bind m k) ≈ bind (interp h m) (fun x => interp h (k x)).
Proof.
  induction m.
  - cbn.
    now rewrite bind_ret_l.
  - cbn in *.
    rewrite bind_bind.
    apply eqM_bind.
    reflexivity.
    intros ?.
    rewrite H1.
    reflexivity.
Qed.

Lemma translate_ret {E F} (h : E ~> F) {X} (x : X) :
  translate h (ret x) ≈ ret x.
Proof.
  apply interp_ret.
Qed.

Lemma translate_bind {E F} (h : E ~> F) {X Y}
  (m : free E X) (k : X -> free E Y) :
  translate h (bind m k) ≈ bind (translate h m) (fun x => translate h (k x)).
Proof.
  apply interp_bind.
Qed.

(* TODO move this *)
Definition icomp {A B C : Type -> Type} (f : A ~> B) (g : B ~> C) : A ~> C :=
  fun _ a => g _ (f _ a).

Lemma translate_translate {E F G} (h1 : E ~> F) (h2 : F ~> G) {X}
  (m : free E X) :
  translate h2 (translate h1 m) ≈ translate (icomp h1 h2) m.
Proof.
  induction m.
  - reflexivity.
  - cbn.
    constructor; intros ?.
    apply H.
Qed.

Lemma translate_proper {E F}
  (inj1 inj2: E ~> F) X (m : free E X) :
  inject_eq inj1 inj2 ->
  translate inj1 m ≈ translate inj2 m.
Proof.
  intros * EQ; induction m.
  - reflexivity.
  - cbn.
    rewrite EQ.
    constructor.
    intros ?; apply H.
Qed.

Lemma translate_id {E R} (m : free E R) :
  translate inject_id m ≈ m.
Proof.
  induction m.
  - reflexivity.
  - cbn in *.
    constructor; intros ?; auto.
Qed.

(* TODO think about inject_eq *)

Class packF :=
  {
    (* The semi-lattice domain by which we index the monads of interest *)
    D : Type;
    D_le : relation D where "x ⊑ y" := (D_le x y);
    D_PO_le :> PreOrder D_le;
    D_PO :> PartialOrder eq D_le;
    Djoin : D -> D -> D where "x ⊔ y" := (Djoin x y);
    Djoin_le_l : forall d1 d2, d1 ⊑ d1 ⊔ d2;
    Djoin_le_r : forall d1 d2, d2 ⊑ d1 ⊔ d2;
    Djoin_assoc : forall d1 d2 d3,  d1 ⊔ (d2 ⊔ d3) = d1 ⊔ d2 ⊔ d3;

    (* First layer of reification: in terms of signatures *)
    Dreify : D -> signature;
    (* To the order must correspond injections *)
    Dinj : forall {d1 d2}, d1 ⊑ d2 ->
                      Dreify d1 -< Dreify d2;

    (* Coherence properties of these injections *)
    Dinj_proper : forall d1 d2 (I12 I12' : d1 ⊑ d2),
      inject_eq (Dinj I12) (Dinj I12');

    Dinj_refl :
      forall d (I : d ⊑ d),
        inject_eq (Dinj I) inject_id;

    Dinj_trans : forall d1 d2 d3 (I12 : d1 ⊑ d2) (I23 : d2 ⊑ d3),
      inject_eq
        (icomp (Dinj I12) (Dinj I23))
        (Dinj (PreOrder_Transitive _ _ _ I12 I23));

    (* Second layer of reification: in terms of concrete monads *)
    Mreify : D -> monad;
    (* And indeed those must be monads *)
    Dmonad :> forall d: D, Mon (Mreify d);
    (* And to the order correspond morphisms *)
    Dmorph :> forall {d1 d2}, d1 ⊑ d2 ->
                        MonMorph (Mreify d1) (Mreify d2);

    Dfmap {d1 d2} (I : d1 ⊑ d2) [X] := @fmap _ _ (@Morph _ _ _ _ (Dmorph I)) X;

    (* Coherence properties of these morphisms *)
    (* Note: this should be relaxed to pointwise eqM instead of pointwise eq *)
    Dmorph_proper : forall d1 d2 (I J : d1 ⊑ d2),
      inject_eq (Dfmap I) (Dfmap J);

    Dmorph_refl :
      forall d (I : d ⊑ d) [X] (m : _ X),
        Dfmap I m ≈ m;

    Dmorph_trans : forall d1 d2 d3 (I12 : d1 ⊑ d2) (I23 : d2 ⊑ d3),
      inject_eq
        (icomp (Dfmap I12) (Dfmap I23))
        (Dfmap (PreOrder_Transitive _ _ _ I12 I23));

    (* Both layers are connected by handlers *)
    Dhandle : forall d: D, Dreify d ~> Mreify d;

    Freify d :=
      free (Dreify d);
    ℑ [d X] : Freify d X -> Mreify d X :=
      interp (Dhandle d) (X := X);
    Ffmap {d1 d2} (I : d1 ⊑ d2) [X] : Freify d1 X -> Freify d2 X :=
      fun m => translate (Dinj I) m;

    Dnat : forall d1 d2 (I : d1 ⊑ d2) X (m: Freify d1 X),
      ℑ (Ffmap I m) ≈ Dfmap I (ℑ m)

  }.

Section lattice_free.

  Infix "⊑" := D_le.
  Context {PACK : packF}.

  Notation "'[[' d ']]'" := (Freify d).

  #[local] Instance reify_into_monads : forall l, Mon [[l]].
  Proof.
    intros; typeclasses eauto.
  Defined.

  #[local] Instance subevent_morph {E F} (h : E ~> F)
    : MonadMorphism (free E) (free F) :=
    {| fmap := translate h |}.

  #[local] Instance reify_le : forall (d1 d2 : D), d1 ⊑ d2 -> MonMorph [[d1]] [[d2]].
  Proof.
    intros * sub.
    unshelve econstructor.
    eapply subevent_morph.
    now apply Dinj.
    split.
    - intros; reflexivity.
    - cbn; intros.
      apply translate_bind.
    - unfold Proper, respectful.
      cbn.
      intros * EQ.
      induction EQ.
      + cbn; constructor.
      + cbn; constructor; intros ?; auto.
  Defined.

  #[refine, global] Instance packLF_of_packF : packL :=
    {|
      L := D;
      L_le := D_le;
      L_PO_le := D_PO_le;
      PO_L := D_PO;
      join := Djoin;
      reify := Freify;
    |}.
  Proof.
    apply Djoin_le_l.
    apply Djoin_le_r.
    apply Djoin_assoc.
    - intros.
      cbn.
      erewrite translate_proper.
      2:apply Dinj_refl.
      apply translate_id.
    - intros.
      cbn.
      etransitivity.
      + apply translate_translate.
      + apply translate_proper.
        unfold inject_trans.
        eapply inject_eq_trans; [| now apply Dinj_proper ].
        eapply inject_eq_trans; [| now apply Dinj_trans ].
        now intros ? ?.
  Defined.

  (* We can retrieve the [bindL] operator as follows: *)
  (* Check @bindL packLF_of_packF. *)
  (* : forall l1 l2 X Y, Freify l1 X -> (X -> Freify l2 Y) -> Freify (Djoin l1 l2) Y *)
  
  #[refine, global] Instance packLM_of_packF : packL :=
    {|
      L := D;
      L_le := D_le;
      L_PO_le := D_PO_le;
      PO_L := D_PO;
      join := Djoin;
      reify := Mreify;
    |}.
  Proof.
    apply Djoin_le_l.
    apply Djoin_le_r.
    apply Djoin_assoc.
    apply Dmorph_refl.
    intros.
    unfold climbPO.
    etransitivity.
    epose proof Dmorph_trans _ _ _ I12 I23 X m.
    unfold inject,Dfmap,icomp in *.
    rewrite H; reflexivity.
    epose proof Dmorph_proper l1 l3 (@PreOrder_Transitive (@D PACK) (@D_le PACK) (@D_PO_le PACK) l1 l2 l3 I12 I23) I13 X m.
    unfold inject,Dfmap,icomp in *.
    rewrite <- H.
    reflexivity.
  Defined.

  (* We can retrieve the [bindL] operator as follows: *)
  (* Check @bindL packLM_of_packF. *)
  (* : forall l1 l2 X Y, Mreify l1 X -> (X -> Mreify l2 Y) -> Mreify (Djoin l1 l2) Y *)

End lattice_free.
