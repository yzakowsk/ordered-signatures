From OrderedMon Require Import
     utils.

Class Monad (M : monad) : Type
  :=
  {
    ret  {X : Type} (x : X) : M X ;
    bind {X Y : Type} (m : M X) (k : X -> M Y) : M Y ;
    eqM  {X : Type} : M X -> M X -> Prop
  }.

Module MonadNotations.

  Notation "c >>= f" := (@bind _ _ _ _ c f) (at level 58, left associativity).
  Notation "e1 ;; e2" := (@bind _ _ _ _ e1 (fun _ => e2))
                           (at level 61, right associativity).
  Notation "x <- c1 ;; c2" := (@bind _ _ _ _ c1 (fun x => c2))
                               (at level 61, c1 at next level, right associativity).
  Notation "' pat <- c1 ;; c2" :=
    (@bind _ _ _ _ c1 (fun x => match x with pat => c2 end))
      (at level 61, pat pattern, c1 at next level, right associativity).

End MonadNotations.
Infix "≈" := eqM (at level 80, no associativity).
Import MonadNotations.

Class MonadLaws M {MM : Monad M} : Prop := {
    bind_ret_l {X Y} (x : X) (k : X -> M Y) : ret x >>= k ≈ k x ;

    bind_ret_r {X} (m : M X) : m >>= ret ≈ m ;

    bind_bind {X Y Z} (m : M X) (k : X -> M Y) (g : Y -> M Z) :
    (m >>= k) >>= g ≈ v <- m;; (k v >>= g) ;

    eqM_equiv X :> Equivalence (eqM (X := X)) ;

    eqM_bind {X Y} :> Proper (eqM ==> pw eqM ==> eqM) (@bind M _ X Y)
  }.

Class MonadMorphism (M N : monad) : Type := {
    fmap : forall [X], M X -> N X
  }.

Class MonadMorphismLaws M N `{Monad M} `{Monad N} `{MonadMorphism M N} : Prop := {
    fmap_ret : forall X (x : X), fmap (ret x) ≈ ret x ;

    fmap_bind : forall X Y (m : M X) (k : X -> M Y),
      fmap (m >>= k) ≈ x <- fmap m ;; fmap (k x) ;

    fmap_eqM :> forall X, Proper (eqM ==> eqM) (@fmap M N _ X)
  }.

Class Mon (M : monad) : Type :=
  {
    MonM  :> Monad M;
    MonML :> @MonadLaws _ MonM;
  }.

Class MonMorph (M N : monad) `{Mon M} `{Mon N} : Type :=
  {
    Morph  :> @MonadMorphism M N;
    MorphL :> @MonadMorphismLaws M N _ _ Morph;
  }.

Ltac bind_ret_r1 :=
  match goal with
    |- eqM ?m ?n =>
      let x := fresh in
      remember n as x;
      rewrite <- (bind_ret_r m); subst x
  end.

Ltac bind_ret_r2 :=
  match goal with
    |- eqM ?m ?n =>
      let x := fresh in
      remember m as x;
      rewrite <- (bind_ret_r n); subst x
  end.

Ltac cut :=
  apply eqM_bind; [reflexivity | intros ?].

Lemma MonMorph_trans : forall M N T `{Mon M} `{Mon N} `{Mon T},
  MonMorph M N ->
  MonMorph N T ->
  MonMorph M T.
Proof.
  intros * MN NT.
  unshelve econstructor.
  { constructor. exact (fun _ m => fmap (fmap m)). }
  constructor.
  - intros. simpl. now do 2 rewrite fmap_ret.
  - intros. simpl. now do 2 rewrite fmap_bind.
  - intros * ? * E. simpl. now do 2 (rewrite fmap_eqM; [ reflexivity |]).
Defined.

Lemma MonMorph_refl : forall M `{Mon M},
  MonMorph M M.
Proof.
  intros.
  unshelve econstructor.
  { constructor. exact (fun _ => id). }
  now constructor.
Defined.

Definition bindH {M N T}
  {MM : Mon M} {MN : Mon N} {MT : Mon T}
  {MMT : MonMorph M T} {MNT : MonMorph N T}
  : forall {X Y}, M X -> (X -> N Y) -> T Y :=
  fun _ _ m k => v <- fmap m;; fmap (k v).

Section HLaws.

  Lemma bindH_ret_l
    {M N T : monad}
    {MM : Mon M} {MN : Mon N} {MT : Mon T}
    {FMT : MonMorph M T} {FNT : MonMorph N T}
    :
    forall {X Y} (x : X) (k : X -> N Y),
      bindH (M := M) (T := T) (ret x) k ≈ fmap (k x).
  Proof.
    intros.
    unfold bindH.
    rewrite fmap_ret, bind_ret_l.
    reflexivity.
  Qed.

  Lemma bindH_ret_r
    {M N T : monad}
    {MM : Mon M} {MN : Mon N} {MT : Mon T}
    {FMT : MonMorph M T} {FNT : MonMorph N T}
    :
    forall {X} (c : M X),
      bindH (N := N) c (fun x => ret x) ≈ fmap c.
  Proof.
    intros.
    unfold bindH.
    bind_ret_r2.
    apply eqM_bind; [reflexivity | intros ?].
    rewrite fmap_ret.
    reflexivity.
  Qed.

  (* For the associativity of bindH, we provide two versions. *)
  Lemma bindH_bindH_all
    (* We have a tiny pyramid of six monads *)
    {A B C AB BC T}
    {MA : Mon A} {MB : Mon B} {MC : Mon C}
    {MAB : Mon AB} {MBC : Mon BC} {MT : Mon T}

    (* We must at least be able to send anyone to its parent,
       and the left and right nodes all the way up in one jump
     *)
    {FAAB : MonMorph A AB} {FBAB : MonMorph B AB}
    {FBBC : MonMorph B BC} {FCBC : MonMorph C BC}
    {FABT : MonMorph AB T} {FBCT : MonMorph BC T}
    {FAT  : MonMorph A T}  {FCT  : MonMorph C T}

    {FBT  : MonMorph B T}

    (* The jump two "double jumps" must commute with the obvious
       composition of [fmap]s. This should probably be phrased nicer
       by introducing the equality on arrows in the Kleisli category
       of a monad.
     *)
    (commAB : forall (X : Type) (m : A X),
        fmap (fmap (N := AB) m) ≈ fmap (N := T) m)
    (commAB' : forall (X : Type) (m : B X),
        fmap (fmap (N := AB) m) ≈ fmap (N := T) m)
    (commBC : forall (X : Type) (m : B X),
        fmap (fmap (N := BC) m) ≈ fmap (N := T) m)
    (commBC' : forall (X : Type) (m : C X),
        fmap (fmap (N := BC) m) ≈ fmap (N := T) m)

    (* Then we finally get what we want: commutation of binds *)
    [X Y Z : Type] (mx : A X) (kxy : X -> B Y) (kyz : Y -> C Z):
    bindH (bindH mx kxy) kyz ≈ bindH mx (fun x => bindH (kxy x) kyz).
  Proof.
    unfold bindH.
    rewrite !fmap_bind.
    rewrite !bind_bind.
    rewrite commAB.
    cut.
    rewrite commAB'.
    rewrite fmap_bind.
    rewrite commBC.
    cut.
    rewrite commBC'.
    reflexivity.
  Qed.

  (* A refined version asking for less hypotheses. *)
  Lemma bindH_bindH
    {A B C AB BC T}
    {MA : Mon A} {MB : Mon B} {MC : Mon C}
    {MAB : Mon AB} {MBC : Mon BC} {MT : Mon T}
    {FAAB : MonMorph A AB} {FBAB : MonMorph B AB}
    {FBBC : MonMorph B BC} {FCBC : MonMorph C BC}
    {FABT : MonMorph AB T} {FBCT : MonMorph BC T}
    {FAT  : MonMorph A T}  {FCT  : MonMorph C T}

    (commAB : forall (X : Type) (m : A X),
        fmap (fmap (N := AB) m) ≈ fmap (N := T) m)
    (commBC : forall (X : Type) (m : C X),
        fmap (fmap (N := BC) m) ≈ fmap (N := T) m)
    (* Insteed of asking for the two double-jumps from B to T to commute,
     we require that the central square from B to T commutes.
     This enables us to avoid assuming a morphism from B to T. *)
    (commBT : forall (X : Type) (m : B X),
        fmap (N := T) (fmap (N := AB) m) ≈ fmap (N := T) (fmap (N := BC) m))

    [X Y Z : Type] (mx : A X) (kxy : X -> B Y) (kyz : Y -> C Z):
    bindH (bindH mx kxy) kyz ≈ bindH mx (fun x => bindH (kxy x) kyz).
  Proof.
    intros.
    set (FBT := MonMorph_trans B AB T _ _).
    apply bindH_bindH_all; auto.
    - intros. simpl. now rewrite commBT.
    - intros. simpl. now rewrite commBT.
  Qed.

  (* A version not asking for the double-jumps from A and C to T,
   less useful in practice. *)
  Lemma bindH_bindH_min
    {A B C AB BC T}
    {MA : Mon A} {MB : Mon B} {MC : Mon C}
    {MAB : Mon AB} {MBC : Mon BC} {MT : Mon T}
    {FAAB : MonMorph A AB} {FBAB : MonMorph B AB}
    {FBBC : MonMorph B BC} {FCBC : MonMorph C BC}
    {FABT : MonMorph AB T} {FBCT : MonMorph BC T}

    (commBT : forall (X : Type) (m : B X),
        fmap (N := T) (fmap (N := AB) m) ≈ fmap (N := T) (fmap (N := BC) m))

    [X Y Z : Type] (mx : A X) (kxy : X -> B Y) (kyz : Y -> C Z):
    let FAT := MonMorph_trans A AB T _ _ in
    let FCT := MonMorph_trans C BC T _ _ in
    bindH (bindH mx kxy) kyz ≈ bindH mx (fun x => bindH (kxy x) kyz).
  Proof.
    intros. now apply bindH_bindH.
  Qed.

  (* We indeed have built an extension of the usual monadic bind. *)
  Lemma bindH_extends_bind
    {M} {MM : Mon M}
    [X Y : Type] (m : M X) (k : X -> M Y) :
    bindH (MNT := MonMorph_refl _) (MMT := MonMorph_refl _) m k ≈ bind m k.
  Proof.
    reflexivity.
  Qed.

End HLaws.

