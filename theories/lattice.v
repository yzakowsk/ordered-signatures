From OrderedMon Require Import
  utils
  monad
  po.
Import EqNotations.

Reserved Infix "⊑" (at level 70, no associativity).
Reserved Infix "⊔" (at level 36, left associativity).
Reserved Notation "'<[' l ']>'".

Class packL :=
  {
    (* The semi-lattice domain by which we index the monads of interest *)
    L : Type;
    L_le : relation L where "x ⊑ y" := (L_le x y);
    L_PO_le :> PreOrder L_le;
    PO_L :> PartialOrder eq L_le;
    join : L -> L -> L where "x ⊔ y" := (join x y);
    join_le_l : forall l1 l2, l1 ⊑ l1 ⊔ l2;
    join_le_r : forall l1 l2, l2 ⊑ l1 ⊔ l2;
    join_assoc : forall l1 l2 l3,  l1 ⊔ (l2 ⊔ l3) = l1 ⊔ l2 ⊔ l3;

    (* We map indexes to monads *)
    reify : L -> monad where "'<[' l ']>'" := (reify l);
    reify_into_monads :> forall l : L, Mon (reify l);

    (* The ordering on L must entail that we know how to transport computations accordingly *)
    reify_le : forall l1 l2 : L, L_le l1 l2 -> MonMorph (reify l1) (reify l2);

    climbL [l1 l2] LE [X] m :=
      @climbPO _ L_le reify reify_into_monads reify_le l1 l2 LE X m;

    (* The way we transport computations should not depend on the proof of inequality we use *)
    reify_le_refl :
    forall l (I : l ⊑ l) X (m : <[l]> X),
      climbL I m ≈ m;

    reify_le_trans :
    forall l1 l2 l3 (I12 : l1 ⊑ l2) (I13 : l1 ⊑ l3) (I23 : l2 ⊑ l3) X (m : <[l1]> X),
      climbL I23 (climbL I12 m)
        ≈ climbL I13 m;

  }.

Section Lattice.

  Infix "⊑" := L_le.
  Infix "⊔" := join.
  Notation "'<[' l ']>'" := (reify l).


  Context {args : packL}.

  Definition climbL_left [l1 l2] :
    forall [X], <[l2]> X -> <[l1 ⊔ l2]> X :=
    climbL (join_le_r _ _).

  Definition climbL_right [l1 l2] :
    forall [X], <[l1]> X -> <[l1 ⊔ l2]> X :=
    climbL (join_le_l _ _).

  Definition bindL {l1 l2} :
    forall [X Y], <[l1]> X -> (X -> <[l2]> Y) -> <[l1 ⊔ l2]> Y :=
    @bindPO L L_le reify reify_into_monads reify_le
            l1 l2 _
      (join_le_l _ _) (join_le_r _ _).

  Lemma bindL_ret_l [l1 l2 : L] :
    forall X Y (k : X -> <[l2]> Y) (x : X),
      bindL (ret (M := <[l1]>) x) k ≈ climbL_left (k x).
  Proof.
    apply bindPO_ret_l.
  Qed.

  Lemma bindL_ret_r [l1 l2] :
    forall [X] (m : <[l1]> X),
      bindL m (ret (M := <[l2]>)) ≈ climbL_right m.
  Proof.
    apply bindPO_ret_r.
  Qed.

  (* This is where you can't get away for free anymore, we need some coercion in the types. *)

  Definition climbL_eq [l1 l2] (E : l1 = l2) :
    forall [X], <[l1]> X -> <[l2]> X :=
    climbL (eq_subrelation (@PreOrder_Reflexive _ _ L_PO_le) _ _ E).

  Definition climbL_assoc [l1 l2 l3] :
    forall [X], <[l1 ⊔ (l2 ⊔ l3)]> X -> <[l1 ⊔ l2 ⊔ l3]> X :=
    climbL_eq (join_assoc _ _ _).

  Lemma bindL_bindL [l1 l2 l3] :
    forall [X Y Z] (m : <[l1]> X) (k1 : X -> <[l2]> Y) (k2 : Y -> <[l3]> Z),
      bindL (bindL m k1) k2 ≈ climbL_assoc (bindL m (fun x => bindL (k1 x) k2)).
  Proof.
    intros.
    unfold bindL, climbL_assoc, climbL_eq, climbL.
    unshelve rewrite bindPO_climbPO.
    4: apply reify_le_trans.
    - transitivity (l1 ⊔ l2); auto using join_le_l.
    - rewrite <- join_assoc; auto using join_le_r.
    - rewrite bindPO_bindPO.
      + reflexivity.
      + now apply L_PO_le.
      + intros. now apply reify_le_trans.
  Qed.

  Lemma rew_eq_refl : forall A x (P : A -> Type) (H : P x),
    rew [P] eq_refl in H = H.
  Proof. reflexivity. Qed.

  Lemma climbL_rew : forall l1 l2 (E : l1 = l2) X (x : <[l1]> X),
    rew [fun l => <[l]> X] E in x ≈ climbL_eq E x.
  Proof.
    intros.
    subst.
    rewrite rew_eq_refl.
    unfold climbL_eq.
    now rewrite reify_le_refl.
  Qed.

  Lemma bindL_bindL_rew [l1 l2 l3] :
    forall [X Y Z] (m : <[l1]> X) (k1 : X -> <[l2]> Y) (k2 : Y -> <[l3]> Z),
      bindL (bindL m k1) k2 ≈ rew [fun l => <[l]> Z] join_assoc _ _ _ in (bindL m (fun x => bindL (k1 x) k2)).
  Proof.
    intros.
    rewrite climbL_rew.
    now apply bindL_bindL.
  Qed.

End Lattice.
