From Coq Require Export
  Classes.RelationClasses
  Setoid
  Morphisms.

Notation "M ~> N" := (forall X, M X -> N X) (at level 99).
Definition pw {X Y} (R : relation X) : relation (Y -> X) :=
  fun f g => forall y, R (f y) (g y).

#[global] Instance pwEquiv {X Y} (R : relation X) `{Equivalence _ R} : Equivalence (@pw _ Y R).
Proof.
  split.
  - now intros ? ?.
  - intros ? ? HR ?; symmetry; apply HR.
  - intros ? ? HR1 ? ? HR2; etransitivity; eauto.
Qed.

Definition signature := Type -> Type.
Definition monad := Type -> Type.

Ltac break :=
  match goal with
  | [ |- context [ (let (_,_) := ?X in _) ] ] => destruct X eqn:?
  | [ |- context [ (match ?X with _ => _ end) ] ] => destruct X eqn:?
  | [ H : context [ match ?X with _ => _ end ] |- _] => destruct X eqn:?
  | [ H : context [ (let (_,_) := ?X in _) ] |- _ ] => destruct X eqn:?
  | [ H : option _ |- _ ] => destruct H
  | [ H : (_,_) = (_,_) |- _ ] => inversion_clear H
   end.

Tactic Notation "!break" := repeat break.

