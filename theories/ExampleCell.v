(*|
Minimal case study: a simple memory cell
========================================

.. coq:: none
|*)
From Coq Require Import Nat Program.
From OrderedMon Require Import
  utils
  monad
  po
  lattice
  signatures
  free.
Import EqNotations.

(*|
Concrete monads
---------------

Given a system equipped with a single memory cell storing a [Nat],
we may work with four kinds of monadic computations:

- [pure] computations do not interact with the cell at all
- [read] computations may read, but not modify the cell
- [write] computations may update the cell, but not read it
- [state] computations may both read and update the cell

We reflect these intents structurally in the four distinct associated monads.

.. coq::
|*)

Definition cell := nat.

Definition pure  : monad := fun X => X.
Definition read  : monad := fun X => cell -> X.
Definition write : monad := fun X => (X * option cell)%type.
Definition state : monad := fun X => cell -> cell * X.

(*|
They are monads as promised
|*)
Definition pure_monad : Monad pure :=
  {|
    ret _ x      := x;
    bind _ _ m k := k m;
    eqM _ m n    := m = n
  |}
.

#[global, refine] Instance pure_mon : Mon pure :=
  {|
    MonM := pure_monad;
  |}.
Proof.
  split.
  - reflexivity.
  - reflexivity.
  - reflexivity.
  - split; cbv; intuition.
    now subst.
  - repeat intro.
    cbn in *.
    now subst.
Defined.

Definition read_monad : Monad read :=
  {|
    ret _ x      := fun c => x;
    bind _ _ m k := fun c => let x := m c in k x c;
    eqM _ m n    := forall c, m c = n c
  |}
.

#[global, refine] Instance read_mon : Mon read :=
  {|
    MonM := read_monad;
  |}.
Proof.
  split.
  - reflexivity.
  - reflexivity.
  - reflexivity.
  - split; cbv; intuition.
    rewrite H, H0; reflexivity.
  - repeat intro.
    cbn in *.
    rewrite H; apply H0.
Defined.

Definition write_monad : Monad write :=
  {|
    ret _ x      := (x, None);
    bind _ _ m k := let '(x,mc) := m in
                    match k x with
                    | (x,None) => (x,mc)
                    | (x,Some c) => (x,Some c)
                    end;
    eqM _ m n    := m = n
  |}
.

#[global, refine] Instance write_mon : Mon write :=
  {|
    MonM := write_monad;
  |}.
Proof.
  split.
  - intros *; cbn; !break; reflexivity.
  - intros *; cbn; !break; reflexivity.
  - intros *; cbn.
    destruct m as [? [?|]].
    destruct (k x) as [? [?|]].
    + now !break.
    + now !break.
    + destruct (k x) as [? [?|]].
      * now !break.
      * now !break.
  - split; cbv; intuition.
    now break; subst.
  - repeat intro.
    cbn in *; subst.
    do 5 break; subst.
    all:rewrite H0,Heqw1 in Heqw0; now inversion_clear Heqw0.
Defined.

Definition state_monad : Monad state :=
  {|
    ret _ x      := fun c => (c,x);
    bind _ _ m k := fun c => let '(c',x) := m c in k x c';
    eqM _ m n    := forall c, m c = n c
  |}
.

#[global, refine] Instance state_mon : Mon state :=
  {|
    MonM := state_monad;
  |}.
Proof.
  split.
  - reflexivity.
  - intros * c.
    cbn; destruct (m c); reflexivity.
  - intros * c; cbn.
    destruct (m c); cbn.
    destruct (k x c0); cbn.
    reflexivity.
  - split; cbv; intuition.
    rewrite H, H0; reflexivity.
  - repeat intro.
    cbn in *.
    rewrite <- H; destruct (x c); cbn.
    apply H0.
Defined.

(*|
Furthermore, they naturally form a little lattice shaped into a diamond.
We explicit the corresponding morphisms.
|*)

(*|
From a monad to itself (the identity morphism)
|*)
#[local] Instance id_morph (M : monad) `{Monad M}
  : MonadMorphism M M | 100 :=
  {| fmap _ m := m |}.

#[local, refine] Instance id_monmorph (M : monad) `{Mon M} : MonMorph M M :=
  {| Morph := id_morph M |}.
Proof.
  typeclasses eauto.
  split.
  - reflexivity.
  - reflexivity.
  - repeat intro.
    now cbn.
Defined.

(*|
From the pure monad into any other
|*)
#[local] Instance pure_M_morph (M : monad) `{Monad M}
  : MonadMorphism pure M | 100 :=
  {| fmap X m := ret (m : X) |}.

#[local, refine] Instance pure_M_monmorph (M : monad) `{Mon M} : MonMorph pure M :=
  {| Morph := pure_M_morph M |}.
Proof.
  typeclasses eauto.
  split.
  - reflexivity.
  - intros. cbn.
    now rewrite bind_ret_l.
  - repeat intro.
    now rewrite H0.
Defined.

(*|
From the [read] to the [state] monad
|*)
#[local] Instance read_state_morph
  : MonadMorphism read state :=
  {| fmap _ (m : read _) := fun c => (c, m c) |}.

#[local, refine] Instance read_state_monmorph : MonMorph read state :=
  {| Morph := read_state_morph |}.
Proof.
  split.
  - reflexivity.
  - reflexivity.
  - repeat intro.
    now cbn; rewrite (H c).
Defined.

(*|
From the [write] to the [state] monad
|*)
#[local] Instance write_state_morph
  : MonadMorphism write state :=
  {| fmap _ m := fun c => match m with
                       | (x, None)    => (c, x)
                       | (x, Some c') => (c',x)
                       end
  |}.

#[local, refine] Instance write_state_monmorph : MonMorph write state :=
  {| Morph := write_state_morph |}.
Proof.
  split.
  - reflexivity.
  - cbn; intros *.
    destruct m as [? [?|]].
    all:destruct (k x) as [? [?|]]; auto.
  - repeat intro.
    now cbn; rewrite H.
Defined.

(*|
We can equip these monads with the read and write operations,
and the morphisms behave as expected with these operations.
|*)

Definition state_read : state cell := fun c => (c, c).

Definition state_write : cell -> state unit := fun c' c => (c', ()).

Definition read_read : read cell := id.

Definition write_write : cell -> write unit := fun c => ((), Some c).

Lemma morph_read : fmap read_read ≈ state_read.
Proof.
  reflexivity.
Qed.

Lemma morph_write : forall c, fmap (write_write c) ≈ state_write c.
Proof.
  reflexivity.
Qed.

(*|
Programming
-----------
Our first goal is to provide a framework to program within all these
monads, without having to explicitly state all the transformations
between all these monads.
|*)

Module WithExplicitMorphisms.

  Import MonadNotations.

  Section InitFetch.

  (* We consider a simple example manipulating these monads:
   we are provided two functions based on two different monads,
   and we want to manipulate them both. *)
  Variable init  : nat -> write unit.
  Variable fetch : read nat.

  (* A first approach is to explicitly call [fmap] when needed.
   It works, but we can't say that the programmer is not thinking
   about these transformations. *)
  Definition main (n : nat) : state bool :=
    fmap (init n)    ;;
    v1 <- fmap fetch ;;
    v2 <- fmap fetch ;;
    ret (v1 =? v2).

  End InitFetch.

End WithExplicitMorphisms.

Module HeterogeneousBind.

  (* Another approach is to use our heterogeneous bind operator. *)

  Notation "e1 ;; e2" :=
    (bindH e1 (fun _ => e2))
      (at level 61, right associativity, format "e1 ;; '//' e2").
  Notation "x <- c1 ;; c2" :=
    (bindH c1 (fun x => c2))
      (at level 61, c1 at next level, right associativity,
      format "x  <-  c1 ;; '//' c2").

  Section InitFetch.

  Variable init  : nat -> write unit.
  Variable fetch : read nat.

  (* In this case, the morphism instances work like a charm: not
   only can we manipulate operations acting from different monads,
   but Coq even infers the correct returned monad. *)
  Definition main (n : nat) :=
    init n      ;;
    v1 <- fetch ;;
    v2 <- fetch ;;
    ret (v1 =? v2).

   (* This heavily relies on the typeclass inference, and could be in
    some cases impractical: we now explore other means of combining
    these functions. *)

  End InitFetch.

End HeterogeneousBind.

(*|
Indexing
--------
We now turn to indexing our little world. The structure is of course
quite elementary, a single diamond.
|*)

Variant Idx : Type := | V | W | R | RW.

Definition relIdx : Idx -> Idx -> Prop :=
  fun l l' => match l, l' with
           | V, _ | _, RW | W, W | R, R => True
           | _, _ => False
           end.

#[local] Instance preorder_Idx : PreOrder relIdx.
Proof.
  split.
  - intros []; now cbn.
  - intros [] [] []; cbn; intuition.
Defined.

#[local] Instance partialorder_Idx : PartialOrder eq relIdx.
Proof.
  intros a b.
  split.
  - intros <-; cbn; intuition.
  - destruct a,b; cbn in *; intuition.
Defined.

Infix "⊑" := relIdx (at level 70, no associativity).

(* We can now put these ingredients together to directly work with the
  concrete monadic computations by reifying this diamond into their
  counterparts defined earlier. *)

Module Direct.

  Definition reifyM : Idx -> monad :=
    fun l => match l with
          | V => pure
          | R => read
          | W => state
          | RW => state
          end.

  Notation "'[[' l ']]'" := (reifyM l).

  #[local] Instance reify_into_monads : forall l, Mon [[l]].
  Proof.
    intros []; try typeclasses eauto.
  Defined.

  #[local] Instance reify_le : forall l1 l2, l1 ⊑ l2 -> MonMorph [[l1]] [[l2]].
  Proof.
    intros [] [] ORD; cbn in ORD; try (now inversion ORD); cbn; try typeclasses eauto.
  Defined.
  Arguments reify_le {l1 l2}.
  Arguments climbPO {_ _ _ _ _} {_ _} _ {_}.

  Lemma reify_le_trans :
    forall l1 l2 l3 (I12 : l1 ⊑ l2) (I13 : l1 ⊑ l3) (I23 : l2 ⊑ l3) X (m : [[l1]] X),
      climbPO I23 (climbPO I12 m) ≈ climbPO I13 m.
  Proof.
    intros [] [] []; cbn; intuition.
  Qed.

End Direct.

(* This leads to the second heterogeneous bind presented in the article. *)
Module WithAnOrder.

  Import Direct.

  (* We instanciate [bindPO] from po.v to the indices above. *)
  Definition bindPO := @bindPO Idx relIdx reifyM _ (@reify_le).
  Arguments bindPO l1 l2 l3 [LT1 LT2] [X Y].

  (* At this point, we provide no way to infer the different monads—[l3] in particular—,
    and we incorporate the indices in our notation. *)
  Notation "e1 @ l1 , l2 , l3 ;; e2" :=
    (bindPO l1 l2 l3 (LT1 := I) (LT2 := I) e1 (fun _ => e2))
      (at level 61, right associativity, format "e1  @ l1 , l2 , l3 ;; '//' e2").
  Notation "x <- c1 @ l1 , l2 , l3 ;; c2" :=
    (bindPO l1 l2 l3 (LT1 := I) (LT2 := I) c1 (fun x => c2))
      (at level 61, c1 at next level, right associativity,
      format "x  <-  c1  @ l1 , l2 , l3 ;; '//' c2").

  Section InitFetch.

  Variable init  : nat -> [[W]] unit.
  Variable fetch : [[R]] nat.

  Definition main (n : nat) :=
    init n      @ W, RW, RW ;;
    v1 <- fetch @ R, RW, RW ;;
    v2 <- fetch @ R, V, RW  ;;
    ret (v1 =? v2).

  End InitFetch.

  (* The lack of inferance of indices makes it quite heavy for the programmer.
   In particular, Coq has no way to infer [l3] (the returned monad).
   This can be eased by adding a join operation to our domain of indices. *)

End WithAnOrder.

(* We equip our indices with a join operation.
 As this join will be used for to help type inference,
 it is important that it is computable. *)
Definition joinIdx (d1 d2 : Idx) :=
  match d1, d2 with
  | V, V                        => V
  | V, R | R, V | R, R          => R
  | V, W | W, V | W, W          => W
  | R, W | _, RW | W, R | RW, _ => RW
  end.

Module Lattice.

  Import Direct.

  #[refine, global] Instance cell_pack : packL :=
    {|
      L := Idx;
      L_le := relIdx;
      join := joinIdx;
      reify := reifyM
    |}.
  Proof.
    - intros [] []; reflexivity.
    - intros [] []; reflexivity.
    - intros [] [] []; reflexivity.
    - apply reify_into_monads.
    - intros. now apply reify_le.
    - intros [] *; reflexivity.
    - intros [] [] [] *; inversion I12; inversion I13; inversion I23; reflexivity.
  Defined.

  Notation "e1 ;; e2" :=
    (bindL e1 (fun _ => e2))
      (at level 61, right associativity, format "e1 ;; '//' e2").
  Notation "x <- c1 ;; c2" :=
    (bindL c1 (fun x => c2))
      (at level 61, c1 at next level, right associativity,
      format "x  <-  c1 ;; '//' c2").

  (* As in this case our lattice has a bottom element,
    we specialise [ret] to the corresponding monad. *)
  Definition ret [X] := ret (M := [[V]]) (X := X).

  Section InitFetch.

  Variable init  : nat -> [[W]] unit.
  Variable fetch : [[R]] nat.

  (* We can now program using functions defined in different monads without
   having to think about the monads involved, nor stressing the monadic inference. *)
  Definition main (n : nat) :=
    init n      ;;
    v1 <- fetch ;;
    v2 <- fetch ;;
    ret (v1 =? v2).

  (* Coq is able to infer a type for [main], and it is convertible to what was expected. *)
  Definition main' n : [[RW]] bool := main n.

  End InitFetch.

End Lattice.

(* As an orthogonal direction, we now explore the free monad, reifying into signatures. *)
Module FreeCell.

  Variant Rd : signature :=
    | rd : Rd cell.

  Variant Wr : signature :=
    | wr (c : cell) : Wr unit.

  Definition reifyIdx : Idx -> signature :=
    fun d => match d with
          | V => ∅
          | R => Rd
          | W => Wr
          | RW => Rd +' Wr
          end.

  Lemma reify_subevent :
    forall (l1 l2 : Idx), relIdx l1 l2 ->
                     (reifyIdx l1) -< (reifyIdx l2).
  Proof.
    intros [] [] EQ; cbn in *; first [inversion EQ; fail | try typeclasses eauto].
  Defined.

  Lemma reify_subevent_proper:
    forall (l1 l2 : Idx) (I12 I12' : relIdx l1 l2),
      inject_eq (reify_subevent l1 l2 I12) (reify_subevent l1 l2 I12').
  Proof.
    intros [] [] ? ?; cbn in *; intuition; try now apply inject_eq_refl.
    all: try now intros ? [].
  Defined.

  Definition MreifyIdx : Idx -> monad :=
    fun d => match d with
          | V  => pure
          | R  => read
          | W  => write
          | RW => state
          end.

  Arguments inl1 {E F} [X].
  Arguments inr1 {E F} [X].
  Definition h_pure : ∅ ~> pure :=
    fun _ e => match e with end.

  Definition h_read : Rd ~> read :=
    fun _ e c => match e with | rd => c end.

  Definition h_write : Wr ~> write :=
    fun _ e => match e with | wr c => (tt, Some c) end.

  Definition h_state : Rd +' Wr ~> state :=
    fun _ e c => match e with
              | inl1 e =>
                  match e with
                  | rd => (c,c)
                  end
              | inr1 e =>
                  match e with
                  | wr c' => (c',tt)
                  end
              end.

  Definition hreifyIdx (d : Idx) : reifyIdx d ~> MreifyIdx d :=
    match d with
    | V  => h_pure
    | R  => h_read
    | W  => h_write
    | RW => h_state
    end.

  Lemma translate_void : forall X h (m : free ∅ X), translate h m ≈ m.
  Proof.
    intros.
    destruct m; cbn.
    reflexivity.
    inversion e.
  Qed.

  Lemma translate_interp_void_gen :
    forall {F M} `{Mon M} (f : ∅ ~> F) (h : F ~> M) (h' : ∅ ~> M) X (m : free ∅ X),
      interp h (translate f m) ≈ interp h' m.
  Proof.
    intros.
    destruct m; cbn.
    reflexivity.
    inversion e.
  Qed.

  Lemma translate_interp_void :
    forall {F M} `{Mon M} (f : ∅ ~> F) (h : F ~> M) X (m : free ∅ X),
      interp h (translate f m) ≈ interp (icomp f h) m.
  Proof.
    intros; apply translate_interp_void_gen.
  Qed.

  #[local]
  Instance translate_proper' {E F} (h : E ~> F) X :
    Proper (eqM ==> eqM) (translate h (X := X)).
  Proof.
    intros ? ? EQ.
    induction EQ; cbn.
    reflexivity.
    constructor; intros ?; apply H.
  Qed.

  #[local]
  Instance interp_proper' {E M} `{Mon M} (h : E ~> M) X :
    Proper (eqM ==> eqM) (interp h (X := X)).
  Proof.
    intros ? ? EQ.
    induction EQ; cbn.
    reflexivity.
    apply eqM_bind.
    reflexivity.
    auto.
  Qed.

  Lemma void_comp_is_pure X (m : free ∅ X) :
    exists x, m = ret x.
  Proof.
    destruct m as [x | ? e].
    exists x; reflexivity.
    inversion e.
  Qed.

  Lemma translate_id {E} X (m : free E X) :
    translate Subop_refl m ≈ m.
  Proof.
    induction m. reflexivity.
    constructor; intros ?; apply H.
  Qed.

  #[refine, global] Instance cell_pack : packF :=
    {|
      D := Idx;
      D_le := relIdx;
      Djoin := joinIdx;
      Dreify := reifyIdx;
      Dinj := reify_subevent;
      Mreify := MreifyIdx;
      Dhandle := hreifyIdx
    |}.
  Proof.
    1,2:intros [] []; reflexivity.
    intros [] [] []; reflexivity.
    apply reify_subevent_proper.
    - intros [] ?; cbn in *.
      all:try apply inject_eq_refl.
      intros ? [].
    - intros [] [] [] EQ1 EQ2; cbn in *; intuition.
      all: try now intros ? [].
    - intros []; typeclasses eauto.
    - intros [] [] ?; cbn in *; intuition.
    - intros [] [] ? ?; cbn in *; now intuition.
    - intros [] ?; cbn in *; now intuition.
    - intros [] [] [] ? ?; cbn in *; now intuition.
    - intros [] [] I ? ?; try now inversion I.
      all: try (destruct (void_comp_is_pure _ m) as [? ->]; reflexivity).
      all: try (simpl reify_subevent; rewrite translate_id; reflexivity).
      + induction m.
        reflexivity.
        cbn; intros ?.
        destruct e; cbn.
        specialize (H tt c0); cbn in H.
        rewrite H.
        destruct (interp h_write (k ())) eqn:EQ.
        destruct o; cbn in *; reflexivity.
      + induction m.
        reflexivity.
        cbn; intros ?.
        destruct e; cbn.
        specialize (H c c); cbn in H.
        rewrite H.
        reflexivity.
  Defined.

End FreeCell.


(*|
Hoare-style reasoning
---------------------
We may define reasoning principles in each of these monads.
One convenient way to do so is shaped as Hoare triplets.
|*)

(*|
Pure computations are a bit degenerated: the precondition would directly
be a proposition.
We ignore it here for convenience
|*)
Definition post_pure X := (X -> Prop).
Definition Hoare_pure {X} : pure X -> post_pure X -> Prop :=
  fun m Q => Q m.

(*|
Read only computations are predicated over the initial value of the cell,
but the post-condition does not refer it.
|*)
Definition pre_read := (cell -> Prop).
Definition post_read X := (X -> Prop).
Definition Hoare_read {X} : pre_read -> read X -> post_read X -> Prop :=
  fun P m Q =>
    forall c, P c ->
         let x := m c in Q x.

(*|
Write only computations are guarded by a pure predicate (we ignore it),
 and carry two post-condition: one judging the value computed and the
 cell written if it has been updated, the other judging the value when
 the cell has not been updated.
|*)
Definition post_write X := ((X -> Prop) * (cell -> Prop))%type.
Definition Hoare_write {X} :
  write X -> post_write X -> Prop :=
  fun m '(Qx,Qc) =>
    let '(x,mc) := m in
    match mc with
    | None => Qx x
    | Some c => Qx x /\ Qc c
    end.

(*|
Finally, state computations are predicated over the initial value of the
cell, and establish post-conditions of the final cell and computed value.
For convenience, we use a relational post as a mean to avoid the need for
meta ghost variables.
|*)
Definition pre_state := (cell -> Prop).
Definition post_state X := ((* cell -> *) X -> cell -> Prop).
Definition Hoare_state {X} : pre_state -> state X -> post_state X -> Prop :=
  fun P m Q =>
    forall c, P c ->
         let '(c',x) := m c in Q (* c *) x c'.

(*|
Transporting Hoare triplets upwards
-----------------------------------

Our grand goal is to establish results on state computations.
We hence describe how (hopefully easier) results established lower
in the hierarchy of monads transport to the stateful case.
|*)

(*|
Read only computations preserve the state.
This may be seen as meaning that without any constraint on the precondition,
we know that the cell is unchanged in the post.
Or assuming an arbitrary precondition, this precondition still holds at the end.
Or as a way to transport a Hoare triplet.
|*)
Lemma read_state_invariant {X} v :
  forall (m : read X),
    Hoare_state (fun c => c = v) (fmap m) (fun _ c => c = v).
Proof.
  now intros * c HP; cbn.
Qed.

Lemma read_state_invariant' {X} P v :
  forall (m : read X),
    Hoare_state (fun c => P c /\ c = v)
      (fmap m) (fun _ c => c = v /\ P c).
Proof.
  now intros * c HP; cbn.
Qed.

Lemma read_state_invariant'' {X} v :
  forall (m : read X) (P: pre_read) (Q :post_read X),
    Hoare_read P m Q ->
    Hoare_state (fun c => P c /\ c = v)
      (fmap m) (fun x c => c = v /\ Q x).
Proof.
  intros * HH c [HP <-]; cbn.
  split; auto.
Qed.

Lemma read_state_invariant_explicit {X} (m : read X):
  forall c,
    let (cf,x)   := fmap m c  in
    c = cf.
Proof.
  intros.
  reflexivity.
Qed.

(*|
Write only computations are more annoying to describe: they have the
property to not depend on the initial value of the cell.
This seems a bit tricky to describe as a Hoare triplet, we directly
express the invariant on the computation.
|*)
Lemma write_state_invariant_explicit {X} (m : write X):
  forall c c',
    let (cf,x)   := fmap m c  in
    let (cf',x') := fmap m c' in
    x = x'
    /\ ((cf = c /\ cf' = c') \/ cf = cf').
Proof.
  intros.
  cbn.
  destruct m,o; intuition.
Qed.

(*|
Pure computations mix both invariants
|*)
Lemma pure_state_invariant {X} v :
  forall (m : pure X),
    Hoare_state (fun c => c = v) (fmap m) (fun _ c => c = v).
Proof.
  now intros * c HP; cbn.
Qed.

Lemma pure_state_invariant' {X} : forall (m : pure X) (P: pre_state) (Q :post_pure X) v,
    Hoare_pure m Q ->
    Hoare_state (fun c => P c /\ c = v)
      (fmap m) (fun x c => c = v /\ P c /\ Q x).
Proof.
  intros * HH c [HP <-]; cbn.
  split; auto.
Qed.

Lemma pure_state_invariant'' {X} v :
  forall (m : pure X) (P: pre_state),
    Hoare_state (fun c => P c /\ c = v) (fmap m) (fun _ c => c = v /\ P c).
Proof.
  now intros * c HP; cbn.
Qed.

Lemma pure_state_invariant_explicit {X} (m : pure X):
  forall c c',
    let (cf,x)   := fmap m c  in
    let (cf',x') := fmap m c' in
    x = x' /\ cf = c /\ cf' = c'.
Proof.
  intros.
  cbn.
  intuition.
Qed.

(*|
And hence can program with computations at different indexes.
|*)
Section ProofEx.

  Import FreeCell.

  Definition bindF {d1 d2 : L} [X Y : Type]
    (m : Freify d1 X) (k : X -> Freify d2 Y) :
    Freify (Djoin d1 d2) Y
    := @bindL (@packLF_of_packF _) d1 d2 _ _ m k.
  Definition bindM {d1 d2 : L} [X Y : Type]
    (m : Mreify d1 X) (k : X -> Mreify d2 Y) :
    Mreify (Djoin d1 d2) Y
    := @bindL (@packLM_of_packF _) d1 d2 _ _ m k.
  Arguments bindM {d1 d2} [X Y] : rename.
  Arguments bindF {d1 d2} [X Y] : rename.

  Notation "e1 ;; e2" :=
    (bindF e1 (fun _ => e2))
      (at level 61, right associativity, format "e1 ;; '//' e2").
  Notation "x <- c1 ;; c2" :=
    (bindF c1 (fun x => c2))
      (at level 61, c1 at next level, right associativity,
      format "x  <-  c1 ;; '//' c2").

  Definition dret := @ret (free (reifyIdx V)) _.
  Arguments dret [X].

  (* Notation "'compute' l" := (free (reifyIdx l)) (at level 10). *)
  Definition compute l := free (reifyIdx l).

  Variable init  : nat -> compute W unit.
  Variable fetch : compute R nat.

  (* We explicitly provide the type of the function [main] below,
    but Coq is able to infer it.
    (It will rechnically infer compute [compute (W ⊔ (R ⊔ (R ⊔ V))) bool],
    but it is convertible to [compute RW bool].)
    This would not have been the case if we didn't provide a join operator
    and sticked to the second idea (presented in po.v). *)
  Definition main (n : nat) : compute RW bool :=
    init n      ;;
    v1 <- fetch ;;
    v2 <- fetch ;;
    dret (v1 =? v2).

  Instance hoare_state_eqM {X}:
    Proper (eq ==> eqM ==> eq ==> impl) (@Hoare_state X).
  Proof.
    intros ? ? <- ? ? EQ ? ? <- HH ? HP.
    apply HH in HP.
    rewrite <- EQ.
    now break.
  Qed.

  Definition ℑs [d : D] [X Y] : (X -> compute d Y) -> (X -> Mreify d Y)
    := fun k x => ℑ (k x).

  Lemma bind_ℑ :
    forall d1 d2 X Y (m : compute d1 X) (k : X -> compute d2 Y),
      ℑ (bindF m k) ≈ bindM (ℑ m) (ℑs k).
  Proof.
    intros.
    pose proof Dnat d1 (join d1 d2) (@Djoin_le_l _ _ _) as EQ.
    unfold ℑ, bindF,bindM, bindL, bindPO, bindH in *.
    rewrite interp_bind.
    apply eqM_bind.
    apply EQ.
    intros ?.
    pose proof Dnat d2 (join d1 d2) (@Djoin_le_r _ _ _) as EQ'.
    apply EQ'.
  Qed.

  Lemma Hoare_state_cut {X Y} (P : pre_state) (R : post_state X) (Q : post_state Y) (m : state X) (k : X -> state Y) :
    Hoare_state P m R ->
    (forall x, Hoare_state (R x) (k x) Q) ->
    Hoare_state P (bind m k) Q
  .
  Proof.
    intros Hm Hk c HP.
    cbn.
    specialize (Hm c HP).
    destruct (m c).
    apply Hk in Hm.
    now break.
  Qed.
  Definition T2 {X Y} : X -> Y -> Prop := fun _ _ => True.
  Lemma Hoare_state_T2 : forall X P (m : _ X),
      Hoare_state P m T2.
  Proof.
    intros * c HP.
    break; red; auto.
  Qed.

  Ltac ecut := eapply Hoare_state_cut.
  Tactic Notation "bcut" uconstr(T) :=
    apply Hoare_state_cut with (R := T).

  Lemma Hoare_state_cut_WR :
    forall [X Y] (m : Freify W X) (k : X -> Freify R Y)
      P (T : post_state X) (Q : post_state Y),
      Hoare_state P (fmap (ℑ m)) T ->
      (forall x, Hoare_state (T x) (@ℑ _ RW Y (@Ffmap _ R RW I Y (k x))) Q) ->
      Hoare_state P (ℑ (bindF m k)) Q.
  Proof.
    intros.
    rewrite bind_ℑ.
    ecut.
    eauto.
    intros.
    unfold ℑs.
    pose proof Dnat R RW I _ (k x) as EQ.
    rewrite <- EQ; clear EQ.
    eauto.
  Qed.

  Definition Pimpl (P P' : pre_state) :=
    forall c, P c -> P' c.
  Instance Pimpl_refl : Reflexive Pimpl.
  Proof.
    now cbv.
  Qed.

  Definition Qimpl {X} (Q Q' : post_state X) :=
    forall (x : X) (c : cell), Q x c -> Q' x c.

  Lemma Hoare_state_weaken :
    forall [X] (m : state X)
      P P' (Q Q' : post_state X),
      Pimpl P P' ->
      Qimpl Q' Q ->
      Hoare_state P' m Q' ->
      Hoare_state P m Q.
  Proof.
    intros * PP' Q'Q HH c HP; apply PP',HH in HP; break; now apply Q'Q.
  Qed.

  Lemma interp_bindfree :
    forall {E : Type -> Type} {M : monad} `{Monad M} `{MonadLaws M},
        forall (h : E ~> M) {X Y : Type} (m : free E X) (k : X -> free E Y),
          interp h (bindfree m k) ≈ bind (interp h m) (fun x : X => interp h (k x)).
  Proof.
    intros; apply interp_bind.
  Qed.

  Instance lifteqM_read {X} : Proper (eqM ==> eq ==> eq) (interp h_read (X := X)).
  Proof.
    intros ?? EQ c ? <-.
    revert c.
    induction EQ; cbn; intros; auto.
  Qed.

  Lemma read_twice (m : compute R nat) :
    forall c,
      ℑ (v1 <- m ;;
         v2 <- m ;;
         dret (v1 =? v2)) c = true.
  Proof.
    intros ?.
    rewrite (bind_ℑ R R).
    cbn.
    unfold ℑs, ℑ,bindM, bindL,bindPO,bindH.
    cbn.
    match goal with
      |- interp ?h (bindfree ?m ?k) ?c = _ =>
        pose proof interp_bindfree h m k as EQ;
        specialize (EQ c);
        cbn in EQ;
        rewrite EQ;
        clear EQ
    end.
    rewrite translate_id.
    apply PeanoNat.Nat.eqb_refl.
  Qed.

  Theorem cool : forall (n : nat),
      Hoare_state
        (fun _ => True)
        (ℑ (main n))
        (fun v _ => v = true).
  Proof.
    intros n; unfold main.
    match goal with
      |- Hoare_state _ (ℑ (@bindF ?d1 ?d2 _ _ _ _)) _ =>
        rewrite (bind_ℑ d1 d2);
        ecut; [apply Hoare_state_T2|
                intros ?
        ]
    end.
    intros c _.
    apply read_twice.
  Qed.

End ProofEx.
