(* This file aimed as presenting the formalism closer from the paper presentation. *)

From Coq Require Import
  Classes.RelationClasses
  Setoid
  Morphisms.
From ExtLib Require Import
  Structures.Monads
  Structures.MonadLaws.
From ITree Require Import
  Monad
  ITree.
Require Import JMequiv.

Import MonadNotation.
Open Scope monad_scope.

Set Implicit Arguments.

(* FIXME: Is such a definition already somewhere in the context? *)
Class MonadMorphism {M1 M2} `{Monad M1} `{Monad M2} (f : M1 ~> M2) := {
    MonadMorphism_ret : forall X (x : X), f _ (ret x) = ret x ;
    MonadMorphism_bind : forall U V (u : M1 U) (k : U -> M1 V),
      f _ (u >>= k) = x <- f _ u ;; f _ (k x) ;
  }.

Section PartialOrder.

(* We try to generalise as much as can be: in the paper, we assumed a lattice, but
 a partial order might be just as suitable a choice.
 We treat the lattice case in a later section. *)

Variable L : Type.
Hypothesis L_le : relation L.
Hypothesis L_PO_le : PreOrder L_le.
Hypothesis PO_L : PartialOrder eq L_le.
Infix "⊑" := L_le (at level 70, no associativity).

Variable reify : L -> Type -> Type.
Notation "'[[' l ']]'" := (reify l).
Hypothesis reify_compatible_LE : forall l1 l2, l1 ⊑ l2 -> [[l1]] -< [[l2]].

Variable M : L -> Type -> Type.
Hypothesis M_eq : forall l, Eq1 (M l).
Hypothesis Monad_M : forall l, Monad (M l).
Hypothesis EQ_M_eq : forall l, Eq1Equivalence (M l).
Hypothesis MonadLaws_M : forall l, MonadLawsE (M l).

Variable handler : forall l, [[l]] ~> M l.

Variable t_M : forall l1 l2, l1 ⊑ l2 -> M l1 ~> M l2.
Hypothesis t_M_Proper : forall l1 l2 (I12 : l1 ⊑ l2) X, Proper (eq1 ==> eq1) (@t_M _ _ I12 X).
Hypothesis t_M_Morphism : forall l1 l2 (I12 : l1 ⊑ l2), MonadMorphism (t_M I12).
Hypothesis t_M_Morphism_handler : forall l1 l2 (I12 : l1 ⊑ l2) T (e : [[l1]] T),
  t_M I12 (handler e) = handler (@subevent _ _ (reify_compatible_LE I12) _ e).

(* Morphisms are compatible with the order between monads. *)
Hypothesis t_M_refl : forall l1 (I : l1 ⊑ l1) X (m : M l1 X), t_M I m ≈ m.
Hypothesis t_M_trans : forall l1 l2 l3 (I12 : l1 ⊑ l2) (I13 : l1 ⊑ l3) (I23 : l2 ⊑ l3) X (m : M l1 X),
  t_M I23 (t_M I12 m) ≈ t_M I13 m.

(* In particular, the result of a morphism does not depend on the specific path/proof
  that we chosed in [L]. *)
Lemma t_M_path : forall l1 l2 (I I' : l1 ⊑ l2) X (m : M l1 X), t_M I m ≈ t_M I' m.
Proof.
  intros.
  assert (I11 : l1 ⊑ l1) by reflexivity.
  setoid_rewrite <- (t_M_refl I11 m) at 1.
  rewrite t_M_trans with (I13 := I').
  reflexivity.
Qed.

(* A lemma to avoid having to specify [l1 ⊑ l3]. *)
Lemma t_M_trans_d : forall l1 l2 l3 (I12 : l1 ⊑ l2) (I23 : l2 ⊑ l3) X (m : M l1 X),
  t_M I23 (t_M I12 m) ≈ t_M (PreOrder_Transitive _ _ _ I12 I23) m.
Proof. intros. apply t_M_trans. Qed.

(* We can now define a notion of heterogeneous monad within this framework. *)
Definition retH : forall [l] X, X -> M l X :=
  fun l X x => ret x.
Definition bindH : forall l1 l2 l3, l1 ⊑ l3 -> l2 ⊑ l3 -> forall U V, M l1 U -> (U -> M l2 V) -> M l3 V :=
  fun l1 l2 l3 I13 I23 U V c k =>
    x <- t_M I13 c ;; t_M I23 (k x).


(* The result of [bindH] is independent of the path taken by the order relation. *)

Lemma bindH_path : forall l1 l2 l3 (I13 I13' : l1 ⊑ l3) (I23 I23' : l2 ⊑ l3)
    U V (m : M l1 U) (k : U -> M l2 V),
  bindH I13 I23 m k ≈ bindH I13' I23' m k.
Proof.
  intros. unfold bindH.
  apply Proper_bind.
  { apply t_M_path. }
  intro x.
  apply t_M_path.
Qed.

Instance bindH_Proper : forall l1 l2 l3 (I13 : l1 ⊑ l3) (I23 : l2 ⊑ l3) U V,
  Proper (eq1 ==> (eq ==> eq1) ==> eq1) (@bindH _ _ _ I13 I23 U V).
Proof.
  intros. intros m m' Em k k' Ek.
  unfold bindH.
  apply Proper_bind.
  { now apply t_M_Proper. }
  intro x.
  apply t_M_Proper.
  now apply Ek.
Qed.

Lemma bindH_le_arg : forall l1 l1' l2 l2' l3
    (I13 : l1 ⊑ l3) (I23 : l2 ⊑ l3) (I13' : l1' ⊑ l3) (I23' : l2' ⊑ l3)
    (I1 : l1 ⊑ l1') (I2 : l2 ⊑ l2')
    U V (m : M l1 U) (k : U -> M l2 V),
  bindH I13 I23 m k ≈ bindH I13' I23' (t_M I1 m) (fun x => t_M I2 (k x)).
Proof.
  intros. unfold bindH.
  apply Proper_bind.
  { symmetry. now apply t_M_trans. }
  intro x.
  symmetry. now apply t_M_trans.
Qed.

Lemma bindH_le_res : forall l1 l2 l3 l3'
    (I13 : l1 ⊑ l3) (I23 : l2 ⊑ l3) (I3 : l3 ⊑ l3')
    (I13' : l1 ⊑ l3') (I23' : l2 ⊑ l3')
    U V (m : M l1 U) (k : U -> M l2 V),
  bindH I13' I23' m k ≈ t_M I3 (bindH I13 I23 m k).
Proof.
  intros. unfold bindH.
  rewrite MonadMorphism_bind.
  rewrite t_M_trans with (I13 := I13').
  setoid_rewrite t_M_trans with (I13 := I23').
  reflexivity.
Qed.

(* We can now prove the three heterogeneous monadic laws, analog to the three monadic laws. *)

Lemma bindH_retH_l : forall l1 l2 l3 (I13 : l1 ⊑ l3) (I23 : l2 ⊑ l3) U V (k : U -> M l2 V) (x : U),
  bindH I13 I23 (retH x) k ≈ t_M I23 (k x).
Proof.
  intros. unfold bindH, retH.
  rewrite MonadMorphism_ret.
  rewrite bind_ret_l.
  reflexivity.
Qed.

Lemma bindH_retH_r : forall l1 l2 l3 (I13 : l1 ⊑ l3) (I23 : l2 ⊑ l3) U (m : M l1 U),
  bindH I13 I23 m (retH (X := U)) ≈ t_M I13 m.
Proof.
  intros. unfold bindH, retH.
  assert (E : forall x : U, t_M I23 (ret x) ≈ ret x).
  { intros. rewrite MonadMorphism_ret. reflexivity. }
  setoid_rewrite E.
  rewrite bind_ret_r.
  reflexivity.
Qed.

Lemma bindH_bindH : forall l1 l2 l3 l4 l5 l6
    (I14 : l1 ⊑ l4) (I16 : l1 ⊑ l6) (I25 : l2 ⊑ l5) (I24 : l2 ⊑ l4) (I35 : l3 ⊑ l5) (I36 : l3 ⊑ l6) (I46 : l4 ⊑ l6) (I56 : l5 ⊑ l6)
    U V W (m : M l1 U) (k1 : U -> M l2 V) (k2 : V -> M l3 W),
  bindH I46 I36 (bindH I14 I24 m k1) k2 ≈ bindH I16 I56 m (fun x => bindH I25 I35 (k1 x) k2).
Proof.
  intros. unfold bindH.
  rewrite MonadMorphism_bind.
  rewrite bind_bind.
  assert (E : forall x : U, t_M I56 (y <- t_M I25 (k1 x);; t_M I35 (k2 y)) ≈ y <- t_M I56 (t_M I25 (k1 x));; t_M I56 (t_M I35 (k2 y))).
  { intros. rewrite MonadMorphism_bind. reflexivity. }
  setoid_rewrite E.
  setoid_rewrite t_M_trans_d.
  setoid_rewrite t_M_path at 1 2 6.
  reflexivity.
Qed.

Section Lattice.

(* In the previous section, we were working on a partial order, and we had to provide explicitely
  the [l1 ⊑ l3] and [l2 ⊑ l3] parameters to [bindH], as well as a [l] parameter to [retH].
  If we instead assume to work in a lattice, then all these parameters can be inferred:
  [binH] can return the join of both [l1] and [l2] (which is greater to both by construction),
  and [retH] can return the bottom element of the lattice. *)

(* Note that don’t actually need precisely a lattice: we just need [l1 ⊔ l2] to be greater than
 both [l1] and [l2], as well as the associativity of [⊔]. *)

Variable join : L -> L -> L.
Infix "⊔" := join (at level 36, left associativity).
Hypothesis join_le_l : forall l1 l2, l1 ⊑ l1 ⊔ l2.
Hypothesis join_le_r : forall l1 l2, l2 ⊑ l1 ⊔ l2.
Hypothesis join_assoc : forall l1 l2 l3, l1 ⊔ (l2 ⊔ l3) = l1 ⊔ l2 ⊔ l3.

Variable bottom : L.
Notation "⊥" := bottom.
Hypothesis bottom_spec : forall l, ⊥ ⊑ l.
Hypothesis bottom_join_l : forall l, ⊥ ⊔ l = l.
Hypothesis bottom_join_r : forall l, l ⊔ ⊥ = l.

(* We probably don’t need this hypothesis, but it is a good intuition to have in hand:
  [⊔] has a role very close to [+'], but [+'] duplicates common parts (i.e. [E +' E]
  is larger than just [E]) whilst [⊔] doesn’t. *)
Hypothesis join_reify : forall l1 l2, [[l1 ⊔ l2]] -< [[l1]] +' [[l2]].

(* With these assumptions, the [retH] and [bindH] become much easier to manipulate:
 - we no longer have to provide proofs of the [l1 ⊑ l3] and [l2 ⊑ l3] parts.
 - the returned level [l] is automatically inferred. *)
Definition retL : forall X, X -> M ⊥ X :=
  retH (l := ⊥).
Definition bindL : forall l1 l2 U V, M l1 U -> (U -> M l2 V) -> M (l1 ⊔ l2) V :=
  fun l1 l2 => bindH (join_le_l _ _) (join_le_r _ _).

(* The three heterogeneous monadic laws are conserved by definition.
 Some care has to be taken as the types don’t syntactically match: we base ourselves
 on a variation of the John Major’s equality. *)

Definition M_eq_JM : forall [l1 X1], M l1 X1 -> forall [l2 X2], M l2 X2 -> Prop :=
  @JMequiv2 L Type M M_eq.
Infix "≋" := M_eq_JM (at level 80).

(* TODO: clean *)

(* Some lemmas to deal with the equalities introduced by this tactic. *)

Lemma L_eq_le : forall l1 l2, l1 = l2 -> l2 ⊑ l1.
Proof. intros. subst. reflexivity. Qed.

Lemma eq_rect_remove : forall l1 l2 (E : l2 = l1) X x,
  eq_rect _ (fun l => M l X) x _ E ≈ t_M (L_eq_le (eq_sym E)) x.
Proof. intros. subst. rewrite t_M_refl. reflexivity. Qed.

Lemma eq_rect_r_remove : forall l1 l2 (E : l1 = l2) X x,
  eq_rect_r (fun l => M l X) x E ≈ t_M (L_eq_le E) x.
Proof.
  intros. subst. rewrite eq_rect_r_eq_refl.
  rewrite t_M_refl. reflexivity.
Qed.

Lemma eq_pair_fst : forall A B (a1 a2 : A) (b1 b2 : B),
  (a1, b1) = (a2, b2) ->
  a1 = a2.
Proof. inversion 1. reflexivity. Defined.

Lemma eq_pair_snd : forall A B (a1 a2 : A) (b1 b2 : B),
  (a1, b1) = (a2, b2) ->
  b1 = b2.
Proof. inversion 1. reflexivity. Qed.

Lemma eq_rect_r_remove2 : forall l1 l2 X (E : (l1, X) = (l2, X)) x,
  eq_rect_r (fun '(l, X) => M l X) x E ≈ t_M (L_eq_le (eq_pair_fst E)) x.
Proof.
  intros.
  set (E1 := eq_pair_fst E). set (E2 := eq_pair_snd E).
  clearbody E1 E2. subst. simpl in x.
  rewrite t_M_refl.
  unfold eq_rect_r.
  rewrite <- Eqdep.EqdepTheory.eq_rect_eq.
  (* There might be a way to prove this lemma without using this axiom.
    One could get some inspiration from the term built by the [reflexivity] tactic
    in [eq_rect_remove] (it is doing much more than just [apply eq_refl]). *)
  reflexivity.
Qed.

(* Here follow some versions of the heterogeneous monadic laws.
  These may be useful in some contexts, but in general the later ones are to be prefered. *)

Lemma bindL_retL_l_t_M : forall l U V (k : U -> M l V) (x : U),
  bindL (retL x) k ≈ t_M (L_eq_le (bottom_join_l l)) (k x).
Proof.
  intros. unfold bindL, retL.
  rewrite bindH_retH_l.
  apply t_M_path.
Qed.

Lemma bindL_retL_r_t_M : forall l U (m : M l U),
  bindL m (retL (X := U)) ≈ t_M (L_eq_le (bottom_join_r l)) m.
Proof.
  intros. unfold bindL, retL.
  rewrite bindH_retH_r.
  apply t_M_path.
Qed.

Lemma bindL_bindL_t_M : forall l1 l2 l3
    U V W (m : M l1 U) (k1 : U -> M l2 V) (k2 : V -> M l3 W),
  bindL (bindL m k1) k2
  ≈ t_M (L_eq_le (eq_sym (join_assoc l1 l2 l3))) (bindL m (fun x => bindL (k1 x) k2)).
Proof.
  intros. unfold bindL.
  assert (Ia : l1 ⊑ l1 ⊔ l2 ⊔ l3).
  { transitivity (l1 ⊔ l2); apply join_le_l. }
  assert (Ib : l2 ⊔ l3 ⊑ l1 ⊔ l2 ⊔ l3).
  { rewrite <- join_assoc. apply join_le_r. }
  rewrite bindH_bindH with (l5 := l2 ⊔ l3) (I16 := Ia) (I56 := Ib)
                           (I25 := join_le_l l2 l3) (I35 := join_le_r l2 l3).
  rewrite <- bindH_le_res.
  reflexivity.
Qed.

Lemma bindL_retL_l_eq_rect_r : forall l U V (k : U -> M l V) (x : U),
  bindL (retL x) k ≈ ltac:(rewrite bottom_join_l; exact (k x)).
Proof.
  intros.
  rewrite eq_rect_r_remove.
  now apply bindL_retL_l_t_M.
Qed.

Lemma bindL_retL_r_eq_rect_r : forall l U (m : M l U),
  bindL m (retL (X := U)) ≈ ltac:(rewrite bottom_join_r; exact m).
Proof.
  intros.
  rewrite eq_rect_r_remove.
  now apply bindL_retL_r_t_M.
Qed.

Lemma bindL_bindL_eq_rect : forall l1 l2 l3
    U V W (m : M l1 U) (k1 : U -> M l2 V) (k2 : V -> M l3 W),
  bindL (bindL m k1) k2
  ≈ ltac:(rewrite <- join_assoc; exact (bindL m (fun x => bindL (k1 x) k2))).
Proof.
  intros.
  rewrite eq_rect_remove.
  now apply bindL_bindL_t_M.
Qed.

(* We can now state and prove the three heterogeneous monadic laws. *)

Lemma bindL_retL_l : forall l U V (k : U -> M l V) (x : U),
  bindL (retL x) k ≋ k x.
Proof.
  intros.
  set (E := ltac:(now rewrite bottom_join_l) : (⊥ ⊔ l, V) = (l, V)).
  apply eq_rect_r_JMequiv with (E := E).
  rewrite bindL_retL_l_t_M.
  rewrite eq_rect_r_remove2.
  apply t_M_path.
Qed.

Lemma bindL_retL_r : forall l U (m : M l U),
  bindL m (retL (X := U)) ≋ m.
Proof.
  intros.
  set (E := ltac:(now rewrite bottom_join_r) : (l ⊔ ⊥, U) = (l, U)).
  apply eq_rect_r_JMequiv with (E := E).
  rewrite bindL_retL_r_t_M.
  rewrite eq_rect_r_remove2.
  apply t_M_path.
Qed.

Lemma bindL_bindL : forall l1 l2 l3
    U V W (m : M l1 U) (k1 : U -> M l2 V) (k2 : V -> M l3 W),
  bindL (bindL m k1) k2 ≋ bindL m (fun x => bindL (k1 x) k2).
Proof.
  intros.
  set (E := ltac:(now rewrite join_assoc) : (l1 ⊔ l2 ⊔ l3, W) = (l1 ⊔ (l2 ⊔ l3), W)).
  apply eq_rect_r_JMequiv with (E := E).
  rewrite bindL_bindL_t_M.
  rewrite eq_rect_r_remove2.
  apply t_M_path.
Qed.

End Lattice.

End PartialOrder.
