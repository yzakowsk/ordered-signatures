(** This file defines the monad-ish structure. **)


Set Implicit Arguments.

From TLC Require Import LibTactics.
Require Export LibExec.

From Coq Require Export List.
Import ListNotations.

From ExtLib Require Structures.Monad.
From ITree Require Export ITree.
From ITree Require Import TranslateFacts.

Import ITree.Eq.Eqit.

Export Structures.Monad.
Export Monads.
Export MonadNotation.

Open Scope type_scope.


(** * Lemmas **)

(** A lemma to easily convert the events of [itree]s. **)
#[export] Instance Embeddable_itree_event : forall E F R,
  E -< F ->
  Embeddable (itree E R) (itree F R).
Proof. intros E F R S T. refine (translate S T). Defined.

(** A lemma to unfold [embed]. **)
Lemma embed_unfold : forall A B (H : Embeddable A B),
  embed = H.
Proof. reflexivity. Qed.


(** How we assume events are organised. **)
(* FIXME: Remove what can be removed. *)

Module Type EventLattice.

(** The main type of event descriptor. **)
Parameter t : Type.
Parameter event : t -> Type -> Type.

(** It is ordered. **)
Parameter eq : t -> t -> Prop.
Parameter eq_refl : forall d : t, eq d d.
Parameter eq_sym : forall x y : t, eq x y -> eq y x.
Parameter eq_trans : forall x y z : t, eq x y -> eq y z -> eq x z.
Parameter le : t -> t -> Prop.
Parameter lt : t -> t -> Prop.
Parameter le_trans : forall d1 d2 d3 : t, le d1 d2 -> le d2 d3 -> le d1 d3.
Parameter le_antisym : forall d1 d2 : t, le d1 d2 -> le d2 d1 -> eq d1 d2.
Parameter lt_trans : forall d1 d2 d3 : t, lt d1 d2 -> lt d2 d3 -> lt d1 d3.
Parameter lt_not_eq : forall d1 d2 : t, lt d1 d2 -> ~ eq d1 d2.

(** The order is compatible with the event embedding. **)
Parameter event_le : forall d1 d2 : t, le d1 d2 -> event d1 -< event d2.

(** Some compatibilities. **)
Parameter event_eq : forall d1 d2 : t, eq d1 d2 -> event d1 = event d2.
Parameter event_le_trans : forall (d1 d2 d3 : t) (O12 : le d1 d2) (O23 : le d2 d3) T (e : event d1 T),
  event_le O23 (event_le O12 e) = event_le (le_trans O12 O23) e.

(** It is structured as a lattice. **)
Parameter join : t -> t -> t.
Parameter join_assoc : LibOperation.assoc join.
Parameter join_comm : LibOperation.comm join.
Parameter join_same : LibOperation.idempotent2 join.
Parameter le_join : forall d1 d2 : t, le d1 d2 -> eq (join d1 d2) d2.
Parameter join_le_l : forall d1 d2 : t, le d1 (join d1 d2).
Parameter join_le_r : forall d1 d2 : t, le d2 (join d1 d2).

Parameter meet : t -> t -> t.
Parameter meet_assoc : LibOperation.assoc meet.
Parameter meet_comm : LibOperation.comm meet.
Parameter meet_same : LibOperation.idempotent2 meet.
Parameter le_meet : forall d1 d2 : t, le d1 d2 -> eq (meet d1 d2) d1.
Parameter meet_le_l : forall d1 d2 : t, le (meet d1 d2) d1.
Parameter meet_le_r : forall d1 d2 : t, le (meet d1 d2) d2.

Parameter bottom : t.
Parameter bottom_is_bottom : forall d : t, le bottom d.
Parameter join_bottom_r : LibOperation.neutral_r join bottom.
Parameter join_bottom_l : LibOperation.neutral_l join bottom.
Parameter meet_bottom_r : LibOperation.absorb_r meet bottom.
Parameter meet_bottom_l : LibOperation.absorb_l meet bottom.

Parameter top : t.
Parameter top_is_top : forall d : t, le d top.
Parameter join_top_r : LibOperation.absorb_r join top.
Parameter join_top_l : LibOperation.absorb_l join top.
Parameter meet_top_r : LibOperation.neutral_r meet top.
Parameter meet_top_l : LibOperation.neutral_l meet top.

End EventLattice.

Module MonadFromEventLattice (E : EventLattice).

Include E.
Local Instance event_le' : forall d1 d2 : t, le d1 d2 -> event d1 -< event d2 := event_le.

(** Monadic structure **)

Definition ret R (r : R) : itree (event bottom) R :=
  Ret r.

Definition bind [d1 d2 T U] (x : itree (event d1) T) (k : T -> itree (event d2) U) :
  itree (event (join d1 d2)) U :=
  let L1 := join_le_l d1 d2 in
  let L2 := join_le_r d1 d2 in
  ITree.bind
    (@embed (itree (event d1) T) (itree (event (join d1 d2)) T) ltac:(typeclass) x)
    (fun t => @embed (itree (event d2) U) (itree (event (join d1 d2)) U) ltac:(typeclass) (k t)).

Definition if_then_else [d1 d2 d3 R] (b : itree (event d1) bool)
  (t : itree (event d2) R) (e : itree (event d3) R) : itree (event (join d1 (join d2 d3))) R :=
  bind b (fun b =>
    let L2 := join_le_l d2 d3 in
    let L3 := join_le_r d2 d3 in
    if b
    then (@embed (itree (event d2) R) (itree (event (join d2 d3)) R) ltac:(typeclass) t)
    else (@embed (itree (event d3) R) (itree (event (join d2 d3)) R) ltac:(typeclass) e)).

Definition lift [A B d] (f : A -> B) (a : itree (event d) A) : itree (event d) B.
Proof.
  lets r: (bind a (fun a => ret (f a))).
  rewrite join_bottom_r in r. exact r.
Defined.

Definition lift2 [A B C d1 d2] (f : A -> B -> C) (a : itree (event d1) A) (b : itree (event d2) B)
  : itree (event (join d1 d2)) C :=
  bind a (fun a => lift (f a) b).

Definition or [d1 d2] (a : itree (event d1) bool) (b : itree (event d2) bool) :=
  lift2 or a b.
Definition and [d1 d2] (a : itree (event d1) bool) (b : itree (event d2) bool) :=
  lift2 and a b.

(* Helper lemma for [easy_convert]. *)
Lemma assoc_inv : forall A (f : A -> A -> A),
  LibOperation.assoc f ->
  forall x y z, f (f x y) z = f x (f y z).
Proof. introv assoc. introv. rewrite~ assoc. Qed.

(** Given [t : itree (event _) _], tries to simplify the goal using [E]’s equations
  to seemlessly [t]. **)
Ltac easy_convert t :=
  let rec foreach l tac :=
    lazymatch l with
    | nil => idtac
    | boxer ?v :: ?l => tac v; foreach l tac
    end in
  let lemmas := constr:(>> (assoc_inv join_assoc) join_same (assoc_inv meet_assoc) meet_same
                           join_bottom_r join_bottom_l meet_bottom_r meet_bottom_l
                           join_top_r join_top_l meet_top_r meet_top_l) in
  repeat progress foreach lemmas ltac:(fun lemma => try rewrite lemma) ;
  exact t.

Lemma bind_of_return : forall A B d (a : A) (f : A -> itree (event d) B),
  bind (ret a) f ≅ ltac:(easy_convert (f a)).
Proof.
  introv. unfolds bind, ret, @embed, Embeddable_itree_event.
  rewrite translate_ret, bind_ret_l.
  (* FIXME: I need to prove that
     using the fact that [bottom] is less than [d] to get [event bottom -< event d]
     using [event_le], then using [translate] on [itree]
     is the same than
     rewriting the type of the [itree] using [join_bottom_l]. *)
  (*
  translate (event_le' (join_le_r bottom d)) (f a)
  ≅ Logic.eq_rect_r (fun t0 : t => itree (event t0) B) (f a) (join_bottom_l d)
   *)
Admitted. (* TODO *)

Lemma return_of_bind : forall A d (aM : itree (event d) A),
  bind aM (@ret _) ≅ ltac:(easy_convert aM).
Proof.
  introv. unfolds bind, ret, @embed, Embeddable_itree_event.
  lazymatch goal with |- ITree.bind _ ?f ≅ _ => asserts E: (forall a : A, f a ≅ Ret a) end.
  { intro a. rewrite translate_ret. reflexivity. }
  etransitivity.
  { eapply eq_itree_clo_bind with (U1 := A) (U2 := A) (UU := @Logic.eq A);
      [ reflexivity | inversion 1; apply E ]. }
  rewrite bind_ret_r.
  (* TODO *)
  (*
  translate (event_le' (join_le_l d bottom)) aM
  ≅ Logic.eq_rect_r (fun t0 : t => itree (event t0) A) aM (join_bottom_r d)
  *)
Admitted. (* TODO *)

Lemma bind_associativity : forall A B C d1 d2 d3
    (aM : itree (event d1) A) (f : A -> itree (event d2) B) (g : B -> itree (event d3) C),
  bind (bind aM f) g ≅ ltac:(easy_convert (bind aM (fun a => bind (f a) g))).
Proof.
  introv. unfolds bind, ret, @embed, Embeddable_itree_event.
  rewrite translate_bind, bind_bind, <- translate_cmpE.
  (* TODO: An interesting property to prove about [event_le] here:
     it is compatible with the transitivity of [le]. *)
Admitted. (* TODO *)

End MonadFromEventLattice.
