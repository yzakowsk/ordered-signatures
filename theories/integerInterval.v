(* A small definition file for manipulating integer intervals. *)

From Coq Require Import Lia.

Definition interval a b := { x : nat | a <= x <= b }.

Notation "[[ a ; b ]]" := (interval a b).

Definition interval_to_nat [a b : nat] : [[a; b]] -> nat := fun I => proj1_sig I.

(* When pattern matching over [[[a; b]]], it suffices to provide a value for each
 value between [a] and [b]. *)

Fixpoint repeated_functions n X Y :=
  match n with
  | O => Y
  | S n' => X -> repeated_functions n' X Y
  end.

Fixpoint ignore_arguments n X Y (y : Y) : repeated_functions n X Y :=
  match n with
  | O => y
  | S n' => fun _ => ignore_arguments n' X Y y
  end.

Definition get_ith n i X : i < n -> repeated_functions n X X.
Proof.
  generalize n. clear n. induction i; intros n I.
  - destruct n; [ lia |]. intro x. exact (ignore_arguments _ _ _ x).
  - destruct n; [ lia |]. intros _. apply IHi. lia.
Defined.

Definition match_interval [a b] : forall [X], [[a; b]] -> repeated_functions (1 + b - a) X X :=
  fun X '(exist _ i Pi) => get_ith (1 + b - a) (i - a) X ltac:(lia).

(* Example: define a function : [[[1; 3]] -> nat]. *)
Definition example (i : [[1; 3]]) : nat :=
  match_interval i
    (* 1 => *) 11
    (* 2 => *) 12
    (* 3 => *) 13.

Lemma example_2 : example (exist _ 2 (ltac:(lia) : 1 <= 2 <= 3)) = 12.
Proof. reflexivity. Qed.

(*Require Extraction.
Recursive Extraction example.*)

