From OrderedMon Require Import
  utils
  monad.

(* Reserved Infix "⊑" (at level 70, no associativity). *)
(* Reserved Notation "'[[' l ']]'". *)

(* Class packO := *)
(*   { *)
(*     (* The partial order domain by which we index the monads of interest *) *)
(*     L : Type; *)
(*     L_le : relation L where "x ⊑ y" := (L_le x y); *)
(*     L_PO_le :> PreOrder L_le; *)
(*     PO_L :> PartialOrder eq L_le; *)

(*     (* We map indexes to monads *) *)
(*     reify : L -> monad where "'[[' l ']]'" := (reify l); *)
(*     reify_into_monads :> forall l : L, Mon (reify l); *)

(*     (* The ordering on L must entail that we know how to transport computations accordingly *) *)
(*     reify_le : forall {l1 l2 : L}, L_le l1 l2 -> MonMorph (reify l1) (reify l2); *)

(*     climbPO [l1 l2] (I12 : l1 ⊑ l2) : *)
(*       forall [X], [[l1]] X -> [[l2]] X := *)
(*         @fmap _ _ (Morph (MonMorph := reify_le I12)); *)


(*     (* The way we transport computations should not depend on the proof of inequality we use *) *)
(*     reify_le_refl : *)
(*     forall l (I : l ⊑ l) X (m : [[l]] X), *)
(*       climbPO I m ≈ m; *)

(*     reify_le_trans : *)
(*     forall l1 l2 l3 (I12 : l1 ⊑ l2) (I13 : l1 ⊑ l3) (I23 : l2 ⊑ l3) X (m : [[l1]] X), *)
(*       climbPO I23 (climbPO I12 m) *)
(*         ≈ climbPO I13 m; *)

(*   }. *)

(* Infix "⊑" := L_le. *)
(* Notation "'[[' l ']]'" := (reify l). *)

Section PartialOrder.

  (* Context {args : packO}. *)

  Variable L : Type.
  Hypothesis L_le : Relation_Definitions.relation L.
  Hypothesis L_PO_le : PreOrder L_le.
  Hypothesis PO_L : PartialOrder eq L_le.
  Infix "⊑" := L_le (at level 70, no associativity).

  Variable reify : L -> monad.
  Notation "'[[' l ']]'" := (reify l).
  Context {reify_into_monads : forall l, Mon [[l]]}.
  Context {reify_le : forall l1 l2, l1 ⊑ l2 -> MonMorph [[l1]] [[l2]]}.
  Arguments reify_le {l1 l2}.

  (* Specializing fmap through the PO *)
  Definition climbPO [l1 l2] (I12 : l1 ⊑ l2) :
    forall [X], [[l1]] X -> [[l2]] X :=
    @fmap _ _ (Morph (MonMorph := reify_le I12)).

  Hypothesis reify_le_trans :
    forall l1 l2 l3 (I12 : l1 ⊑ l2) (I13 : l1 ⊑ l3) (I23 : l2 ⊑ l3) X (m : [[l1]] X),
      climbPO I23 (climbPO I12 m) ≈ climbPO I13 m.

  Definition bindPO
    {l1 l2 l3} (LT1 : l1 ⊑ l3) (LT2 : l2 ⊑ l3) :
    forall [X Y], [[l1]] X -> (X -> [[l2]] Y) -> [[l3]] Y :=
    @bindH _ _ _ _ _ _ (reify_le LT1) (reify_le LT2).

  Lemma bindPO_ret_l
    [l1 l2 l3] (I13 : l1 ⊑ l3) (I23 : l2 ⊑ l3) :
    forall X Y (k : X -> [[l2]] Y) (x : X),
      bindPO I13 I23 (ret x) k ≈ climbPO I23 (k x).
  Proof.
    intros. apply bindH_ret_l.
  Qed.

  Lemma bindPO_ret_r
    [l1 l2 l3] (I13 : l1 ⊑ l3) (I23 : l2 ⊑ l3) :
    forall X (m : [[l1]] X),
      bindPO I13 I23 m (ret (X := X)) ≈ climbPO I13 m.
  Proof.
    intros. apply bindH_ret_r.
  Qed.

  Lemma bindPO_bindPO
    [l1 l2 l3 l4 l5 l6]
    (I14 : l1 ⊑ l4) (I16 : l1 ⊑ l6)
    (I25 : l2 ⊑ l5) (I24 : l2 ⊑ l4)
    (I35 : l3 ⊑ l5) (I36 : l3 ⊑ l6)
    (I46 : l4 ⊑ l6) (I56 : l5 ⊑ l6)
    :
    forall [X Y Z] (m : [[l1]] X) (k1 : X -> [[l2]] Y) (k2 : Y -> [[l3]] Z),
      bindPO I46 I36 (bindPO I14 I24 m k1) k2
      ≈ bindPO I16 I56 m (fun x => bindPO I25 I35 (k1 x) k2).
  Proof.
    intros. unshelve eapply bindH_bindH.
    - intros. now apply reify_le_trans.
    - intros. now apply reify_le_trans.
    - intros. transitivity (climbPO (PreOrder_Transitive l2 l4 l6 I24 I46) m0).
      + now apply reify_le_trans.
      + symmetry. now apply reify_le_trans.
  Qed.

  (* The above lemma is general, but forces the user to specify unecessary proofs
    of inequality.  The following two lemmas are rephrased to avoid such hypotheses
    that can be deduced from the context.  But these hypotheses vary in the left and
    right rewritting, so two lemmas are needed. *)

  Lemma bindPO_bindPO_right
    [l1 l2 l3 l4 l5 l6]
    (I14 : l1 ⊑ l4)
    (I25 : l2 ⊑ l5) (I24 : l2 ⊑ l4)
    (I35 : l3 ⊑ l5) (I36 : l3 ⊑ l6)
    (I46 : l4 ⊑ l6) (I56 : l5 ⊑ l6)
    :
    forall [X Y Z] (m : [[l1]] X) (k1 : X -> [[l2]] Y) (k2 : Y -> [[l3]] Z),
      bindPO I46 I36 (bindPO I14 I24 m k1) k2
      ≈ bindPO (PreOrder_Transitive _ _ _ I14 I46) I56 m (fun x => bindPO I25 I35 (k1 x) k2).
  Proof.
    intros. now apply bindPO_bindPO.
  Qed.

  Lemma bindPO_bindPO_left
    [l1 l2 l3 l4 l5 l6]
    (I14 : l1 ⊑ l4) (I16 : l1 ⊑ l6)
    (I25 : l2 ⊑ l5) (I24 : l2 ⊑ l4)
    (I35 : l3 ⊑ l5)
    (I46 : l4 ⊑ l6) (I56 : l5 ⊑ l6)
    :
    forall [X Y Z] (m : [[l1]] X) (k1 : X -> [[l2]] Y) (k2 : Y -> [[l3]] Z),
      bindPO I16 I56 m (fun x => bindPO I25 I35 (k1 x) k2)
      ≈ bindPO I46 (PreOrder_Transitive _ _ _ I35 I56) (bindPO I14 I24 m k1) k2.
  Proof.
    intros. symmetry. now apply bindPO_bindPO.
  Qed.

  (* This will be useful in lattice.v: bindPO is compatible with climbPO. *)
  Lemma bindPO_climbPO [l1 l2 l3 l3']
    (I13 : l1 ⊑ l3) (I23 : l2 ⊑ l3) (I3 : l3 ⊑ l3')
    (I13' : l1 ⊑ l3') (I23' : l2 ⊑ l3') :
    forall [X Y] (m : [[l1]] X) (k : X -> [[l2]] Y),
      climbPO I3 (bindPO I13 I23 m k) ≈ bindPO I13' I23' m k.
  Proof.
    intros.
    unfold bindPO, bindH, climbPO.
    rewrite fmap_bind.
    apply eqM_bind.
    - pose proof (reify_le_trans l1 l3 l3' I13 I13' I3) as E1.
      unfold climbPO in E1.
      now rewrite E1.
    - intro.
      pose proof (reify_le_trans l2 l3 l3' I23 I23' I3) as E2.
      unfold climbPO in E2.
      now rewrite E2.
  Qed.

End PartialOrder.

