(** This file provides an example of instantiation. **)

Set Implicit Arguments.

From TLC Require Import LibTactics LibList.
Require Export Result.

From Coq Require Export List.
Import ListNotations.


(** * Event types **)

(** We assume given a set of basic events,
  each associated to some actions that can be triggered. **)

Universe r. (** We force each event kind to return in the same universe. **)

(* We assume given the following actions. *)
Axiom
    RGlobal
    WGlobal
    EHeap
    EIO
    Funtab
    Error
    Impossible
    LongJump
    NotImplemented
    Debug
  : Type -> Type@{r}.

(** * Event Descriptors **)

Module EventDescriptor.

(** Given the events defined in the previous section, one could imagine
  joining all these events in a single type [RGlobal +' WGlobal +' …].
  This would however provide very few information about what each function
  actually do.  Instead, we would like to have some kind of minimal set of
  events associated to each function.  For instance, most function never
  update the value of any global variable: the event [WGlobal] should
  preferably not be present in the type of such functions.  Not having such
  an event in the signature of a function ensures by type that this
  function will never modify a global variable, thus helping proving
  properties for this function.

  Inferring this minimal set is not trivial in general without having to add
  a lot of annotations or using some kind of meta-programming (typically, Ltac).
  For instance, Coq doesn’t provide a mechanism to infer that an expression of
  the form [if _ then a else b] should have some kind of union type between the
  type of [a] and the type of [b] (and for good reasons: such common larger
  type may not exist or may not be unique).  We are however here in the presence
  of a lattice of types, and this can be wroked with by explicitely defining
  this lattice. **)

(** Event descriptor state for each kind of events whether it can be triggerred
  by the associated function. **)
Record event_descriptor := make_event_descriptor {
    dRGlobal : bool ;
    dWGlobal : bool ;
    dEHeap : bool ;
    dEIO : bool ;
    dFuntab : bool ;
    dError : bool ;
    dImpossible : bool ;
    dLongJump : bool ;
    dNotImplemented : bool ;
    dDebug : bool ;
  }.

(** The correspondences between the fields of [event_descriptor] and events,
  mainly for automation purposes. **)
Definition event_descriptor_correspondence := [
    (dRGlobal, RGlobal) ;
    (dWGlobal, WGlobal) ;
    (dEHeap, EHeap) ;
    (dEIO, EIO) ;
    (dFuntab, Funtab) ;
    (dError, Error) ;
    (dImpossible, Impossible) ;
    (dLongJump, LongJump) ;
    (dNotImplemented, NotImplemented) ;
    (dDebug, Debug)
  ].

(** Adding the definitions expected by [OrderedType]. **)
Definition t := event_descriptor.

Definition eq (d1 d2 : t) :=
  Forall (fun '(dE, _) => dE d1 = dE d2) event_descriptor_correspondence.

Global Instance Decidable_eq : forall d1 d2, Decidable (eq d1 d2).
Proof.
  intros d1 d2. unfolds eq.
  (* This is frustrating: I can’t apply [Forall_Decidable] because of universe constraints here. *)
  induction event_descriptor_correspondence as [|[dE ?] l].
  - applys decidable_make true. rew_bool_eq. apply~ Forall_nil.
  - applys Decidable_equiv (dE d1 = dE d2 /\ Forall (fun '(dE, _) => dE d1 = dE d2) l).
    + iff F; inverts~ F.
    + typeclass.
Defined.

Lemma eq_eq : forall d1 d2, eq d1 d2 <-> d1 = d2.
Proof.
  intros d1 d2. destruct d1, d2. unfolds eq, event_descriptor_correspondence. iff F.
  - repeat (let E := fresh "E" in inverts F as E F; simpl in E).
    fequals~.
  - inverts F. repeat constructors~.
Qed.

Global Instance Comparable_t : Comparable t.
Proof.
  constructors. intros d1 d2. applys Decidable_equiv.
  - apply eq_eq.
  - typeclass.
Defined.

Lemma eq_refl : forall d, eq d d.
Proof. intro d. apply~ eq_eq. Qed.

Lemma eq_sym : forall x y : t, eq x y -> eq y x.
Proof. intros d1 d2 E. apply eq_eq in E. apply~ eq_eq. Qed.

Lemma eq_trans : forall x y z : t, eq x y -> eq y z -> eq x z.
Proof. intros d1 d2 d3 E1 E2. apply eq_eq in E1, E2. apply eq_eq. substs~. Qed.

Definition le (d1 d2 : t) :=
  Forall (fun '(dE, _) => decide ((dE d1 : bool) -> dE d2)) event_descriptor_correspondence.

Global Instance Decidable_le : forall d1 d2, Decidable (le d1 d2).
Proof.
  intros d1 d2. unfolds le.
  (* This is frustrating: I can’t apply [Forall_Decidable] because of universe constraints here. *)
  induction event_descriptor_correspondence as [|[dE ?] l].
  - applys decidable_make true. rew_bool_eq. apply~ Forall_nil.
  - applys Decidable_equiv ((dE d1 -> dE d2) /\ Forall (fun '(dE, _) => decide ((dE d1 : bool) -> dE d2)) l).
    + iff F; inverts F as I F.
      * constructors~. rewrite decide_spec. rew_bool_eq~.
      * rewrite decide_spec in I. rew_bool_eq~ in I.
    + typeclass.
Defined.

Definition lt (d1 d2 : t) :=
  le d1 d2 /\ d1 <> d2.

Global Instance Decidable_lt : forall d1 d2, Decidable (lt d1 d2).
Proof. typeclass. Defined.

Lemma le_trans : forall d1 d2 d3 : t, le d1 d2 -> le d2 d3 -> le d1 d3.
Proof.
  intros d1 d2 d3 F1 F2. destruct d1, d2, d3. unfolds eq, event_descriptor_correspondence.
  repeat (let E := fresh "E" in inverts F1 as E F1; simpl in E; rewrite decide_spec in E; rew_bool_eq in E).
  repeat (let E := fresh "E" in inverts F2 as E F2; simpl in E; rewrite decide_spec in E; rew_bool_eq in E).
  repeat constructors; simpl; rewrite decide_spec; rew_bool_eq~.
Qed.

Lemma le_antisym_exact : forall d1 d2 : t, le d1 d2 -> le d2 d1 -> d1 = d2.
Proof.
  intros d1 d2 F1 F2. destruct d1, d2. unfolds eq, event_descriptor_correspondence.
  repeat (let E := fresh "E" in inverts F1 as E F1; simpl in E; rewrite decide_spec in E; rew_bool_eq in E).
  repeat (let E := fresh "E" in inverts F2 as E F2; simpl in E; rewrite decide_spec in E; rew_bool_eq in E).
  fequals; extens; splits~.
Qed.

Lemma le_antisym : forall d1 d2 : t, le d1 d2 -> le d2 d1 -> eq d1 d2.
Proof. intros d1 d2 F1 F2. erewrite* le_antisym_exact. apply~ eq_refl. Qed.

Lemma lt_trans : forall d1 d2 d3 : t, lt d1 d2 -> lt d2 d3 -> lt d1 d3.
Proof.
  introv (L1&D1) (L2&D2). splits~.
  - applys le_trans L1 L2.
  - intro_subst. forwards: le_antisym_exact L1 L2. substs~.
Qed.

Lemma lt_not_eq : forall d1 d2 : t, lt d1 d2 -> ~ eq d1 d2.
Proof. introv (L&D) E. apply eq_eq in E. substs~. Qed.

(** [event d] is the event type corresponding to the event descriptor [d]. **)
Definition event (d : t) : Type -> Type@{r} :=
  LibList.fold_left (fun '(dE, E) T => if (dE d : bool) then (E : Type -> Type@{r}) +' T else T) void1
    event_descriptor_correspondence.

(** Mapping each component of two event descriptors. **)
Definition map2 (f : bool -> bool -> bool) (d1 d2 : event_descriptor) : event_descriptor.
Proof.
  let rec g acc l :=
    lazymatch l with
    | [] => exact acc
    | (?dE, _) :: ?l => g (acc (f (dE d1) (dE d2))) l
    end in
  let l := eval compute in event_descriptor_correspondence in
  g make_event_descriptor l.
Defined.

Lemma map2_assoc : forall f,
  LibOperation.assoc f ->
  LibOperation.assoc (map2 f).
Proof. intros f Af d1 d2 d3. destruct d1, d2, d3; compute; fequals. Qed.

Lemma map2_comm : forall f,
  LibOperation.comm f ->
  LibOperation.comm (map2 f).
Proof. intros f Af d1 d2. destruct d1, d2; compute; fequals. Qed.

Lemma map2_same : forall f,
  LibOperation.idempotent2 f ->
  LibOperation.idempotent2 (map2 f).
Proof. intros f Af d. destruct d; compute; fequals. Qed.

(** The [join] and [meet] operations of the lattice. **)

Definition join : t -> t -> t :=
  map2 (fun b1 b2 => b1 || b2).

Definition meet : t -> t -> t :=
  map2 (fun b1 b2 => b1 && b2).

Lemma join_assoc : LibOperation.assoc join.
Proof. apply map2_assoc. apply or_assoc. Qed.

Lemma join_comm : LibOperation.comm join.
Proof. apply map2_comm. apply or_comm. Qed.

Lemma join_same : LibOperation.idempotent2 join.
Proof. apply map2_same. apply or_same. Qed.

Lemma le_join : forall d1 d2,
  le d1 d2 ->
  eq (join d1 d2) d2.
Proof.
  introv F. destruct d1, d2.
  repeat (let E := fresh "E" in inverts F as E F; simpl in E; rewrite decide_spec in E; rew_bool_eq in E).
  apply eq_eq.
  unfolds join, map2; fequals;
    lazymatch goal with |- ?b1 || ?b2 = _ => destruct b1, b2; (reflexivity || false*) end.
Qed.

Lemma join_le_l : forall d1 d2,
  le d1 (join d1 d2).
Proof.
  intros d1 d2. destruct d1, d2. unfolds le, event_descriptor_correspondence.
  repeat constructors; rewrite decide_spec; rew_bool_eq; simpl; rew_istrue; left~.
Qed.

Lemma join_le_r : forall d1 d2,
  le d2 (join d1 d2).
Proof.
  intros d1 d2. destruct d1, d2. unfolds le, event_descriptor_correspondence.
  repeat constructors; rewrite decide_spec; rew_bool_eq; simpl; rew_istrue; right~.
Qed.

Lemma meet_assoc : LibOperation.assoc meet.
Proof. apply map2_assoc. apply and_assoc. Qed.

Lemma meet_comm : LibOperation.comm meet.
Proof. apply map2_comm. apply and_comm. Qed.

Lemma meet_same : LibOperation.idempotent2 meet.
Proof. apply map2_same. apply and_same. Qed.

Lemma le_meet : forall d1 d2,
  le d1 d2 ->
  eq (meet d1 d2) d1.
Proof.
  introv F. destruct d1, d2.
  repeat (let E := fresh "E" in inverts F as E F; simpl in E; rewrite decide_spec in E; rew_bool_eq in E).
  apply eq_eq.
  unfolds meet, map2; fequals;
    lazymatch goal with |- ?b1 && ?b2 = _ => destruct b1, b2; (reflexivity || false*) end.
Qed.

Lemma meet_le_l : forall d1 d2,
  le (meet d1 d2) d1.
Proof.
  intros d1 d2. destruct d1, d2. unfolds le, event_descriptor_correspondence.
  repeat constructors; rewrite decide_spec; rew_bool_eq; simpl; rew_istrue; intros (?&?); autos~.
Qed.

Lemma meet_le_r : forall d1 d2,
  le (meet d1 d2) d2.
Proof.
  intros d1 d2. destruct d1, d2. unfolds le, event_descriptor_correspondence.
  repeat constructors; rewrite decide_spec; rew_bool_eq; simpl; rew_istrue; intros (?&?); autos~.
Qed.

(** The bottom of the lattice. **)
Definition empty : event_descriptor :=
  ltac:(constructor; exact false).

Definition bottom : t := empty.

Lemma bottom_is_bottom : forall d, le bottom d.
Proof.
  intro d. destruct d.
  repeat constructor; simpl; rewrite decide_spec; rew_bool_eq*.
Qed.

Lemma join_bottom_r : LibOperation.neutral_r join bottom.
Proof. intro d. destruct d. unfolds join, map2, bottom, empty; fequals; apply or_false_r. Qed.

Lemma join_bottom_l : LibOperation.neutral_l join bottom.
Proof. intro d. destruct d. unfolds join, map2, bottom, empty; fequals; apply or_false_l. Qed.

Lemma meet_bottom_r : LibOperation.absorb_r meet bottom.
Proof. intro d. destruct d. unfolds meet, map2, bottom, empty; fequals; try apply and_false_r. Qed.

Lemma meet_bottom_l : LibOperation.absorb_l meet bottom.
Proof. intro d. destruct d. unfolds meet, map2, bottom, empty; fequals; apply and_false_l. Qed.

(** The top of the lattice. **)
Definition full : event_descriptor :=
  ltac:(constructor; exact true).

Definition top : t := full.

Lemma top_is_top : forall d, le d top.
Proof.
  intro d. destruct d.
  repeat constructor; simpl; rewrite decide_spec; rew_bool_eq~.
Qed.

Lemma join_top_r : LibOperation.absorb_r join top.
Proof. intro d. destruct d. unfolds join, map2, top, full; fequals; apply or_true_r. Qed.

Lemma join_top_l : LibOperation.absorb_l join top.
Proof. intro d. destruct d. unfolds join, map2, top, full; fequals; apply or_true_l. Qed.

Lemma meet_top_r : LibOperation.neutral_r meet top.
Proof. intro d. destruct d. unfolds meet, map2, top, full; fequals; apply and_true_r. Qed.

Lemma meet_top_l : LibOperation.neutral_l meet top.
Proof. intro d. destruct d. unfolds meet, map2, top, full; fequals; apply and_true_l. Qed.


(** The order [le] is directly linked to subtyping. **)
Global Instance event_le : forall d1 d2,
  le d1 d2 ->
  event d1 -< event d2.
Proof.
  introv L. unfolds event, le.
  asserts G: ((void1 : Type -> Type@{r}) -< (void1 : Type -> Type@{r})); [ typeclass |]. gen G.
  generalize (void1 : Type -> Type@{r}) at 1 3. generalize (void1 : Type -> Type@{r}).
  gen L. induction event_descriptor_correspondence as [|(dE&?) l]; introv F I.
  - assumption.
  - repeat rewrite fold_left_cons. apply IHl.
    + inverts~ F.
    + asserts Id: (dE d1 -> dE d2).
      { inverts F as E ?. rewrite decide_spec in E. rew_bool_eq~ in E. }
      repeat cases_if; (typeclass || false~).
Defined.

Lemma event_eq : forall d1 d2 : t, eq d1 d2 -> event d1 = event d2.
Proof.
  intros d1 d2 F. destruct d1, d2. unfolds eq, event_descriptor_correspondence.
  repeat (let E := fresh "E" in inverts F as E F; simpl in E).
  fequals~.
Qed.

Lemma event_le_trans : forall (d1 d2 d3 : t) (O12 : le d1 d2) (O23 : le d2 d3) T (e : event d1 T),
  event_le O23 T (event_le O12 T e) = event_le (le_trans O12 O23) T e.
Proof.
  intros d1 d2 d3 O12 O23 T e.
  unfolds event_le, event_descriptor_correspondence.
  (* simpl. *) (* Taking surprisingly long. *)
Admitted. (* TODO *)

End EventDescriptor.


(** Instantiation. **)

Module EventMonad := MonadFromEventLattice (EventDescriptor).
