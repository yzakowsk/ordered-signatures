(* A notion of John Major’s equivalence, similar to [JMeq] (or [eq_dep]),
 but with a dependent equivalence relation. *)

(* Maybe it’s not fully proper to call it by John Major? Opinions are welcomed ☺ *)

From Coq Require Import
  Classes.RelationClasses
  Logic.EqdepFacts
  JMeq.

Lemma JMeq_eq_Types : forall A (x : A) B (y : B),
  JMeq x y ->
  A = B.
Proof. inversion 1. reflexivity. Qed.

Section RelationParametered.

(* We assume a dependent relation. *)
Variable T : Type.
Variable P : T -> Type.
Variable R : forall [a], Relation_Definitions.relation (P a).

(* We add an additional hypothesis, only used in a couple of lemmas here,
  stating that [R] only based itself on the type [P a] and not upon [a]. *)
Hypothesis R_func : forall a b (E : P a = P b) x y,
  @R a x y ->
  @R b ltac:(rewrite <- E; exact x) ltac:(rewrite <- E; exact y).
(* Some lemmas also use the uniqueness of identity proofs (UIP)
  for the types [P a]. *)
Hypothesis P_UIP : forall a (E : P a = P a), E = eq_refl.

(* As for [JMeq], the only way to be related is to have the same type.
 In this case, [JMequiv] is equivalent to [R]. *)
Inductive JMequiv [a] (x : P a) : forall [b], P b -> Prop :=
  | JMequiv_intro : forall y,
      R x y ->
      JMequiv x y
  .

Lemma JMequiv_eq_Types : forall a x b y, @JMequiv a x b y -> a = b.
Proof. inversion 1. reflexivity. Defined.

Lemma JMequiv_eq_Types_eq : forall a (x y : P a) (r : R x y),
  JMequiv_eq_Types a x a y (JMequiv_intro _ _ r) = eq_refl.
Proof. reflexivity. Qed.

Lemma eq_rect_r_eq_refl : forall A x (P : A -> Type) (H : P x),
  eq_rect_r P H eq_refl = H.
Proof. reflexivity. Qed.

Lemma JMequiv_eq_rect_r : forall a b x y (E : @JMequiv a x b y),
  R x ltac:(erewrite JMequiv_eq_Types; [exact y|apply E]).
Proof.
  intros. destruct E.
  rewrite JMequiv_eq_Types_eq, eq_rect_r_eq_refl.
  assumption.
Qed.

Lemma eq_rect_r_JMequiv : forall a (x : P a) b (y : P b) (E : a = b),
  R x ltac:(rewrite E; exact y) ->
  JMequiv x y.
Proof.
  intros. subst.
  constructor.
  rewrite eq_rect_r_eq_refl in *. assumption.
Qed.

Lemma eq_rect_r_JMequiv_eq_rect_r : forall a (x : P a) b (y : P b) (E : P a = P b),
  R x ltac:(rewrite E; exact y) ->
  JMequiv x ltac:(rewrite E; exact y).
Proof. intros. constructor. assumption. Qed.

Lemma eq_rect_JMequiv : forall a (x : P a) b (y : P b) (E : a = b),
  R ltac:(rewrite <- E; exact x) y ->
  JMequiv x y.
Proof.
  intros. subst. simpl in *.
  constructor. assumption.
Qed.

Lemma JMequiv_add_eq_rect : forall a (x : P a) b (y : P b) (E : P b = P a),
  JMequiv x y ->
  JMequiv x ltac:(rewrite <- E; exact y).
Proof.
  intros a x b y E J.
  inversion J. subst.
  rewrite (P_UIP _ E).
  apply J.
Qed.

(* This lemma would not be correct because of [JMequiv_eq_Types] and the fact [P]
  is allowed not to be injective.
  To have it make sense, we could either make [R] heterogeneous or add as an hypothesis
  the injectivity of [P].

Lemma JMequiv_add_eq_rect_heterogeneous : forall a (x : P a) b (y : P b) c (E : P b = P c),
  JMequiv x y ->
  JMequiv x ltac:(rewrite <- E; exact y).
*)

Lemma JMequiv_add_eq_rect_r : forall a (x : P a) b (y : P b) (E : P a = P b),
  JMequiv x y ->
  JMequiv x ltac:(rewrite E; exact y).
Proof. intros. apply JMequiv_add_eq_rect. assumption. Qed.

Lemma R_func_swap_lr : forall a (x : P a) b (y : P b) (E : P a = P b),
  R x ltac:(rewrite E; exact y) ->
  R ltac:(rewrite <- E; exact x) y.
Proof.
  intros a x b y E r.
  pose proof R_func _ _ E _ _ r as r'.
  rewrite rew_opp_r in r'.
  apply r'.
Qed.

Lemma R_func_swap_rl : forall a (x : P a) b (y : P b) (E : P a = P b),
  R ltac:(rewrite <- E; exact x) y ->
  R x ltac:(rewrite E; exact y).
Proof.
  intros a x b y E r.
  pose proof R_func _ _ (eq_sym E) _ _ r as r'.
  erewrite <- rew_opp_r with (a := x) (x := P b) (P := fun T : Type => T).
  unfold eq_rect_r.
  rewrite eq_sym_involutive.
  apply r'.
Qed.

Lemma eq_rect_JMequiv_eq_rect_r : forall a (x : P a) b (y : P b) (E : P a = P b),
  R ltac:(rewrite <- E; exact x) y ->
  JMequiv x ltac:(rewrite E; exact y).
Proof.
  intros. constructor.
  apply R_func_swap_rl. assumption.
Qed.

Lemma JMequiv_Reflexive : forall a,
  Reflexive (@R a) ->
  Reflexive (fun x y => @JMequiv a x a y).
Proof.
  intros a Refl x.
  constructor. apply Refl.
Qed.

Lemma JMequiv_Symmetric : forall a,
  Symmetric (@R a) ->
  Symmetric (fun x y => @JMequiv a x a y).
Proof.
  intros a Sym x y E.
  destruct E as [y E].
  constructor. apply Sym. assumption.
Qed.

Lemma JMequiv_sym :
  (forall a, Symmetric (@R a)) ->
  forall a (x : P a) b (y : P b), JMequiv x y -> JMequiv y x.
Proof.
  intros Sym a x b y J.
  destruct J as [y r].
  apply JMequiv_Symmetric.
  { apply Sym. }
  constructor. apply r.
Qed.

Lemma JMequiv_Transitive : forall a,
  Transitive (@R a) ->
  Transitive (fun x y => @JMequiv a x a y).
Proof.
  intros a Trans x y z E1 E2.
  destruct E1 as [y E1], E2 as [z E2].
  constructor. eapply Trans; eassumption.
Qed.

Lemma JMequiv_trans :
  (forall a, Transitive (@R a)) ->
  forall a (x : P a) b (y : P b) c (z : P c), JMequiv x y -> JMequiv y z -> JMequiv x z.
Proof.
  intros Trans a x b y c z Exy Eyz.
  destruct Exy as [y rxy], Eyz as [z ryz].
  eapply JMequiv_Transitive; [ apply Trans | .. ]; constructor; eassumption.
Qed.

(* Same comment as [JMequiv_add_eq_rect_heterogeneous] above: those two lemmas are
  not provable under the current assumptions.

Lemma JMequiv_cancel_eq_rect : forall a (x : P a) b (y : P b) (E : P a = P b),
  JMequiv x ltac:(rewrite E; exact y) ->
  JMequiv x y.

Lemma eq_rect_r_JMequiv_UIP : forall a (x : P a) b (y : P b) (E : P a = P b),
  R x ltac:(rewrite E; exact y) ->
  JMequiv x y.
Proof.
  intros a x b y E J.
  apply eq_rect_r_JMequiv_eq_rect_r in J.
  apply JMequiv_cancel_eq_rect in J.
  apply J.
Qed.
*)

End RelationParametered.

Arguments JMequiv [T P] R [a] x [b] _.

Lemma JMequiv_JMeq : forall T (P : T -> Type) a (x : P a) b (y : P b),
  JMequiv (fun _ => eq) x y -> JMeq x y.
Proof.
  intros T P a x b y E.
  destruct E. subst.
  constructor.
Qed.

(* Equivalent definition but with two dependent types. *)
Section RelationParametered2.

Variable T1 T2 : Type.
Variable P : T1 -> T2 -> Type.
Variable R : forall [t1 t2], Relation_Definitions.relation (P t1 t2).

Definition JMequiv2 [a1 a2] x [b1 b2] y :=
  @JMequiv (T1 * T2) (fun '(t1, t2) => P t1 t2) (fun '(t1, t2) => @R t1 t2)
    (a1, a2) x (b1, b2) y.

End RelationParametered2.

Arguments JMequiv2 [T1 T2 P] R [a1 a2] x [b1 b2] y.

