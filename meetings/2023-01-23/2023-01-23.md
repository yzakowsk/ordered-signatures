Visite de Martin à Yannick

* Exposé JFLA

- On est en premier à parler et l’audience a beaucoup de doctorants : mieux vaut faire une bonne introduction aux monades.

- Plan :
	+ Monade
		-- Calculs selon leurs effets.
		-- Abstraire le let-binding.
	+ Free monade
		-- FreeSpec
		-- ITree
		-- (Skeletons)
		-- …
	+ À quoi ça sert
		-- Language de programmation
			++ Simuler des aspects impératifs / parallèles / etc. dans un language fonctionnel pur.
		-- Domaine sémantique
			++ Faire des preuves.
		-- On a tendance à alterner entre ces deux aspects : on va parfois vouloir utiliser les monades uniquement pour l’aspect language, mais finir par se baser sur l’aspect sémantique.
	+ Exemple de R

* Futur

- Dĳkstra Monads For All, Maillard, Cătălin, ….
	+ Observational Monads.
		-- Qui sont mal nommées car pas des monades, en fait.

- Comment bien gérer les équivalences eqM dans les monades ?

- Read avec une empreinte (quelles variables peuvent être touchées ?).

- Types dépendants. Écriture monotone (un compteur qu’on ne peut qu’incrémenter, et donc prouver qu’effectivement il est forcément plus grand qu’avant).

- Jouer avec les signatures :

	+ Faire en sorte que les sgnatures ne soient pas forcément inclues dans le treillis.
		-- Une action A à un niveau du treillis que l’on pourrait décomposer en actions plus petites B et C.
			++ A peut se compiler vers un ITree (B +' C).
			++ On peut donc avoir deux éléments l₁ et l₂ du treillis, avec l₁ ≤ l₂, où A est la signature associée à l₁, et B +' C celle associée à l₂.
			++ A peut être « safe » sans que B et C le soient. Par exemple B pourrait casser un invariant et C le remettre s’il n’a été cassé que par un seul B : si on a B et C, l’invariant n’a aucune raison d’être présent à la fin. Mais A pourrait appeller B, puis C, systématiquement. Avec seulement A, l’invariant est toujours présent. On s’attendrait donc à ce que la monade M l₁ soit plus simple que M l₂.

	+ Des signatures un petit peu bizarres avec des actions qui changeraient de sens.
		-- Attention, gros guillemets parce que je vais utiliser le mot « quantique » sans y connaître grand chose.
		-- On a un q-bit, qu’on va représenter par un vecteur dans le plan.
		-- On aurait deux manières de lire ce q-bit : en le projetant selon deux vecteur différents.
		-- Est-ce que les monades qui ne parlent que d’un seul vecteur pourraient utiliser exactement, syntaxiquement, la même action Read ?

- Une bibliothèque clés en main

	+ Définir des transformateurs de monades, avec des actions associées.
		-- stateT serait associé à Read et Write, par exemple.
		-- Juste une classe de type bien choisie ?
	+ Étant donné un treillis et un transformateur de monade, construire un nouveau treilli :
		-- On dupplique le treillis initial en appliquant le transformateur de monade partout.
		-- On l’ordonne de manière naturelle.
		-- On ajoute les actions associées à ce transformateur de monade aux signatures de chaque élément du treillis avec +'
	+ Tout ceci pourrait ressembler au genre de modifications de sémantiques que l’on voit dans « Meta-theory à la carte ».
		-- On permet à l’utilisateur de construire son treillis comme il l’entends, mais on fournit des outils pour compléter son treillis d’un nouvel élément sémantique.

	+ Question : Comment tout ceci se composerait si on composerait deux treillis ?
		-- Jusque là, c’est comme si on composait avec un treillis à deux éléments (transformateur identité ≤ transformateur d’état).
		-- Peut-être que le treillis de droite est forcément constitué de transformateurs de monades : ce n’est pas un treillis d’indices comme avant.

