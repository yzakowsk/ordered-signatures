\documentclass[aspectratio=169]{beamer}

\usepackage{fontspec}
\setmainfont[Ligatures=TeX]{Linux Libertine O}

\input{beamer-macros}

\input{mintedSettings}

\makeatletter
\newcommand\mpar@math[3]{\ensuremath{\left(#1\right)\ifnempty{#2}{_{#2}}\ifnempty{#3}{^{#3}}}}
\newcommand\mpar@text[3]{\ensuremath{(#1)\ifnempty{#2}{_{#2}}\ifnempty{#3}{^{#3}}}}
\newcommand\mpar@belowabove[3]{\mathchoice{\mpar@math{#1}{#2}{#3}}{\mpar@text{#1}{#2}{#3}}{\mpar@text{#1}{#2}{#3}}{\mpar@text{#1}{#2}{#3}}}

\def\mpar@grabbelow#1#2#3_#4{%
  \mpar@grab{\ifnempty{#1}{#1, }#4}{#2}{#3}%
}
\def\mpar@grabbove#1#2#3^#4{%
  \mpar@grab{#1}{\ifnempty{#2}{#2, }#4}{#3}%
}
\newcommand\mpar@grab[3]{%
  \@ifnextchar_{\mpar@grabbelow{#1}{#2}{#3}}%
  {\@ifnextchar^{\mpar@grababove{#1}{#2}{#3}}%
  {\mpar@belowabove{#3}{#1}{#2}}}%
}

\newcommand\mpar{\mpar@grab{}{}}
\makeatother

\newcommand\funp{\mpar}
\newcommand\funu[1]{\ifnempty{#1}{\funp{#1}}}
\newcommand\fund[2]{\ifnempty{#1#2}{\funp{\sep{#1}{#2}}}}
\newcommand\funt[3]{\ifnempty{#1#2#3}{\funp{\sept{#1}{#2}{#3}}}}
\newcommand\funq[4]{\ifnempty{#1#2#3#4}{\funp{\sepq{#1}{#2}{#3}{#4}}}}

\newcommand\sep[2]{#1\ifnempty{#1}{\ifnempty{#2}{,}}#2}
\newcommand\sept[3]{\sep{#1}{#2}\ifnempty{#1#2}{\ifnempty{#3}{,}}#3}
\newcommand\sepq[4]{\sept{#1}{#2}{#3}\ifnempty{#1#2#3}{\ifnempty{#4}{,}}#4}

\newcommand\evalr{\ensuremath{\Downarrow}}

\newcommand*\seq[2]{\mathit{seq}\ #1\ #2}
\newcommand*\seqx[1]{\mathit{seq}_1\ #1}
\newcommand*\seqxx{\mathit{seq}_2}
\newcommand*\rseq[2]{#1 ; #2}

\newcommand*\abort[1]{\mathbf{abort}\ifnempty{#1}{\,#1}}

\usepackage{mathpartir}

\newcommand\emphb{\textbf}

\newcommand\ldefinition[2]{
	\draw [DarkButter, thick, ->] (#1) to [bend right = 40] (#2) ;
}

\newcommand{\retn}{\ilc{ret}\xspace}
\newcommand{\bindn}{\ilc{bind}\xspace}
\newcommand\inferred[1]{\raisebox{.5ex}{\textcolor{gray}{\small\texttt\,#1}}}
\newcommand\inferredfun[2]{\inferred{#1\,\(\to\)\,#2}}
\newcommand\inferredfmap[2]{\inferred{#1\,\(\leadsto\)\,#2}}
\newcommand\inferredbind[4]{\inferred{#1\,\(\to\)\,(#2\,\(\to\)\,#3)\,\(\to\)\,#4}}
\newcommand\typen{\ilc{Type}\xspace}
\newcommand\lfont[1]{\mbox{\texttt{\lowercase{#1}}}}


\renewcommand\totalmeaningfulframenumber{12}

\title{Hiérarchies de Monades}
\author{Jean Abou Samra, \textbf{Martin Bodin}, et Yannick Zakowski}
\institute{Inria}
\date{11 Mai 2023}

\begin{document}

\begin{frame}
	\titlepage

	\begin{center}
		Spades \includegraphics[height=2ex]{images/Spades.png}

		\url{martin.bodin@inria.fr}
	\end{center}
\end{frame}

% Formaliser des langages de programmation en Coq.
% Coq est un langage fonctionnel pur : Comment représenter les effets de bords ?
\begin{frame}
	\frametitle{Un interpréteur en Coq\minicoqlogo{}}

	\begin{centertikz}
		\node (coq) {\includegraphics[width = 2cm]{images/CoqLogo.png}} ;
		\node [below = -5mm of coq, xshift=-1ex, Plum] {Coq} ;
		\node [right = 4cm of coq, locnode yellow] (lang) {Langage} ;

		\draw[->, thick, DarkPlum] (coq) -- (lang) node [midway, above, Plum] (int) {Interpréteur} ;

		\onslide<2->{
			\node [below = 1cm of coq, locnode orange] (pur) {Fonctionnel pur} ;
			\draw (lang |- pur) node [locnode orange] {Pointeurs, tas, I/O, etc.} ;
		}
	\end{centertikz}

\end{frame}

\section{Monades}

% En fait, plus généralement, comment faire pour ne pas se mélanger les états ?
% Pour forcer une utilisation séquentielle ?
% => On veut formaliser la séquence, d'où le bind.
% => Ça fonctionne parce qu'on ne peut pas sortir de la monade.
\begin{frame}[fragile]
	\frametitle{Comment forcer la séquence~?}
	\framesubtitle{Entrez dans la monade}

\begin{overlayarea}{\textwidth}{6cm}
\begin{onlyenv}<1>
\begin{camlcode}
type 'a m

val ret : 'a -> 'a m
val bind : 'a m -> ('a -> 'b m) -> 'b m
\end{camlcode}
\end{onlyenv}
\begin{onlyenv}<2>
\begin{camlcode}
type 'a m

val ret : 'a -> 'a m
val bind : 'a m -> ('a -> 'b m) -> 'b m

val read : var -> int m
val write : var -> int -> unit m
\end{camlcode}
\end{onlyenv}
\begin{onlyenv}<3>
\begin{camlcode}
type 'a m

val ret : 'a -> 'a m
val bind : 'a m -> ('a -> 'b m) -> 'b m

val read : var -> int m
val write : var -> int -> unit m


let example =
  bind (write "x" 42) (fun () ->
  bind (read "y") (fun v ->
  bind (write "z" (v + 1)) (fun () ->
  ret v)))
\end{camlcode}
\end{onlyenv}
\begin{onlyenv}<4>
\begin{camlcode}
type 'a m

val ret : 'a -> 'a m
val bind : 'a m -> ('a -> 'b m) -> 'b m

val read : var -> int m
val write : var -> int -> unit m


let example =
  write "x" 42 ;%m
  let%m v = read "y" in
  write "z" (v + 1) ;%m
  ret v
\end{camlcode}
\end{onlyenv}
\end{overlayarea}

% Monades
% Une monade, c'est ret + bind + des opérations de base.
% (Oui, ça a beaucoup bougé depuis Leibniz)
%
\end{frame}

% Exemples.
% État.
% Erreur.
% État + erreur.

\begin{frame}[fragile]
	\frametitle{Monade d'état}

\begin{camlcode}
type 'a m = state -> ('a * state)

let ret (v : 'a) : 'a m = fun s -> (v, s)
let bind (r : 'a m) (k : 'a -> 'b m) : 'b m =
  fun s -> let (v, s') = (r s) in k v s'

let read (x : var) : int m = fun s -> (s[x], s)
let write (x : var) (v : int) : unit m = fun s -> ((), s[x <- v])
\end{camlcode}

\end{frame}

\begin{frame}[fragile]
	\frametitle{Monade d'erreur}

\begin{camlcode}
type 'a m =
  | OK of 'a
  | Error of exn

let ret (v : 'a) : 'a m = OK v
let bind (r : 'a m) (k : 'a -> 'b m) : 'b m =
  match r with
  | OK v -> k v
  | Error exn -> Error exn

let fail (e : exn) : 'a m = Error e
\end{camlcode}

\end{frame}

\begin{frame}[fragile]
	\frametitle{Monade d'état et d'erreur}

\begin{camlcode}
type 'a res =
  | OK of 'a
  | Error of exn

type 'a m = state -> ('a * state) res

let ret (v : 'a) : 'a m = fun s -> OK (v, s)
let bind (r : 'a m) (k : 'a -> 'b m) : 'b m =
  fun s ->
    match r s with
    | OK (v, s') -> k v s'
    | Error exn -> Error exn

let read (x : var) : int m = fun s -> OK (s[x], s)
let write (x : var) (v : int) : unit m = fun s -> OK ((), s[x <- v])
let fail (e : exn) : 'a m = fun s -> Error e
\end{camlcode}

\end{frame}

% On a une monade qui commence à se complexifier : et si on voulait justement se simplifier les preuves ?
% Plus une structure est compliquée, plus les preuves sont compliquées.
\begin{frame}[fragile]

	\begin{centertikz}
		\node (code) {
	\begin{minipage}{.5\textwidth}
\begin{camlcode}
let example =
  write "x" 42 ;%m
  let%m v = read "y" in
  write "z" (v + 1) ;%m
  ret v
\end{camlcode}
	\end{minipage}
		} ;

	\onslide<2>{
		\node[below right = 2em of code, xshift = 15mm] (state) {Monade d'état} ;
		\node[above right = 2em of code, xshift = 15mm] (stateerr) {Monade d'état et d'erreur} ;

		\draw[->, thick, DarkPlum] (code) to [out = 0, in = 180] (state) ;
		\draw[->, thick, DarkPlum] (code) to [out = 0, in = 180] (stateerr) ;
		}

	\end{centertikz}

\end{frame}


\section{Morphismes de monade}
\begin{frame}
	\frametitle{Morphismes de monade}
	\begin{centertikz}
		\node (ma) {\texttt{m1}} ;
		\node[below = 1ex of ma] (reta) {\texttt{ret\%m1}} ;
		\node[below = 1ex of reta] (binda) {\texttt{bind\%m1}} ;
		\node[right = 2cm of ma] (mb) {\texttt{m2}} ;
		\node[below = 1ex of mb] (retb) {\texttt{ret\%m2}} ;
		\node[below = 1ex of retb] (bindb) {\texttt{bind\%m2}} ;

		\draw [->, double, thick, Plum] (ma) -- (mb) ;
		\draw [->, double, thick, Plum] (reta) -- (retb) ;
		\draw [->, double, thick, Plum] (binda) -- (bindb) ;
	\end{centertikz}
\end{frame}


% Exemples.
% État => État + erreur
% Erreur => État + erreur
% monade identité => État
% monade identité => Erreur

% montrer le treillis. Puis le treillis de signatures.
% invariant gratuit sur read_only : la valeur de la variable ne change pas. % TODO
% treillis de signatures de R.
% mais zut, pourquoi on prendrait une monade compliquée si on n'utilise pas toutes ses actions
% réponse : le programme dans son intégralité oui, mais les parties de programme non.

\begin{frame}
	\frametitle{Hiérarchie de morphismes}

	\begin{center}
		\begin{tikzpicture}[node distance = 15mm]
				\node (Ldecl) {} ;

			\begin{scope}
				\node [below left = 3cm of Ldecl, xshift = -1cm, yshift=2cm] (Mdecl) {Monades} ;
				\node [below = 2ex of Mdecl] (mRW) {état + erreur} ;
				\node [below right of = mRW] (mW) {état} ;
				\node [below left of = mRW] (mR) {erreur} ;
				\node [below left of = mW] (mV) {pure} ;
				\draw[->, double] (mV) -- (mW) ;
				\draw[->, double] (mW) -- (mRW) ;
				\draw[->, double] (mV) -- (mR) ;
				\draw[->, double] (mR) -- (mRW) ;
				\node [draw, rectangle, rounded corners, fit=(Mdecl)(mRW)(mR)(mW)(mV)] (Mscope) {};
			\end{scope}

			\onslide<2->{
			\begin{scope}
				\node [below right = 3cm of Ldecl, xshift = 1cm, yshift=2cm] (Sdecl) {Signatures} ;
				\node [below = 2ex of Sdecl] (sRW) {\(\{\mbox{\ilc{read}}, \mbox{\ilc{write}}, \mbox{\ilc{fail}}\}\)} ;
				\node [below right of = sRW] (sW) {\(\{\mbox{\ilc{read}}, \mbox{\ilc{write}}\}\)} ;
				\node [below left of = sRW] (sR) {\(\{\mbox{\ilc{fail}}\}\)} ;
				\node [below left of = sW] (sV) {\(\emptyset\)} ;
				\draw[->] (sV) -- (sW) ;
				\draw[->] (sW) -- (sRW) ;
				\draw[->] (sV) -- (sR) ;
				\draw[->] (sR) -- (sRW) ;
				\node [draw, rectangle, rounded corners, fit=(Sdecl)(sRW)(sR)(sW)(sV)] (Sscope) {};
			\end{scope}

			\draw[->, thick, DarkPlum] (Sscope) -- node [below] {interprétation des actions} (Mscope) ;
			}
		\end{tikzpicture}
	\end{center}
\end{frame}


\begin{frame}[fragile]
	\begin{overlayarea}{\textwidth}{8cm}
	\begin{onlyenv}<1>
\begin{camlcode}
type 'a pure                       type 'a state
type 'a error                      type 'a state_error

val fmap_pure_error : 'a pure -> 'a error
val fmap_pure_state : 'a pure -> 'a state
val fmap_error_state_error : 'a error -> 'a state_error
val fmap_state_state_error : 'a state -> 'a state_error
\end{camlcode}
		\begin{centertikz}[node distance = 15mm]
			\begin{scope}
				\node [below left = 3cm of Ldecl, xshift = -1cm, yshift=2cm] (Mdecl) {Monades} ;
				\node [below = 2ex of Mdecl] (mRW) {état + erreur} ;
				\node [below right of = mRW] (mW) {état} ;
				\node [below left of = mRW] (mR) {erreur} ;
				\node [below left of = mW] (mV) {pure} ;
				\draw[->, double] (mV) -- (mW) ;
				\draw[->, double] (mW) -- (mRW) ;
				\draw[->, double] (mV) -- (mR) ;
				\draw[->, double] (mR) -- (mRW) ;
				\node [draw, rectangle, rounded corners, fit=(Mdecl)(mRW)(mR)(mW)(mV)] (Mscope) {};
			\end{scope}
		\end{centertikz}
	\end{onlyenv}
	\begin{onlyenv}<2>
\begin{camlcode}
type 'a pure                       type 'a state
type 'a error                      type 'a state_error

val fmap_pure_error : 'a pure -> 'a error
val fmap_pure_state : 'a pure -> 'a state
val fmap_error_state_error : 'a error -> 'a state_error
val fmap_state_state_error : 'a state -> 'a state_error

let example : int state =
  write "x" 42 ;%state
  let%state v = read "y" in
  write "z" (v + 1) ;%state
  ret v

let bigger_example : int state_error =
  let%state_error v = fmap_state_state_error example in
  if v = 0 then fail Not_found
  else ret 18
\end{camlcode}
	\end{onlyenv}
	\begin{onlyenv}<3>
\begin{camlcode}
type 'a pure                       type 'a state
type 'a error                      type 'a state_error

(* Hiérarchie de monades. *)
(* ... *)



let example (* : int state *) =
  write "x" 42 ;%
  let% v = read "y" in
  write "z" (v + 1) ;%
  ret v

let bigger_example (* : int state_error *) =
  let% v = example in
  if v = 0 then fail Not_found
  else ret 18
\end{camlcode}
	\end{onlyenv}
	\begin{onlyenv}<4>
\begin{camlcode}
type 'a pure                       type 'a state
type 'a error                      type 'a state_error

(* Hiérarchie de monades. *)
(* ... *)
(* Bind hétérogène *)
val bind : 'a m1 -> ('a -> 'b m2) -> 'b m3

let example (* : int state *) =
  write "x" 42 ;%
  let% v = read "y" in
  write "z" (v + 1) ;%
  ret v

let bigger_example (* : int state_error *) =
  let% v = example in
  if v = 0 then fail Not_found
  else ret 18
\end{camlcode}
	\end{onlyenv}
	\begin{onlyenv}<5>
\begin{camlcode}
type 'a m#v                        type 'a m#s
type 'a m#e                        type 'a m#se

(* Hiérarchie de monades. *)
(* ... *)
(* Bind hétérogène *)
val bind : @\(\forall\)@ i j, 'a m#i -> ('a -> 'b m#j) -> 'b m#(i @\(\sqcup\)@ j)

let example (* : int m#s *) =
  write "x" 42 ;%
  let% v = read "y" in
  write "z" (v + 1) ;%
  ret v

let bigger_example (* : int m#se *) =
  let% v = example in
  if v = 0 then fail Not_found
  else ret 18
\end{camlcode}
	\end{onlyenv}
	\end{overlayarea}
\end{frame}

\section{Hiérarchie de monades}

\begin{frame}
	\frametitle{Hiérarchie de monades}

	\begin{center}
		\begin{tikzpicture}[node distance = 15mm]
			\visible<2->{
			\begin{scope}
				\node (Ldecl) {Indices} ;
				\node [below = 2ex of Ldecl] (RW) {\lfont{SE}} ;
				\node [below right of = RW] (W) {\lfont{S}} ;
				\node [below left of = RW] (R) {\lfont{E}} ;
				\node [below left of = W] (V) {\lfont{V}} ;
				\draw[->] (V) -- (W) ;
				\draw[->] (W) -- (RW) ;
				\draw[->] (R) -- (RW) ;
				\draw[->] (V) -- (R) ;
					\node [draw, rectangle, rounded corners, fit=(Ldecl)(RW)(W)(R)(V)] (Lscope) {};
			\end{scope}
			}

			\begin{scope}
				\node [below right = 3cm of Ldecl, xshift = 1cm] (Sdecl) {Signatures} ;
				\node [below = 2ex of Sdecl] (sRW) {\(\{\mbox{\ilc{read}}, \mbox{\ilc{write}}, \mbox{\ilc{fail}}\}\)} ;
				\node [below right of = sRW] (sW) {\(\{\mbox{\ilc{read}}, \mbox{\ilc{write}}\}\)} ;
				\node [below left of = sRW] (sR) {\(\{\mbox{\ilc{fail}}\}\)} ;
				\node [below left of = sW] (sV) {\(\emptyset\)} ;
				\draw[->] (sV) -- (sW) ;
				\draw[->] (sW) -- (sRW) ;
				\draw[->] (sV) -- (sR) ;
				\draw[->] (sR) -- (sRW) ;
				\node [draw, rectangle, rounded corners, fit=(Sdecl)(sRW)(sR)(sW)(sV)] (Sscope) {};
			\end{scope}

			\begin{scope}
				\node [below left = 3cm of Ldecl, xshift = -1cm] (Mdecl) {Monades} ;
				\node [below = 2ex of Mdecl] (mRW) {état + erreur} ;
				\node [below right of = mRW] (mW) {état} ;
				\node [below left of = mRW] (mR) {erreur} ;
				\node [below left of = mW] (mV) {pure} ;
				\draw[->, double] (mV) -- (mW) ;
				\draw[->, double] (mW) -- (mRW) ;
				\draw[->, double] (mV) -- (mR) ;
				\draw[->, double] (mR) -- (mRW) ;
				\node [draw, rectangle, rounded corners, fit=(Mdecl)(mRW)(mR)(mW)(mV)] (Mscope) {};
			\end{scope}

			\visible<2>{
			\draw[->, thick, DarkPlum] (Lscope) to [bend left = 30] node [above, sloped] {réification} (Sscope.north) ;
			\draw[->, thick, DarkPlum] (Lscope) to [bend right = 30] node [above, sloped] {réification} (Mscope.north) ;
			}
			\draw[->, thick, DarkPlum] (Sscope) -- node [below] {interprétation des actions} (Mscope) ;
		\end{tikzpicture}
	\end{center}
\end{frame}

% remontrer le type de bind, puis animation avec trois monades différentes, puis avec m3 = m1 join m2.
% si vous avez compris ça, vous avez compris l'exposé : point de vue utilisateur.
% Se focaliser sur la version treillis, mais indiquer qu'on a en fait 4 versions dans le papier.

\def\anchorYShift{3ex}
\begin{frame}[fragile]
	\frametitle<-2>{Usage}
	\frametitle<3->{Merci pour votre attention~!}

\begin{camlcode}
val bind : @\(\forall\)@ i j, 'a m#i -> ('a -> 'b m#j) -> 'b m#(i @\(\sqcup\)@ j)
\end{camlcode}

\begin{camlcode}
let example (* : int m#s = int state *) = @\eanchor{exampleBegin}@
  write "x" 42 ;%
  let% v = read "y" in
  write "z" (v + 1) ;%
  ret v @\eanchor{exampleEnd}@

let bigger_example (* : int m#se = int state_error *) =
  let% v = example in
  if v = 0 then fail Not_found
  else ret 18
\end{camlcode}

	\begin{tikzpicture}[remember picture, overlay]
		\onslide<2->{
			\draw [decorate, decoration = {brace}, thick, Plum] ($(exampleBegin)+(2ex,\anchorYShift)$) -- ($(exampleBegin |- exampleEnd)+(2ex,\anchorYShift)$) node [right, midway, xshift = 1ex] {\parbox{4cm}{Par construction,\\ne peut pas échouer.}} ;
			}
	\end{tikzpicture}

\end{frame}

\frame{\tableofcontents}
\sectionframe*{Bonus}

\newcommand\questiontoc{
    %\begin{multicols}{2}
    \begin{enumerate}
				\item \hyperlink{frame:monads}{Monades de lecture seule, écriture seule, etc.},
				\item \hyperlink{frame:tailles:semantiques}{Tailles de sémantiques}.
    \end{enumerate}
    %\end{multicols}
}

\frame{\questiontoc}

% Intro monades
% bind (+ ret)
% instantiation à la monade d'état, en montrant le code de read et write d'abord.
% instantiation à read_only en gardant le même code, mais en virant tout ce qu'il faut, avec des espaces.
% puis pure, toujours en virant tout ce qu'on veut.
% montrer write_only après.

\begin{frame}[fragile]
	\label{frame:monads}
	\frametitle<1>{Monades}
	\frametitle<2-3>{Monade d’état}
	\frametitle<4>{Monade de lecture seule}
	\frametitle<5-6>{Monade identité / Monade pure}
	\frametitle<7>{Monade d’écriture seule}

% \begin{align*}
% 	M &: \typen \to \typen \\
% 	& \text{\(M~T\) est un calcul qui à terme va calculer du \(T\).} \\[2ex]
% 	\bindn &: \forall A B, M~A \to (A \to M~B) \to M~B \\
% 	\retn &: \forall A, A \to M~A \\
% \end{align*}
% 
% \vspace

%	\begin{block}{En OCaml}
	\begin{overlayarea}{\textwidth}{6cm}
	\begin{onlyenv}<1>
\begin{camlcode}
(* Une monade m. *)
type 'a m
(* bool m représente un calcul qui à terme va calculer un booléen. *)

(* Séquence *)
val bind :    'a m ->   ('a -> 'b m) -> 'b m



(* Calcul immédiat *)
val ret :    'a -> 'a m
\end{camlcode}
\end{onlyenv}
	\begin{onlyenv}<2>
\begin{camlcode}
(* Une monade pour manipuler une case mémoire. *)
type 'a m
(* bool m représente un calcul qui à terme va calculer un booléen. *)

(* Séquence *)
val bind :    'a m ->   ('a -> 'b m) -> 'b m



(* Calcul immédiat *)
val ret :    'a -> 'a m

val read : int m
val write :     int -> unit m
\end{camlcode}
\end{onlyenv}
	\begin{onlyenv}<3>
\begin{camlcode}
(* Une monade pour manipuler une case mémoire. *)
type 'a m = int -> (int * 'a)
(* bool m représente un calcul qui à terme va calculer un booléen. *)

(* Séquence *)
let bind (e : 'a m) (k : 'a -> 'b m)  : 'b m = fun c ->
  let (c', x) = e c in
  k x c'

(* Calcul immédiat *)
let ret (x : 'a) : 'a m = fun c -> (c, x)

let read : int m = fun c -> (c, c)
let write (c' : int) : unit m = fun _ -> (c', ())
\end{camlcode}
\end{onlyenv}
	\begin{onlyenv}<4>
\begin{camlcode}
(* Une monade pour   lire    une case mémoire. *)
type 'a m = int ->        'a
(* bool m représente un calcul qui à terme va calculer un booléen. *)

(* Séquence *)
let bind (e : 'a m) (k : 'a -> 'b m)  : 'b m = fun c ->
  let      x  = e c in
  k x c

(* Calcul immédiat *)
let ret (x : 'a) : 'a m = fun c ->     x

let read : int m = fun c ->     c
\end{camlcode}
\end{onlyenv}
	\begin{onlyenv}<5>
\begin{camlcode}
(* Une monade sans opération. *)
type 'a m =               'a
(* bool m représente un calcul qui à terme va calculer un booléen. *)

(* Séquence *)
let bind (e : 'a m) (k : 'a -> 'b m)  : 'b m =
  let      x  = e   in
  k x

(* Calcul immédiat *)
let ret (x : 'a) : 'a m =              x
\end{camlcode}
\end{onlyenv}
	\begin{onlyenv}<6>
\begin{camlcode}
(* Une monade sans opération. *)
type 'a m = 'a
(* bool m représente un calcul qui à terme va calculer un booléen. *)

(* Séquence *)
let bind (e : 'a m) (k : 'a -> 'b m) : 'b m =
  k e


(* Calcul immédiat *)
let ret (x : 'a) : 'a m = x
\end{camlcode}
\end{onlyenv}
	\begin{onlyenv}<7>
\begin{camlcode}
(* Une monade pour écrire une case mémoire. *)
type 'a m = 'a * 'a option
(* bool m représente un calcul qui à terme va calculer un booléen. *)

(* Séquence *)
let bind ((x, w) : 'a m) (k : 'a -> 'b m) : 'b m =
  let (v, w') = k x in
  (v, if w' = None then w else w')

(* Calcul immédiat *)
let ret (x : 'a) : 'a m = (x, None)

let write (c' : int) : unit m = ((), Some c')
\end{camlcode}
\end{onlyenv}
	\end{overlayarea}
%	\end{block}

\end{frame}

\frame{\questiontoc}

\begin{frame}
	\label{frame:tailles:semantiques}
    \frametitle{Tailles de sémantiques}

    \vspace{-5mm}

    \begin{centertikz}[scale=.5, node distance = 2cm]

        \node (lambda) {\(\lambda\)-calcul} ;
        % \node [right = 5mm of lambda] (ml) {CoreML} ;
        \node [right of = lambda] (compcert) {C} ;
        \node [right of = compcert] (jscert) {JavaScript{}} ;
        \node [right of = jscert] (coqR) {R{}} ;
        \node [right of = coqR] (wasm) {WebAssembly{}} ;

        % Scale: 1 rule/filter = .005.

        \onslide<1-2>{
            \draw [DarkPlum, fill = LightPlum] ($(lambda.north) + (-.5, .1)$) rectangle ++ (1, .015) ;
            \node [above = 1mm of lambda] {\(3\)~règles} ;
        }
        \onslide<3>{
            \draw [DarkSkyBlue, fill = LightSkyBlue] ($(lambda.north) + (-.5, .1)$) rectangle ++ (1, .02) ;
            \node [above = 1mm of lambda] {\(4\)~fonctions} ;
        }
%       \onslide<4>{
%           \draw [DarkPlum, fill = LightPlum] ($(lambda.north) + (-1, .1)$) rectangle ++ (1, .015) ;
%           \node [above = 1mm of lambda] {\(3\)~règles} ;
%           \draw [DarkSkyBlue, fill = LightSkyBlue] ($(lambda.north) + (0, .1)$) rectangle ++ (1, .02) ;
%       }


%       \onslide<1>{
%           \draw [DarkPlum, fill = LightPlum] ($(ml.north) + (-.5, .1)$) rectangle ++ (1, .25) ;
%           \node [above = 2mm of ml] {\(\sim{}50\)~règles} ; % 61 in pretty-big-step.
%           % Source: http://www.chargueraud.org/research/2012/pretty/
%       }
%       \onslide<2>{
%           \draw [DarkSkyBlue, fill = LightSkyBlue] ($(ml.north) + (-.5, .1)$) rectangle ++ (1, .16) ;
%           \node [above = 2mm of ml, xshift = 1mm] {\(\sim{}30\)~fonctions} ; % 13 value constructors, 19 utility fonctions or predicate.
%           % Source: estimented from http://www.chargueraud.org/research/2012/pretty/
%       }

        \onslide<1-2>{
            \draw [DarkPlum, fill = LightPlum] ($(compcert.north) + (-.5, .1)$) rectangle ++ (1, 1) ;
            \node [above = 6mm of compcert] {\(\sim{}200\)~règles} ; % 110 in big step + 77 in denotational.
            % Source: estimated from http://compcert.inria.fr/doc/html/Csem.html and http://compcert.inria.fr/doc/html/Cop.html
        }
        \onslide<3>{
            \draw [DarkSkyBlue, fill = LightSkyBlue] ($(compcert.north) + (-.5, .1)$) rectangle ++ (1, 0.6) ;
							\node [above = 6mm of compcert] {\(\sim{}120\)~fonctions} ; % 68 value constructors, 56 utility fonctions.
            % Source: estimated from http://compcert.inria.fr/doc/html/Cop.html
        }
%       \onslide<4>{
%           \draw [DarkPlum, fill = LightPlum] ($(compcert.north) + (-1, .1)$) rectangle ++ (1, 1) ;
%           \node [above = 6mm of compcert] {\(\sim{}200\)~règles} ; % 110 in big step + 77 in denotational.
%           \draw [DarkSkyBlue, fill = LightSkyBlue] ($(compcert.north) + (0, .1)$) rectangle ++ (1, 0.6) ;
%       }

				\onslide<2->{
					\node [locnode red, bottom color = white, top color = white, thick, above = 3cm of compcert] (warning) {\parbox{21mm}{\centering{}Arithmétique pointeur}} ;
					\draw [DarkScarletRed, thick, ->] (warning.south) -- ++ (0, -1) ;
					\node [locnode purple, bottom color = white, top color = white, thick, right = 1mm of warning] (new) {\parbox{18mm}{\centering{}Difficulté nouvelle}} ;
					\draw [DarkPlum, thick, ->] (new.east) to[out=0, in=90] ++ (.3, -1) ;
					}


        \onslide<1-2>{
            \draw [DarkPlum, fill = LightPlum] ($(jscert.north) + (-.5, .1)$) rectangle ++ (1, 4.5) ;
            \node [above = 23mm of jscert] {\(\sim{}900\)~règles} ; % ~900 in pretty-big-step.
        }
        \onslide<3>{
            \draw [DarkSkyBlue, fill = LightSkyBlue] ($(jscert.north) + (-.5, .1)$) rectangle ++ (1, 0.4) ;
            \node [above = 2mm of jscert] {\(\sim{}80\)~fonctions} ; % 44 value constructors, 8 records (counts double), 23 monadic binders.
        }
%       \onslide<4>{
%           \draw [DarkPlum, fill = LightPlum] ($(jscert.north) + (-1, .1)$) rectangle ++ (1, 4.5) ;
%           \node [above = 23mm of jscert] {\(\sim{}900\)~règles} ; % ~900 in pretty-big-step.
%           \draw [DarkSkyBlue, fill = LightSkyBlue] ($(jscert.north) + (0, .1)$) rectangle ++ (1, 0.4) ;
%       }

        \onslide<1-2>{
            \draw [DarkPlum, fill = LightPlum] ($(coqR.north) + (-.5, .1)$) rectangle ++ (1, 10) ;
            \node [above = 50mm of coqR] {\(\sim{}2 000\)~règles} ; % 2028 occurrences of 'let%'.
        }
        \onslide<3>{
            \draw [DarkSkyBlue, fill = LightSkyBlue] ($(coqR.north) + (-.5, .1)$) rectangle ++ (1, 0.8) ;
            \node [above = 6mm of coqR] {\(\sim{}160\)~fonctions} ; % 51 value constructors, 17 records (counts double), 77 monadic binders.
        }
%       \onslide<4>{
%           \draw [DarkPlum, fill = LightPlum] ($(coqR.north) + (-1, .1)$) rectangle ++ (1, 10) ;
%           \node [above = 50mm of coqR] {\(\sim{}2 000\)~règles} ; % 2028 occurrences of 'let%'.
%           \draw [DarkSkyBlue, fill = LightSkyBlue] ($(coqR.north) + (0, .1)$) rectangle ++ (1, 0.8) ;
%       }

        \onslide<1-2>{
            \draw [DarkPlum, fill = LightPlum] ($(wasm.north) + (-.5, .1)$) rectangle ++ (1, 1.9) ;
            \node [above = 9mm of wasm] {\(\sim{}350\)~règles} ; % 67 rules + 315 low-level equations.
        }
        \onslide<3>{
            \draw [DarkSkyBlue, fill = LightSkyBlue] ($(wasm.north) + (-.5, .1)$) rectangle ++ (1, 0.23) ;
            \node [above = 2mm of wasm] {\(\sim{}50\)~fonctions} ; % 11 value constructors, 17 records (counts double), 1 monadic binder.
        }
%       \onslide<4>{
%           \draw [DarkPlum, fill = LightPlum] ($(wasm.north) + (-1, .1)$) rectangle ++ (1, 1.9) ;
%           \node [above = 9mm of wasm] {\(\sim{}350\)~règles} ; % 67 rules + 315 low-level equations.
%           \draw [DarkSkyBlue, fill = LightSkyBlue] ($(wasm.north) + (0, .1)$) rectangle ++ (1, 0.23) ;
%       }

%        \draw [thick, dashed, ->] (compcert) to [bend right] ($(compcert) + (-1, -1)$) ;

    \end{centertikz}
%    \vspace{-5mm}

    ~\vfill

		\begin{widemargin}
    \begin{center}
        \tiny\centering{}
				(Estimation grossière
				\rlap{%
					\only<1-2>{de la taille de chaque sémantique si reformulées en petit-pas.)}%
					\only<3>{du nombre de fonctions basiques dans chaque sémantique.)}%
					% \only<4>{du nombre de fonctions basiques et de la taille de chaque sémantique si reformulées en petit-pas.)}
					}%
				% \phantom{du nombre de fonctions basiques et de la taille de chaque sémantique si reformulées en petit-pas.)}
				\phantom{de la taille de chaque sémantique si reformulées en petit-pas.)}
    \end{center}
		\end{widemargin}

%   \begin{overlayarea}{\textwidth}{1cm}
%       \tiny\centering{}
%       \only<1-2>{(Estimation grossière de la taille de chaque sémantique si reformulées en petit-pas.)}
%       \only<3>{(Estimation grossière du nombre de fonctions basiques dans chaque sémantique.)}
%       \only<4>{(Estimation grossière de la taille et du nombre de fonctions basiques de chaque sémantique si reformulées en petit-pas.)}
%   \end{overlayarea}

\end{frame}

\frame{\questiontoc}

\frame{\tableofcontents}

\end{document}

